<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $table = 'company';
    public $timestamps = false;
    protected $fillable = ['address', 'cr_no','company_name','contact_phone','contact_email','website','logo'];
}
