<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesReturn extends Model
{
    protected $table = 'sales_return';

    public function retailer()
    {
        return $this->belongsTo(Retailer::class,'retailer_id','id');
    }
}