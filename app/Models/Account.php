<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = "accounts";
    protected $fillable = [
        'name', 
        'account_no',
        'balance',
        'initial_balance',
        'note'
    ];
}