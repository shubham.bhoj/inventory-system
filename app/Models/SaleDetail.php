<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $table = 'sale_details';
    protected $fillable = [
        'retailer_id', 
        'master_gold',
        'master_dia',
        'master_cus',
        'master_kwd',
        'master_stm',
        'discount', 
        'freight', 
        'sub_total', 
        'vat', 
        'total', 
        'amount',  
        'due',
        'invoiceno',
    ];

    public function retailer()
    {
        return $this->belongsTo('App\Models\Retailer','retailer_id','id');
    }
}