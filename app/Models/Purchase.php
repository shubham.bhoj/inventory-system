<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchase';
    protected $fillable = [
        'supplier_id', 
        'order_no',
        'tot_qty',
        'tot_gross_wt',
        'tot_gld_net_wt',
        'tot_diamond_wt',
        'tot_stone_wt',
        'tot_amount', 
        'total_cost', 
        'discount', 
        'freight', 
        'vat', 
        'grand_total', 
        'paid_amount', 
        'status' ,
        'document',
    ];

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier');
    }
}