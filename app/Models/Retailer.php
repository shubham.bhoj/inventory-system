<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    protected $table = 'retailers';
    protected $fillable = [
        'company_name', 
        'contact_person', 
        'civil_no', 
        'address', 
        'mobile', 
        'mobile_code', 
        'email',
        'retailer_or_walking'
    ];
}