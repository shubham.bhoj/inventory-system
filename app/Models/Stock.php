<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stocks';
    protected $fillable = [
        'supplier_id', 
        'stock_details_id'
    ];

    public function sale()
    {
        return $this->hasOne(Sale::class);
    }
}