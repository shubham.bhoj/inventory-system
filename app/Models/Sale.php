<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sales';
    protected $fillable = [
        'rtl_id',
        'sale_detail_id', 
        'stock_id',
        'gold_per',
        'gold_lab',
        'dia_per',
        'dia_pct',
        'sub_total',
        'customer_per',
        'customer_val',
        'tot_val',
        'kwd_conver',
        'tot_val_kwd',
        'stm_kwd',
        'tot_amt'
    ];

    public function stock()
    {
        return $this->belongsTo(StockDetail::class,'stock_id','id');
    }

    public function retailer()
    {
        return $this->belongsTo(Retailer::class,'rtl_id','id');
    }
}