<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'suppliers';
    protected $fillable = [
        'supplier_code',
        'company_name', 
        'contact_person', 
        'address', 
        'mobile', 
        'mobile_code', 
        'email'
    ];
}