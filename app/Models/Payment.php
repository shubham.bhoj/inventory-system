<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "purchase_payment";
    protected $fillable = [
        'order_no', 
        'total_amount',
        'paid',
        'account_id',
        'paid_by',
        'invoice_no'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Account');
    }

}