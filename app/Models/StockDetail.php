<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockDetail extends Model
{
    protected $table = 'stock_details_new';
    protected $fillable = [
        'code', 
        'design', 
        'image', 
        'gl', 
        'diap', 
        'diav', 
        'sub_total', 
        'item', 
        'cost' ,
        'description',
        'pcs',
        'gq',
        'gw',
        'ngw',
        'dp',
        'dw',
        'dq',
        'dv',
        'cg',
        'csp',
        'csw',
        'gmc',
        'dmc',
        'total',
        'customs',
        'total_value_d',
        'total_value_kwd',
        'stamping',
        'final_total',
        'used',
    ];

    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }
}