<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'historys';

    protected $fillable = [
        'sale_details_id',
        'invoice_no',
        'account_id',
        'tot_amount',
        'amount'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Account');
    }
}