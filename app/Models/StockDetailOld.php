<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockDetailOld extends Model
{
    protected $table = 'stock_details';
    protected $fillable = [
        'stock_id', 
        'code', 
        'design', 
        'image', 
        'gw', 
        'dw', 
        'gl', 
        'diap', 
        'diav', 
        'sub_total', 
        'customs', 
        'total_value_d', 
        'total_value_kwd', 
        'stamping', 
        'final_total', 
        'item', 
        'cost' 
    ];
}