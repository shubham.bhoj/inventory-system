<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarcodeSize extends Model
{
    protected $fillable = ['barcode_id','width','height'];
}
