<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = [
        'custom_charge', 
        'stamping_charge', 
        'sales_rate_kwd', 
        'purchase_rate_kwd', 
        'vat',
        'gold_lab',
        'gold_mc',
        'diamond_mc',
    ];
}