<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoneyTransfer extends Model
{
    protected $table = "money_transfer";

    protected $fillable = [
        'reference_no', 
        'from_account',
        'to_account',
        'amount',
    ];

    public function from_Account()
    {
        return $this->belongsTo('App\Models\Account','from_account', 'id');
    }

    public function to_Account()
    {
        return $this->belongsTo('App\Models\Account','to_account', 'id');
    }
}