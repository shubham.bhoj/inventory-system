<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetails extends Model
{
    protected $table = 'purchase_details';
    protected $fillable = [
        'supplier_id', 
        'order_no', 
        'particular', 
        'qty', 
        'gross_wt', 
        'gld_net_wt', 
        'diamond_wt', 
        'stone_wt', 
        'amount' ,
        'kwd_amount',
    ];
}