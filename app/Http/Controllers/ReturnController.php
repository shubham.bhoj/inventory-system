<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Validator;
use App\Models\Sale;
use App\Models\Stock;
use App\Models\Account;
use App\Models\Barcode;
use App\Models\Company;
use App\Models\Country;
use App\Models\History;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\Purchase;
use App\Models\Retailer;
use App\Models\Supplier;
use App\Models\SaleDetail;
use App\Models\BarcodeSize;
use App\Models\SalesReturn;
use App\Models\SaleReturnDetalis;
use App\Models\StockDetail;
use Illuminate\Http\Request;
use App\Models\MoneyTransfer;
use App\Models\PurchaseDetails;
use Illuminate\Database\Eloquent\Builder;

const UPDATED = "Updated Successfully!";
const FAILED = "Failed, Please Try again!";
class ReturnController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function sale_return()
    {
        $returns = SalesReturn::orderBy('id','desc')->get();
        return view('return.sale',compact('returns'));
    }

    public function new_saleReturn()
    {
        $retailers = SaleDetail::join('retailers','retailers.id','=','sale_details.retailer_id')
        ->select('retailers.*','sale_details.retailer_id')
        ->groupBy('sale_details.retailer_id')
        ->get();
        return view('return.newsale_return',compact('retailers'));
    }

    public function invoice_return(Request $request)
    {
        $id = $request->id;
        $orders = '';
        $retaliers = SaleDetail::where('retailer_id',$id)->get();
        return $retaliers;
    }


    public function get_product(Request $request)
    {
        $invoice = $request->invoice;
        $retalier_id = $request->retailer;
        $retaliers = SaleDetail::where([  ['invoiceno', '=', $invoice],['retailer_id', '=', $retalier_id]  ])->first();
        $stocks = Sale::join('stock_details_new','stock_details_new.id','=','sales.stock_id')
        ->select('stock_details_new.description as stockDes','stock_details_new.design as stockDesign','sales.id as saleId','stock_details_new.*','sales.*','sales.stock_id as stockId')
        ->where('sales.sale_detail_id',$retaliers->id)
         ->get();

        return $stocks;
    }

    public function return_detalis(Request $request)
    {
        $ids = $request->ids;
        $retailer = $request->retailer;
        $invoice = $request->invoice;
        $amount = SaleDetail::where('invoiceno',$invoice)->first();
        $tab = '<input type="hidden" name="retailer_id" value="'.$retailer.'"><input type="hidden" name="sale_invoice" value="'.$invoice.'">';
        $details = Sale::join('stock_details_new','stock_details_new.id','=','sales.stock_id')
            ->select('stock_details_new.description as stockDes','stock_details_new.design as stockDesign','sales.id as saleId','stock_details_new.*','sales.*','sales.id as saleId')
            ->whereIn('sales.id',explode(",",$ids))
            ->get();
            
        $total = $details->sum('tot_amt');
        $settings = Setting::first();
        $vat = round(($total*$settings->vat)/100);
        $i=1;
         foreach($details as $detail)
         {

            $tab .= '<tr><td>'.$i++.'</td><input type="hidden" name="sale_id[]" value="'.$detail->saleId.'"><td>'.$detail->code.'</td><td>'.$detail->stockDesign.'</td><td><img src="'.$detail->image.'" height=50></td><td style="width: 30px;">'.$detail->tot_amt.'<input type="hidden" name="stock_id[]" value="'.$detail->stock_id.'"><input type="hidden" name="gold_lab[]" value="'.$detail->gold_lab.'"><input type="hidden" name="dia_pct[]" value="'.$detail->dia_pct.'"><input type="hidden" name="sub_total[]" value="'.$detail->sub_total.'"><input type="hidden" name="cus_value[]" value="'.$detail->customer_val.'"><input type="hidden" name="tot_val[]" value="'.$detail->tot_val.'">
                <input type="hidden" name="total_kwd_val[]" value="'.$detail->tot_val_kwd.'"><input type="hidden" name="stamping[]" value="'.$detail->stm_kwd.'"><input type="hidden" name="total_amount[]" value="'.$detail->tot_amt.'"></td></tr>'; 
         }

         return response()->json(['data' => $tab,'total' => $total,'vat' => $vat,'amount' => $amount->amount]);

    }


    public function add_saleReturn(Request $request)
    {
        $invoice = 'rr-'.date("Ymd").'-'.date("his");
        $sales_details = SaleDetail::where('invoiceno',$request->sale_invoice)->first();
        $sales = new SalesReturn;
        $sales->invoice = $invoice;
        $sales->sale_invoice = $request->sale_invoice;
        $sales->retailer_id = $request->retailer_id;
        $sales->account_id = $request->account;
        $sales->return_amt = $request->return_amount;
        if($sales->save()){

        for($i=0;$i<count($request->stock_id);$i++){
        $sale_return = new SaleReturnDetalis;
        $sale_return->invoice = $invoice;
        $sale_return->stock_id = $request->stock_id[$i];
        $sale_return->gold_lab = $request->gold_lab[$i];
        $sale_return->dia_pct = $request->dia_pct[$i];
        $sale_return->sub_total = $request->sub_total[$i];
        $sale_return->customer_val = $request->cus_value[$i];
        $sale_return->tot_val = $request->tot_val[$i];
        $sale_return->tot_val_kwd = $request->total_kwd_val[$i];
        $sale_return->stm_kwd = $request->stamping[$i];
        $sale_return->tot_amt = $request->total_amount[$i];
        if($sale_return->save())
        {
            $stocks = StockDetail::findorfail($request->stock_id[$i]);
            $stocks->used = false;
            if($stocks->save())
            {   
            Sale::where([ ['sale_detail_id', '=', $sales_details->id], ['stock_id', '=', $request->stock_id[$i]]])->delete();
           }
        }

        }
    
        $total = $sales_details->total - $request->return_amount;
        $amount = $sales_details->amount - $request->return_amount;
        $vat = $sales_details->vat - $request->vat;
        $due =  $total - $amount;
        $sales_details->vat = $vat;
        $sales_details->total = $total;
        $sales_details->amount = $amount;
        $sales_details->due = $due;
        if($sales_details->save())
        {
            $history = History::where('sale_details_id',$sales_details->id)->first();
                $total_sec = $history->tot_amount - $request->return_amount;
                $amount_sec = $history->amount - $request->return_amount;
                $history->tot_amount = $total_sec;
                $history->amount = $amount_sec;
                if($history->save())
                {
                    $account = Account::findorfail($history->account_id);
                    $bal = $account->balance - $request->return_amount;
                    $ini_bl = $account->initial_balance - $request->return_amount;
                    $account->balance = $bal;
                    $account->initial_balance = $ini_bl;
                    $account->save();
                   
                }else{
                \Session::flash('error', "There is something wrong");
                return redirect()->back();
                }

        }

        \Session::flash('success', "Return Successfully.");
        return redirect()->back();
    }
        \Session::flash('error', "There is something wrong");
        return redirect()->back();
    }


    public function view_return($invoice)
    {
        $returns = SaleReturnDetalis::join('stock_details_new','stock_details_new.id','=','sales_return_details.stock_id')->select('stock_details_new.*','sales_return_details.*')
            ->where('invoice',$invoice)
            ->get();
        $gold_wt = $returns->sum('gw');
        $dia_wt = $returns->sum('dw');
        $gold_lab = $returns->sum('gold_lab');
        $dv = $returns->sum('dia_pct');
        $sub_total = $returns->sum('sub_total');
        $custom = $returns->sum('customer_val');
        $total = $returns->sum('tot_val');
        $total_kwd = $returns->sum('tot_val_kwd');
        $total_stm = $returns->sum('stm_kwd');
        $tot_amt = $returns->sum('tot_amt');
        return view('return.view_return',compact('returns','gold_wt','dia_wt','gold_lab','dv','sub_total','custom','total','total_kwd','total_stm','tot_amt','invoice'));
    }

    public function edit_return($invoice)
    {
        $sale_account = SalesReturn::where('invoice',$invoice)->first();
        $returns = SaleReturnDetalis::join('stock_details_new','stock_details_new.id','=','sales_return_details.stock_id')->select('stock_details_new.*','sales_return_details.*')
            ->where('invoice',$invoice)
            ->get();
        $tot_amt = round($returns->sum('tot_amt'));
        $accounts = Account::get();
        return view('return.return_edit',compact('returns','tot_amt','sale_account','accounts'));
    }

    public function return_invoice(Request $request)
    {
       $invoice = $request->invioceNo;
       $retailer = SalesReturn::where('invoice',$invoice)->first();
       $returns = SaleReturnDetalis::join('stock_details_new','stock_details_new.id','=','sales_return_details.stock_id')->select('stock_details_new.*','sales_return_details.*')
            ->where('sales_return_details.invoice',$invoice)
            ->get();

        $gold_wt = $returns->sum('gw');
        $dia_wt = $returns->sum('dw');
        $gold_lab = $returns->sum('gold_lab');
        $dv = $returns->sum('dia_pct');
        $sub_total = $returns->sum('sub_total');
        $custom = $returns->sum('customer_val');
        $total = $returns->sum('tot_val');
        $total_kwd = $returns->sum('tot_val_kwd');
        $total_stm = $returns->sum('stm_kwd');
        $tot_amt = $returns->sum('tot_amt');
        //$total_sub = ($totamt+$retailer->discount)-$retailer->freight;
       
        $head = '<strong>Date: </strong>'.date('d/m/Y',strtotime($retailer->created_at)).'<br>Reference: '.$retailer->invoice.'<br><b>Sale Return</b></div><br><br><div class="row"><div class="col-md-6"><strong>To,</strong><br>'.$retailer->retailer->company_name.',<br>'.$retailer->retailer->address.',<br>Mobile: +'.$retailer->retailer->mobile.'</div></div>';
        
            $body = '<tr><td style="width:5px"><strong>1</strong></td><td>'.$gold_wt.'</td><td>'.$dia_wt.'</td><td>$'.$gold_lab.'</td><td>$'.$dv.'</td><td>$'.$sub_total.'</td><td>$'.$custom.'</td><td>'.$total.'</td><td>'.$total_kwd.' KD</td><td>'.$total_stm.' KD</td><td>'.$tot_amt.'KD</td></tr>';

        return response()->json(['head' => $head,'body' => $body]);

    }

    public function delete_return($id)
    {

    }
}