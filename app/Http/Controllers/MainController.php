<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesReturn;
use App\Models\SaleReturnDetalis;
use App\Models\PurchaseDetails;
use App\Models\Account;
use App\Models\Purchase;
use App\Models\Setting;
use App\Models\StockDetail;
use App\Models\Supplier;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\Retailer;
use App\Models\Company;
use App\Models\History;
use App\Models\Bank;
use App\Models\Expense;
use Illuminate\Support\Facades\Hash;
use Log;
use Validator;
use Auth;
use DB;
const UPDATED = "Updated Successfully!";
const FAILED = "Failed, Please Try again!";
const Amount = "Please Enter Amount!";
class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
      return view('profile');
    }

    public function banks_list()
    {
      $data = Bank::get();
      return view('banks',compact('data'));
    }

    public function history()
    {
      $data = History::get();
      return view('history',compact('data'));
    }

    public function banks_details(Request $request)
    {
      $data="";
      if(isset($request->id) && !empty($request->id))
      {
      $data = Bank::find($request->id);
      }
      return view('banks_details',compact('data'));
    }

    public function banks_view(Request $request)
    {
        $data = Bank::find($request->id);
        return view('banks_view',compact('data'));
    }

    public function banks_upsert(Request $request)
    {
        try {
        if(isset($request->id) && !empty($request->id))
        {
            $data = Bank::find($request->id);
            $validator = Validator::make($request->all(), [
                'account' => 'unique:banks,account,'.$data->id.',id',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'account' => 'unique:banks',
            ]);
            $data = new Bank; 
        }
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data->name = $request->name;
        $data->account = $request->account;
        $data->balance =  $data->balance + $request->balance;
        $data->note = $request->note;
        $data->save();
        if($data)
        {
            \Session::flash('success', UPDATED);
            return redirect()->route('banks_list');
        } else {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    } catch (\Exception $e)
    {
        \Session::flash('error', FAILED);
        return redirect()->back();
    }
    }

    public function expense_list()
    {
      $data = Expense::orderBy('created_at', 'DESC')->get();
      return view('expense',compact('data'));
    }

    public function expense_details(Request $request)
    {
      $data="";
      if(isset($request->id) && !empty($request->id))
      {
        $data = Expense::find($request->id);
      }
      return view('expense_details',compact('data'));
    }

    public function expense_view(Request $request)
    {
        $data = Bank::find($request->id);
        return view('expense_view',compact('data'));
    }

    public function expense_delete(Request $request,$id)
    {
      try {
        if(isset($request->id) && !empty($request->id))
        {
            $data = Expense::find($request->id);
            $data2 = Bank::where('type',$data->account)->first();
            $data2->balance = $data2->balance + $data->amount;
            $data2->save();
            $data->delete();
            \Session::flash('success', UPDATED);
            return redirect()->route('expense_list');
        }


      } catch (\Exception $e)
      {
          \Session::flash('error', FAILED);
          return redirect()->back();
      }
    }

    public function expense_upsert(Request $request)
    {
      try {
        if(isset($request->id) && !empty($request->id))
        {
            $data = Expense::find($request->id);
            $validator = Validator::make($request->all(), [
              'note' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
              'note' => 'required',
            ]);
            $data = new Expense; 
        }
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data->note = $request->note;
        $data->account = $request->account;
        $data->amount =  $request->amount;
        $data->save();
        if($data)
        {
          $data2 = Bank::where('type',$request->account)->first();
          $data2->balance = $data2->balance - $request->amount;
          $data2->save();
            \Session::flash('success', UPDATED);
            return redirect()->route('expense_list');
        } else {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    } catch (\Exception $e)
    {
        \Session::flash('error', FAILED);
        return redirect()->back();
    }
    }

    public function creditor_list()
    {
      $data = SaleDetail::select()->leftjoin('retailers','retailers.id','sale_details.retailer_id')
        ->select(DB::raw('sum(sale_details.total) as total'),DB::raw('sum(sale_details.due) as due'),DB::raw('sum(sale_details.amount) as cash'),'retailers.company_name','retailers.id as retailerId','retailers.mobile','retailers.mobile_code','sale_details.created_at','sale_details.invoiceno as invoiceno')
        ->where('sale_details.due','>=',1)
        ->groupBy('sale_details.retailer_id')
        ->get();
      return view('creditor_list',compact('data'));
    }

    public function walking_customers()
    {
        $data = Retailer::where('retailer_or_walking',2)->get();
        return view('walking_customers',compact('data'));
    }

    public function walking_customers_view(Request $request)
    {
        $data = Retailer::find($request->id);
        $secondData = SaleDetail::where('retailer_id',$request->id)->get();
        return view('walking_customers_view',compact('data','secondData'));
    }

    public function profile_update(Request $request)
    {
      try {
          $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'current_password' => 'required',
                'new_password' => 'required|string|min:6',
                'confirm_password' => 'required|string|min:6|same:new_password',
            ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
 
      $data = Auth::user();
      if(Hash::check($request->get('current_password'), $data->password))
      {
        $data->password = bcrypt($request->get('new_password'));
        if($data->save())
        {
            \Session::flash('success', UPDATED);
            return redirect()->route('profile');
        } else {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
      } else
      {
        \Session::flash('error','Current Password is Incorrect,Please try with new password');
            return redirect()->back();
      }
      
      } catch (\Exception $e)
      {
          \Session::flash('error', FAILED);
          return redirect()->back();
      }
    }

    public function dues_update(Request $request)
    {
      try {
        $data2 = SaleDetail::find($request->id)->first();
        $data =SaleDetail::find($request->id);
        $data2->knet = $data->knet + $request->knet;
        $data2->cash = $data->cash + $request->cash;
        $data2->due = $data->due - $request->knet -$request->cash;
          if($data2->save())
          {
              \Session::flash('success', UPDATED);
              return redirect()->back();
          } else {
              \Session::flash('error', FAILED);
              return redirect()->back();
          }
      } catch (\Exception $e)
      {
          \Session::flash('error', FAILED);
          return redirect()->back();
      }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     // FOR kapil USe
    public function downloadSalesInvoice($id)
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->salesoHtml($id));
        return $pdf->stream();
    }

    public function salesoHtml($id)
    {
       $retailer = SaleDetail::findorfail($id);
       $details = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
        ->select('stock_details_new.*','sales.*','sales.sub_total as sale_sub')
        ->where('sales.sale_detail_id',$id)
        ->get();

        $gold_wt = $details->sum('gw');
        $dia_wt = $details->sum('dw');
        $gold_lab = $details->sum('gold_lab');
        $dv = $details->sum('dia_pct');
        $sub_total = $details->sum('sale_sub');
        $custom = $details->sum('customer_val');
        $total = $details->sum('tot_val');
        $total_kwd = $details->sum('tot_val_kwd');
        $total_stm = $details->sum('stm_kwd');
        $tot_amt = $details->sum('tot_amt');

         $totamt = $details->sum('tot_amt');
        $total_sub = ($totamt+$retailer->discount)-$retailer->freight;

        $output1='<html>
        <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
        <title>invoice</title>
        <style>
        @page {
          margin: 0px 20px 0px 20px;
          padding: 0px 0px 0px 0px;
          margin-left: 2px;
      } 
        body {
         font-family: verdana,cambria;
       } 
        table {
          margin-bottom: .1em;
        }
        th,td {
          padding: 3pt;
        }        
        table.collapse {
          border-collapse: collapse;
          border: 0pt solid #000;  
        }
        
        table.collapse td {
          border: 0pt solid #000;
        }
        .fontsixze{
          font-size:20px;
        }

        table.dollapse {
            border-collapse: collapse;
            border: 0.5px solid #000;  
          }
          
        table.dollapse td {
        border: 0.5px solid #000;
        }
        table.dollapse th {
          background: #2a5686;
          color: white;
          }
          #header { 
          position: fixed; 
          width: 100%; 
          top: 0; 
          left: 0; 
          right: 0;
        }
        #footer { 
          position: fixed; 
          width: 100%; 
          bottom: 0; 
          left: 0;
          right: 0;
        }
        .custom-page-start {
            margin-top: 120px;
            margin-bottom: 120px;
        }
        .custom-footer-page-number:after {
          content: counter(page);
        }
        </style>
        
        </head>
        
        <body>
       <div id="header"></div> 
       <div id="footer"></div>

      <div class="custom-page-start" style="page-break-after: always;">
        <table width=100% >
         <tr>
        <td>

        <table width=100% class="collapse"> 
        <thead>
          <tr>
            <th colspan=4></th>
          </tr>
        </thead>
          <tbody>
          <tr>
            <td align=center colspan=4><strong></strong></td>
          </tr>
          <tr>
            <td align=center colspan=4></td>
          </tr>
          <tr>
            <td align=center colspan=4>Sales Invoice </td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table width=100% class="collapse jhk"> 
        <thead>
          <tr class="fontsixze">
            <th colspan=2></th>
          </tr>

        </thead>
          <tbody>
          <tr>
            <td style="font-size:12px"><strong>To,</strong></td>
            <td style="text-align:right;font-size:12px"><strong>Date:</strong>'.date('d/m/Y',strtotime($retailer->created_at)).'</td>
          </tr>
          <tr>
            <td style="font-size:10px;font-size:12px">'.ucwords($retailer->retailer->company_name).'</td>
            <td style="text-align:right;font-size:12px">Reference: '.$retailer->invoiceno.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">'.$retailer->retailer->address.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">Mobile: +'.$retailer->retailer->mobile.'</td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td> 
        
        <table style="font-size:10px;height:auto" width=100% class="dollapse" >
        <thead>
            <tr>
            <th style="border:0.1px solid #fff;">No.</th>
            <th style="border:0.1px solid #fff;">Particular</th>
            <th style="border:0.1px solid #fff;">GW</th>
            <th style="border:0.1px solid #fff;">DW</th>
            <th style="border:0.1px solid #fff;">Gold&nbsp;+&nbsp;Lab</th>
            <th style="border:0.1px solid #fff;">Dia&nbsp;Value</th>
            <th style="border:0.1px solid #fff;">Sub&nbsp;Total</th>
            <th style="border:0.1px solid #fff;">Custom</th>
            <th style="border:0.1px solid #fff;">$&nbsp;Total</th>
            <th style="border:0.1px solid #fff;">Amount&nbsp;KD</th>
            <th style="border:0.1px solid #fff;">Staming</th>
            <th style="border:0.1px solid #fff;">Total&nbsp;Amount</th>
            </tr>
        </thead> 
          <tbody>
          ';
         
           $output1.='<tr>
           <td style="width:2px; height:20px">1</td>
           <td style="width:25%">'.$retailer->note.'</td>
           <td align="center">'.$gold_wt.'</td>
           <td align="center">'.$dia_wt.'</td>
           <td align="center">$ '.$gold_lab.'</td>
           <td align="center">$ '.$dv.'</td>
           <td align="center">$ '.$sub_total.'</td>
           <td align="center">$ '.$custom.'</td>
           <td align="center">'.$total.'&nbsp;KD</td>
           <td align="center">'.$total_kwd.'&nbsp;KD</td>
           <td align="center">'.$total_stm.'&nbsp;KD</td>
           <td align="center">'.$tot_amt.'&nbsp;KD</td>
         </tr>';
         
         $output1.='<tr><td><img src="static/white.jpeg" width="20" height="150" style="max-width: none;" alt="No Image"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';


          $output1.='      
           
          </tbody>
        </table>
        </div>
        </td>
        </tr>
        <tr>
        <td>
        </td>
        </tr>
        <tr>
        <td>
        <table width=100% style="font-size:10px" class="dollapse"> 
        <thead>
        <tr>
        <th height="30px">Discount:</th>
        <th>Freight:</th>
        <th>Sub Total:</th>
        <th>VAT(5%)</th>
        <th>Grand Total:</th>
        <th>Paid Amount:</th>
        <th>Due Amount:</th>
        </tr>
        </thead>
          <tbody>

              <tr>
              <td height="30px" align="center">'.round($retailer->discount).'</td>
              <td align="center">'.round($retailer->freight).'</td>
              <td align="center">'.round($total_sub).'</td>
              <td align="center">'.round($retailer->vat).'</td>
              <td align="center">'.round($retailer->total).'</td>
              <td align="center">'.round($retailer->amount).'</td>
              <td align="center">'.round($retailer->due).'</td>
              </tr>
          
          </tbody>
        </table>
        </td>
        </tr>
        </table>
        </body> </html>
        ';
        return $output1;
    }

    public function downloadPurchaseInvoice($id)
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->purchaseoHtml($id));
        return $pdf->stream();
    }

     public function purchaseoHtml($id)
    {
        $body = [];
        $purchase = Purchase::where('id',$id)->first();
        $details = PurchaseDetails::where('order_no',$purchase->order_no)->get();
        $total_qty = $details->sum('qty');
        $total_gross_wt = $details->sum('gross_wt');
        $total_gld_net_wt = $details->sum('gld_net_wt');
        $total_diamond_wt = $details->sum('diamond_wt');
        $total_stone_wt = $details->sum('stone_wt');
        $total_kwd_amt = $details->sum('kwd_amount');

        $output1='<html>
        <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
        <title>invoice</title>
        <style>
        @page {
          margin: 0px 20px 0px 20px !important;
          padding: 0px 0px 0px 0px !important;
        }
        
        footer {
          position: fixed; 
          bottom: -60px; 
          left: 0px; 
          right: 0px;
          height: 100px; 

      }
        body { font-family: verdana,cambria;} 
        table {
          margin-bottom: .1em;
        }
        th,td {
          padding: 3pt;
        }        
        table.collapse {
          border-collapse: collapse;
          border: 0pt solid #000;  
        }
        
        table.collapse td {
          border: 0pt solid #000;
        }
        .fontsixze{
          font-size:20px;
        }

        table.dollapse {
            border-collapse: collapse;
            border: 0.5px solid #000;  
          }
          
        table.dollapse td {
        border: 0.5px solid #000;
        }
        table.dollapse th {
          // border: 0.5px solid #000;
          background: #2a5686;
          color: white;
          }
        </style>
        
        </head>
        
        <body>

        <table width=100% >
        <tr>
        <td>
        <table width=100% class="collapse"> 
        <thead>
          <tr>
            <th colspan=4></th>
          </tr>
        </thead>
          <tbody>
          <tr>
            <td align=center colspan=4><strong></strong></td>
          </tr>
          <tr>
            <td align=center colspan=4></td>
          </tr>
          <tr>
            <td align=center colspan=4 style="color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </td>
          </tr>
          <tr>
            <td align=center colspan=4>Purchase Invoice </td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table width=100% class="collapse jhk"> 
        <thead>
          <tr class="fontsixze">
            <th colspan=2></th>
          </tr>

        </thead>
          <tbody>
          <tr>
            <td style="font-size:12px"><strong>From,</strong></td>
            <td style="text-align:right;font-size:12px"><strong>Date:</strong>'.date('d/m/Y',strtotime($purchase->created_at)).'</td>
          </tr>
          <tr>
            <td style="font-size:10px;font-size:12px">'.ucwords($purchase->supplier->company_name).'</td>
            <td style="text-align:right;font-size:12px">Reference:'.$purchase->order_no.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">'.$purchase->supplier->address.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">Mobile: +'.$purchase->supplier->mobile.'</td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table style="font-size:10px;height:auto" width=100% class="dollapse" >
        <thead>
            <tr>
            <th height="22px">Sr.No</th>
            <th>Particular</th>
            <th>Qty</th>
            <th>Gold Wt</th>
            <th>G. Net Wt</th>
            <th>DIA Wt</th>
            <th>Stone Wt</th>
            </tr>
        </thead> 
          <tbody>
          ';
          $hell=1;
         foreach($details as $key=>$detail){
           $output1.='<tr>
           <td style="width:7px;height:20px" align=center>'.$i++.'</td>
           <td style="width:40%;">'.$detail->particular.'</td>
           <td align=center>'.$detail->qty.'</td>
           <td align=center>'.$detail->gross_wt.'</td>
           <td align=center>'.$detail->gld_net_wt.'</td>
           <td align=center>'.$detail->diamond_wt.'</td>
           <td align=center>'.$detail->stone_wt.'</td>
         </tr>';
         }
         $output1.='<tr><td><img src="static/white.jpeg" width="20" height="200" style="max-width: none;" alt="No Image"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
         <tr><td colspan="2" align=center height="22px"><strong>Total:</strong></td><td align=center>'.$total_qty.'</td><td align=center>'.$total_gross_wt.'</td><td align=center>'.$total_gld_net_wt.'</td><td align=center>'.$total_diamond_wt.'</td><td align=center>'.$total_stone_wt.'</td></tr>';


          $output1.='      
         
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        </td>
        </tr>
        <tr>
        <td>
        <table width=100% style="font-size:10px;border:0.5px solid #d5d5d5;" class="collapse" > 
          <tbody>

          <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>Total:</strong></td>
          <td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px"colspan="10" align=center>'.$total_kwd_amt.'</td></tr>

          <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>Discount:</strong></td><td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px" colspan="10" align=center>'.$purchase->discount.'</td></tr>

              <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>Sub Total:</strong></td><td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px" colspan="10" align=center>'.($total_kwd_amt - $purchase->discount).'</td></tr>

              <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>VAT – 5%:</strong></td><td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px"colspan="10" align=center>'.$purchase->vat.'</td></tr>

              <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>Freight:</strong></td><td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px"colspan="10" align=center>'.$purchase->freight.'</td>tr>

              <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>Grand Total:</strong></td><td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px" colspan="10" align=center>'.$purchase->grand_total.'</td></tr>

              <tr><td style="width:89%;height:20px;border-bottom:0.5px solid #e9edef;"><strong>Paid Amount:</strong></td><td style="width:11%;border-bottom:0.5px solid #e9edef;border-left:0.5px solid #e9edef;height:20px"colspan="10" align=center>'.$purchase->paid_amount.'</td></tr>

              <tr><td style="width:89%;height:20px"><strong>Due:</strong></td><td style="width:11%;border-left:0.5px solid #e9edef;height:20px" colspan="10" align=center>'.($purchase->grand_total - $purchase->paid_amount).'</td></tr>
          

              
          </tbody>
        </table>
        </td>
        </tr>
        </table>
        
        
        </body> </html>
        ';
        return $output1;

    }


    public function downloadSalesDetailsInvoice($id)
    {
      $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->saleDetailoHtml($id));
        return $pdf->stream();
    }

    public function saleDetailoHtml($id)
    {
      $retailer = SaleDetail::findorfail($id);
      $details = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
        ->select('stock_details_new.*','sales.*')
        ->where('sales.sale_detail_id',$id)
        ->get();

        $retailer = SaleDetail::findorfail($id);
        $gw_total = $details->sum('gw');
        $dw_total = $details->sum('dw');
        $gl = $details->sum('gold_lab');
        $dp = $details->sum('dia_pct');
        $cs = $details->sum('customer_val');
        $totsub_total = $details->sum('sub_total');
        $total_value = $details->sum('tot_val');
        $tot_val_kwd = $details->sum('tot_val_kwd');
        $tot_stm_kwd = $details->sum('stm_kwd');
        $total_tot = $details->sum('total_value_d');
        $tot_tot_amt = $details->sum('tot_amt');

        $output1='<html>
        <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
        <title>Sales Details</title>
        <style>
        @page {
          margin: 0px 30px 0px 30px ;
          padding: 0px 0px 0px 0px ;
          margin-left: 2px;
        }
        footer {
          position: fixed; 
          bottom: -60px; 
          left: 0px; 
          right: 0px;
          height: 100px; 
         }
        body { font-family: verdana,cambria; 
          margin: 0px !important;
          padding: 0px !important;} 

        table {
          margin-bottom: .1em;
        }
        th,td {
          padding: 3pt;
        }        
        table.collapse {
          border-collapse: collapse;
          border: 0pt solid #000;  
        }
        
        table.collapse td {
          border: 0pt solid #000;
        }
        .fontsixze{
          font-size:20px;
        }

        table.dollapse {
            border-collapse: collapse;
            border: 0.5px solid #000;  
          }
          
        table.dollapse td {
        border: 0.5px solid #000;
        }
        table.dollapse th {
          //border: 0.5px solid #000;
          background: #2a5686;
          color: white;
          }
          
        </style>
        
        </head>
        
        <body>
        <table width=100%>
        <tr>
            <td align=center colspan=4 style="color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </td>
          </tr>
          <tr>
        <td>
        <table width=100% class="collapse jhk"> 
        <thead>
          <tr class="fontsixze">
            <th colspan=2></th>
          </tr>

        </thead>
          <tbody>
          <tr>
            <td style="font-size:12px"><strong>To,</strong></td>
            <td style="text-align:right;font-size:12px"><strong>Date:</strong>'.date('d/m/Y',strtotime($retailer->created_at)).'</td>
          </tr>
          <tr>
            <td style="font-size:10px;font-size:12px">'.ucwords($retailer->retailer->company_name).'</td>
            <td style="text-align:right;font-size:12px">Reference: '.$retailer->invoiceno.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">'.$retailer->retailer->address.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">Mobile: +'.$retailer->retailer->mobile.'</td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
          <tr>
        <td> 
        <table style="font-size:8px;height:auto" width=100% class="dollapse" >
        <thead>
            <tr>
            <th>Gold MC</th>
            <th>Dia P/Ct</th>
            <th>Custom</th>
            <th>KWD Convertion</th>
            </tr>
        </thead> 
          <tbody>
          <tr>
           <td align="center"><b>'.$retailer->master_gold.'</b></td>
           <td align="center"><b>'.$retailer->master_dia.'</b></td>
           <td align="center"><b>'.$retailer->master_cus.'</b></td>
           <td align="center"><b>'.$retailer->master_kwd.'</b></td>
         </tr>      
           
          </tbody>
        </table>
        </td>
</tr>
        <tr>
        <td>
        <table style="height:auto;" width=100% class="dollapse" >
        <thead>
            <tr>
            <th style="border:0.1px solid #fff;font-size:7px;">No</td>
            <th style="border:0.1px solid #fff;font-size:7px;">Code</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Design</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Image</th>
            <th style="border:0.1px solid #fff;font-size:7px;">GW</th>
            <th style="border:0.1px solid #fff;font-size:7px;">DW</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Gold+Lab</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Dia&nbsp;Value</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Sub&nbsp;Total</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Custom</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Total&nbsp;($)</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Amount&nbsp;KD</th>
            <th style="border:0.1px solid #fff;font-size:7px;">St.&nbsp;KWD</th>
            <th style="border:0.1px solid #fff;font-size:7px;">T.&nbsp;Amount</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Item</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Clarity</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Grade</th>
            <th style="border:0.1px solid #fff;font-size:7px;">St&nbsp;Pcs</th>
            <th style="border:0.1px solid #fff;font-size:7px;">St&nbsp;Wt</th>
            </tr>
        </thead> 
          <tbody style="font-size:8px;">
          ';
          $hell=1;
         foreach($details as $key=>$dt){

           $output1.='<tr>
           <td>'.$hell++.'</td>
           <td>&nbsp;'.$dt->code.'&nbsp;</td>
            <td>&nbsp;'.$dt->design.'&nbsp;</td>
            <td><img src='.$dt->image.' id="myImg'.$dt->id.'" width="60" height="60" style="max-width: none;" alt="No Image"></td>
            <td>&nbsp;&nbsp;'.$dt->gw.'&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;'.$dt->dw.'&nbsp;&nbsp;</td>
            <td>$ '.$dt->gold_lab.'</td>
            <td>$ '.$dt->dia_pct.'</td>
            <td width=28>$ '.$dt->sub_total.'</td>
            <td width=23>$ '.$dt->customer_val.'</td>
            <td width=26>$ '.$dt->tot_val.'</td>
            <td width=24>'.$dt->tot_val_kwd.' KD</td>
            <td width=24>'.$dt->stm_kwd.' KD</td>
            <td width=28>'.$dt->tot_amt.'KD</td>
            <td width=45>'.$dt->description.'</td>
            <td width=20>'.$dt->dq.'</td>
            <td width=20>'.$dt->cg.'</td>
            <td width=20>'.$dt->csp.'</td>
            <td width=20>'.$dt->csw.'</td>
         </tr>';
        }
         $output1.='
          <tr> <td colspan="4" style="text-align: center; font-weight:bold">Total:</td>
          <td>'.$gw_total.'</td>
          <td>'.$dw_total.'</td>
          <td>$ '.$gl.'</td>
          <td>$ '.$dp.'</td>
          <td>$ '.$totsub_total.'</td>
          <td>$ '.$cs.'</td>
          <td>$ '.$total_value.'</td>
          <td>'.$tot_val_kwd.' KD</td>
          <td>'.$tot_stm_kwd.' KD</td>
          <td>'.$tot_tot_amt.' KD</td>
          <td colspan="5"></td>
          </tr></tbody>
          </table>

          </body> </html>';

        return $output1;
}

  public function downloadReturnInvoice($invoice)
  {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->returnoHtml($invoice));
        return $pdf->stream();

  }

  public function returnoHtml($invoice)
  {
       $retailer = SalesReturn::where('invoice',$invoice)->first();
       $returns = SaleReturnDetalis::join('stock_details_new','stock_details_new.id','=','sales_return_details.stock_id')->select('stock_details_new.*','sales_return_details.*')
            ->where('sales_return_details.invoice',$invoice)
            ->get();

        $gold_wt = $returns->sum('gw');
        $dia_wt = $returns->sum('dw');
        $gold_lab = $returns->sum('gold_lab');
        $dv = $returns->sum('dia_pct');
        $sub_total = $returns->sum('sub_total');
        $custom = $returns->sum('customer_val');
        $total = $returns->sum('tot_val');
        $total_kwd = $returns->sum('tot_val_kwd');
        $total_stm = $returns->sum('stm_kwd');
        $tot_amt = $returns->sum('tot_amt');

      $output1='<html>
        <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
        <title>invoice</title>
        <style>
        @page {
          margin: 0px 20px 0px 20px;
          padding: 0px 0px 0px 0px;
          margin-left: 2px;
      } 
        body {
         font-family: verdana,cambria;
       } 
        table {
          margin-bottom: .1em;
        }
        th,td {
          padding: 3pt;
        }        
        table.collapse {
          border-collapse: collapse;
          border: 0pt solid #000;  
        }
        
        table.collapse td {
          border: 0pt solid #000;
        }
        .fontsixze{
          font-size:20px;
        }

        table.dollapse {
            border-collapse: collapse;
            border: 0.5px solid #000;  
          }
          
        table.dollapse td {
        border: 0.5px solid #000;
        }
        table.dollapse th {
          background: #2a5686;
          color: white;
          }
          #header { 
          position: fixed; 
          width: 100%; 
          top: 0; 
          left: 0; 
          right: 0;
        }
        #footer { 
          position: fixed; 
          width: 100%; 
          bottom: 0; 
          left: 0;
          right: 0;
        }
        .custom-page-start {
            margin-top: 120px;
            margin-bottom: 120px;
        }
        .custom-footer-page-number:after {
          content: counter(page);
        }
        </style>
        
        </head>
        
        <body>
       <div id="header"></div> 
       <div id="footer"></div>

      <div class="custom-page-start" style="page-break-after: always;">
        <table width=100% >
         <tr>
        <td>

        <table width=100% class="collapse"> 
        <thead>
          <tr>
            <th colspan=4></th>
          </tr>
        </thead>
          <tbody>
          <tr>
            <td align=center colspan=4><strong></strong></td>
          </tr>
          <tr>
            <td align=center colspan=4></td>
          </tr>
          <tr>
            <td align=center colspan=4>Sales Return Invoice </td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table width=100% class="collapse jhk"> 
        <thead>
          <tr class="fontsixze">
            <th colspan=2></th>
          </tr>

        </thead>
          <tbody>
          <tr>
            <td style="font-size:12px"><strong>To,</strong></td>
            <td style="text-align:right;font-size:12px"><strong>Date:</strong>'.date('d/m/Y',strtotime($retailer->created_at)).'</td>
          </tr>
          <tr>
            <td style="font-size:10px;font-size:12px">'.ucwords($retailer->retailer->company_name).'</td>
            <td style="text-align:right;font-size:12px">Reference: '.$retailer->invoice.'</td>
           
          </tr>
          <tr>
            <td style="font-size:12px">'.$retailer->retailer->address.'</td>
            <td style="text-align:right;font-size:12px"><b>Sale Return</b></td>
          </tr>
          <tr>
            <td style="font-size:12px">Mobile: +'.$retailer->retailer->mobile.'</td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td> 
        
        <table style="font-size:10px;height:auto" width=100% class="dollapse" >
        <thead>
            <tr>
            <th style="border:0.1px solid #fff;">No.</th>
            <th style="border:0.1px solid #fff;">GW</th>
            <th style="border:0.1px solid #fff;">DW</th>
            <th style="border:0.1px solid #fff;">Gold&nbsp;+&nbsp;Lab</th>
            <th style="border:0.1px solid #fff;">Dia&nbsp;Value</th>
            <th style="border:0.1px solid #fff;">Sub&nbsp;Total</th>
            <th style="border:0.1px solid #fff;">Custom</th>
            <th style="border:0.1px solid #fff;">$&nbsp;Total</th>
            <th style="border:0.1px solid #fff;">Amount&nbsp;KD</th>
            <th style="border:0.1px solid #fff;">Staming</th>
            <th style="border:0.1px solid #fff;">Total&nbsp;Amount</th>
            </tr>
        </thead> 
          <tbody>
          ';
         
           $output1.='<tr>
           <td style="width:2px; height:20px">1</td>
           <td align="center">'.$gold_wt.'</td>
           <td align="center">'.$dia_wt.'</td>
           <td align="center">$ '.$gold_lab.'</td>
           <td align="center">$ '.$dv.'</td>
           <td align="center">$ '.$sub_total.'</td>
           <td align="center">$ '.$custom.'</td>
           <td align="center">'.$total.'&nbsp;KD</td>
           <td align="center">'.$total_kwd.'&nbsp;KD</td>
           <td align="center">'.$total_stm.'&nbsp;KD</td>
           <td align="center">'.$tot_amt.'&nbsp;KD</td>
         </tr>';
         
         $output1.='<tr><td><img src="static/white.jpeg" width="20" height="150" style="max-width: none;" alt="No Image"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>      
           
          </tbody>
        </table>
        </div>
        </td>
        </tr>
        <tr>
        <td>
        </td>
        </tr>
        
        </table>
        </body> </html>
        ';
        return $output1;
  }

  public function downloadReturnViewInvoice($invoice)
  {

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->saleReturnViewoHtml($invoice));
        return $pdf->stream();
    }

    public function saleReturnViewoHtml($invoice)
    {
      $retailer = SalesReturn::where('invoice',$invoice)->first();
      $returns = SaleReturnDetalis::join('stock_details_new','stock_details_new.id','=','sales_return_details.stock_id')->select('stock_details_new.*','sales_return_details.*')
            ->where('invoice',$invoice)
            ->get();
        $gold_wt = $returns->sum('gw');
        $dia_wt = $returns->sum('dw');
        $gold_lab = $returns->sum('gold_lab');
        $dv = $returns->sum('dia_pct');
        $sub_total = $returns->sum('sub_total');
        $custom = $returns->sum('customer_val');
        $total = $returns->sum('tot_val');
        $total_kwd = $returns->sum('tot_val_kwd');
        $total_stm = $returns->sum('stm_kwd');
        $tot_amt = $returns->sum('tot_amt');

        $output1='<html>
        <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
        <title>Sales Details</title>
        <style>
        @page {
          margin: 0px 30px 0px 30px ;
          padding: 0px 0px 0px 0px ;
          margin-left: 2px;
        }
        footer {
          position: fixed; 
          bottom: -60px; 
          left: 0px; 
          right: 0px;
          height: 100px; 
         }
        body { font-family: verdana,cambria; 
          margin: 0px !important;
          padding: 0px !important;} 

        table {
          margin-bottom: .1em;
        }
        th,td {
          padding: 3pt;
        }        
        table.collapse {
          border-collapse: collapse;
          border: 0pt solid #000;  
        }
        
        table.collapse td {
          border: 0pt solid #000;
        }
        .fontsixze{
          font-size:20px;
        }

        table.dollapse {
            border-collapse: collapse;
            border: 0.5px solid #000;  
          }
          
        table.dollapse td {
        border: 0.5px solid #000;
        }
        table.dollapse th {
          //border: 0.5px solid #000;
          background: #2a5686;
          color: white;
          }
          
        </style>
        
        </head>
        
        <body>
        <table width=100%>
        <tr>
            <td align=center colspan=4 style="color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </td>
          </tr>
          <tr>
        <td>
        <table width=100% class="collapse jhk"> 
        <thead>
          <tr class="fontsixze">
            <th colspan=2></th>
          </tr>

        </thead>
          <tbody>
          <tr>
            <td style="font-size:12px"><strong>To,</strong></td>
            <td style="text-align:right;font-size:12px"><strong>Date:</strong>'.date('d/m/Y',strtotime($retailer->created_at)).'</td>
          </tr>
          <tr>
            <td style="font-size:10px;font-size:12px">'.ucwords($retailer->retailer->company_name).'</td>
            <td style="text-align:right;font-size:12px">Reference: '.$retailer->invoice.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">'.$retailer->retailer->address.'</td>
          </tr>
          <tr>
            <td style="font-size:12px">Mobile: +'.$retailer->retailer->mobile.'</td>
          </tr>
          </tbody>
        </table>
        </td>
        </tr>

        <tr>
        <td>
        <table style="height:auto;" width=100% class="dollapse" >
        <thead>
            <tr>
            <th style="border:0.1px solid #fff;font-size:7px;">No</td>
            <th style="border:0.1px solid #fff;font-size:7px;">Code</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Design</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Image</th>
            <th style="border:0.1px solid #fff;font-size:7px;">GW</th>
            <th style="border:0.1px solid #fff;font-size:7px;">DW</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Gold+Lab</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Dia&nbsp;Value</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Sub&nbsp;Total</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Custom</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Total&nbsp;($)</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Amount&nbsp;KD</th>
            <th style="border:0.1px solid #fff;font-size:7px;">St.&nbsp;KWD</th>
            <th style="border:0.1px solid #fff;font-size:7px;">T.&nbsp;Amount</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Item</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Clarity</th>
            <th style="border:0.1px solid #fff;font-size:7px;">Grade</th>
            <th style="border:0.1px solid #fff;font-size:7px;">St&nbsp;Pcs</th>
            <th style="border:0.1px solid #fff;font-size:7px;">St&nbsp;Wt</th>
            </tr>
        </thead> 
          <tbody style="font-size:8px;">
          ';
          $hell=1;
         foreach($returns as $key=>$dt){

           $output1.='<tr>
           <td>'.$hell++.'</td>
           <td>&nbsp;'.$dt->code.'&nbsp;</td>
            <td>&nbsp;'.$dt->design.'&nbsp;</td>
            <td><img src='.$dt->image.' id="myImg'.$dt->id.'" width="60" height="60" style="max-width: none;" alt="No Image"></td>
            <td>&nbsp;&nbsp;'.$dt->gw.'&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;'.$dt->dw.'&nbsp;&nbsp;</td>
            <td>$ '.$dt->gold_lab.'</td>
            <td>$ '.$dt->dia_pct.'</td>
            <td width=28>$ '.$dt->sub_total.'</td>
            <td width=23>$ '.$dt->customer_val.'</td>
            <td width=26>$ '.$dt->tot_val.'</td>
            <td width=24>'.$dt->tot_val_kwd.' KD</td>
            <td width=24>'.$dt->stm_kwd.' KD</td>
            <td width=28>'.$dt->tot_amt.'KD</td>
            <td width=45>'.$dt->description.'</td>
            <td width=20>'.$dt->dq.'</td>
            <td width=20>'.$dt->cg.'</td>
            <td width=20>'.$dt->csp.'</td>
            <td width=20>'.$dt->csw.'</td>
         </tr>';
        }
         $output1.='
          <tr> <td colspan="4" style="text-align: center; font-weight:bold">Total:</td>
          <td>'.$gold_wt.'</td>
          <td>'.$dia_wt.'</td>
          <td>$ '.$gold_lab.'</td>
          <td>$ '.$dv.'</td>
          <td>$ '.$sub_total.'</td>
          <td>$ '.$custom.'</td>
          <td>$ '.$total.'</td>
          <td>'.$total_kwd.' KD</td>
          <td>'.$total_stm.' KD</td>
          <td>'.$tot_amt.' KD</td>
          <td colspan="5"></td>
          </tr></tbody>
          </table>

          </body> </html>';

        return $output1;
}

}
