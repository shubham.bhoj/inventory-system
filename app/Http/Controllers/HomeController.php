<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesReturn;
use App\Models\Barcode;
use App\Models\BarcodeSize;
use App\Models\MoneyTransfer;
use App\Models\Payment;
use App\Models\Purchase;
use App\Models\PurchaseDetails;
use App\Models\Account;
use App\Models\Setting;
use App\Models\Supplier;
use App\Models\Retailer;
use App\Models\Country;
use App\Models\Company;
use App\Models\Stock;
use App\Models\StockDetail;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\History;
use Illuminate\Database\Eloquent\Builder;
use Log;
use DB;
use Validator;
use Importer;
use PHPExcel; 
use PHPExcel_IOFactory;
const UPDATED = "Updated Successfully!";
const FAILED = "Failed, Please Try again!";
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
//=========================== sale  =========================
        $sales = SaleDetail::whereDate('created_at', '>=', date('Y-m-d'))->whereDate('created_at', '<=' ,date('Y-m-d'))
        ->get();
        $tot_sale = $sales->sum('total');

//=========================== Purchase  =========================
        $purchase = Purchase::whereDate('created_at', '>=', date('Y-m-d'))->whereDate('created_at', '<=' ,date('Y-m-d'))->get();
        $tot_purchase = $purchase->sum('grand_total');

//=========================== Profit  =========================
        $data = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
                ->select('stock_details_new.*','sales.*')
                ->where('stock_details_new.used',true)
                ->whereDate('sales.created_at', '>=', date('Y-m-d'))->whereDate('sales.created_at', '<=' ,date('Y-m-d'))->get();

        $total_sale = $data->sum('tot_amt');
        $stock_rate = $data->sum('final_total');
        $total_profit = $total_sale - $stock_rate;


//=========================== sale Return =========================
        $return = SalesReturn::whereDate('created_at', '>=', date('Y-m-d'))->whereDate('created_at', '<=' ,date('Y-m-d'))->get();
        $return_cnt = $return->sum('return_amt');

//=========================== Best saller  =========================
        $sales = SaleDetail::whereDate('created_at', '>=', date('Y-m-d'))->whereDate('created_at', '<=' ,date('Y-m-d'))
        ->get();
        $tot_sale = $sales->sum('total');


//=========================== Best saller month =========================
        $best_selling_qty = SaleDetail::select(DB::raw('retailer_id,sum(total) as total_price'))->whereDate('created_at', '>=', date('Y-m-01'))->whereDate('created_at', '<=' ,date('Y-m-31'))->groupBy('retailer_id')->orderBy('total_price','desc')->paginate(5);

         $yearly_best_selling_qty = Sale::select(DB::raw('rtl_id, count(stock_id) as total_qty'))->whereDate('created_at', '>=',  date("Y").'-01-01')->whereDate('created_at', '<=' ,date("Y").'-12-31')->groupBy('rtl_id')->orderBy('total_qty','desc')->paginate(5);

        $yearly_best_selling_price = SaleDetail::select(DB::raw('retailer_id,sum(total) as total_price'))->whereDate('created_at', '>=',  date("Y").'-01-01')->whereDate('created_at', '<=' ,date("Y").'-12-31')->groupBy('retailer_id')->orderBy('total_price','desc')->paginate(5);

        return view('home',compact('tot_sale','tot_purchase','total_profit','return_cnt','best_selling_qty','yearly_best_selling_qty','yearly_best_selling_price'));
    }


    public function dashboardFilter($start_data, $end_date)
    {
//=========================== sale  =========================
        $sales = SaleDetail::whereDate('created_at', '>=', $start_data)->whereDate('created_at', '<=' ,$end_date)
        ->get();
        $result[] = $sales->sum('total');


//=========================== sales Return =========================
         $return = SalesReturn::whereDate('created_at', '>=', $start_data)->whereDate('created_at', '<=' ,$end_date)->get();
         $result[] = $return->sum('return_amt');


//=========================== Purchase  =========================
         $purchase = Purchase::whereDate('created_at', '>=', $start_data)->whereDate('created_at', '<=' ,$end_date)->get();
         $result[] = $purchase->sum('grand_total');


//=========================== Profit  =========================
         $data = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
                ->select('stock_details_new.*','sales.*')
                ->where('stock_details_new.used',true)
                 ->whereDate('sales.created_at', '>=', $start_data)->whereDate('sales.created_at', '<=' ,$end_date)->get();

         $total_sale = $data->sum('tot_amt');
         $stock_rate = $data->sum('final_total');
         $result[] = $total_sale - $stock_rate;
       
        return $result;
    }

    public function purchasers_list()
    {
        return view('products');
    }


    //------------------------ Retailers  -----------------------------------------

    public function retailers_list()
    {
        //$data = Retailer::where('retailer_or_walking',1)->get();
        $data = Retailer::get();
        return view('retailers',compact('data'));
    }

    public function retailers_view(Request $request)
    {
        $data = Retailer::find($request->id);
        $secondData = SaleDetail::where('retailer_id',$request->id)->get();
        $due = $secondData->sum('due');
        $total = $secondData->sum('total');
        $accounts = Account::where('is_active','1')->get();
        return view('retailers_view',compact('data','secondData','total','due','accounts'));
    }

    public function retailers_details(Request $request)
    {
        $countries = Country::get();
        $data="";
        if(isset($request->id) && !empty($request->id))
        {
        $data = Retailer::find($request->id);
        }
        return view('retailers_details',compact('data','countries'));
    }

    public function retailers_upsert(Request $request)
    {
        try {
        // if(isset($request->id) && !empty($request->id))
        // {
        //     $data = Retailer::find($request->id);
        //     if()
        //     $validator = Validator::make($request->all(), [
        //         'email' => 'unique:retailers,email,'.$data->id.',id',
        //     ]);
        // } else {
        //     $validator = Validator::make($request->all(), [
        //         'email' => 'unique:retailers',
        //     ]);
        //     $data = new Retailer; 
        // }
        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator)->withInput();
        //}
         $data = new Retailer; 
        $data->company_name = $request->company_name;
        $data->contact_person = $request->contact_person;
        $data->address = $request->address;
        $data->civil_no = $request->civil_no;
        $data->mobile = $request->mobile;
        $data->mobile_code = $request->mobile_code;
        $data->email = $request->email;
        $data->save();
        if($data)
        {
            \Session::flash('success', UPDATED);
            return redirect()->route('retailers_list');
        } else {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    } catch (\Exception $e)
    {
        \Session::flash('error', FAILED);
        return redirect()->back();
    }
    }



    //<!------------------------------------- Suppliers ------------------------------>


    public function suppliers_list()
    {
        $data = Supplier::get();
        return view('suppliers',compact('data'));
    }

    public function suppliers_view(Request $request)
    {
        $data = Supplier::find($request->id);
        $secondData = Purchase::where('supplier_id',$request->id)->get();
        $due = $secondData->sum('grand_total') - $secondData->sum('paid_amount');
        $total = $secondData->sum('grand_total');
        $accounts = Account::where('is_active','1')->get();
        return view('suppliers_view',compact('data','secondData','due','total','accounts'));
    }

    public function suppliers_details(Request $request)
    {
        $countries = Country::get();
        $data="";
        if(isset($request->id) && !empty($request->id))
        {
        $data = Supplier::find($request->id);
        }
        return view('suppliers_details',compact('data','countries'));
    }

    public function suppliers_upsert(Request $request)
    {
        
        try {
        // if(isset($request->id) && !empty($request->id))
        // {
        //     $data = Supplier::find($request->id);
        //     $validator = Validator::make($request->all(), [
        //         'email' => 'unique:suppliers,email,'.$data->id.',id',
        //     ]);
        // } else {
        //     $validator = Validator::make($request->all(), [
        //         'email' => 'unique:suppliers',
        //     ]);
        //     $data = new Supplier; 
        // }
        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }

        $data = new Supplier; 
        $code = substr($request->company_name, 0, 2)."-".rand(1111,9999);
        $data->supplier_code = $code;
        $data->company_name = $request->company_name;
        $data->contact_person = $request->contact_person;
        $data->address = $request->address;
        $data->mobile = $request->mobile;
        $data->mobile_code = $request->mobile_code;
        $data->email = $request->email;
        $data->save();
        if($data)
        {
            \Session::flash('success', UPDATED);
            return redirect()->route('suppliers_list');
        } else {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    } catch (\Exception $e)
    {
        \Session::flash('error', FAILED);
        return redirect()->back();
    }
    }
    
    public function stocks_list()
    {
        // leftjoin('stocks','stocks.id','stock_details_new.stock_id')
        //         ->leftjoin('suppliers','suppliers.id','stocks.supplier_id')
        //         ->select('stock_details_new.*','suppliers.company_name','mobile','mobile_code')
        //         ->

        $data = StockDetail::where('stock_details_new.used',false)
                ->orderBy('id','desc')
                ->get();
        $settings_data = Setting::first();
        return view('stocks',compact('data','settings_data'));
    }

    public function stocks_details(Request $request)
    {
        $suppliers = Supplier::get();
        $settings = Setting::first();
        return view('stocks_details',compact('suppliers','settings'));
    }

    public function print_barcode(Request $request){

        $ids = $request->ids;
        $stockAll = StockDetail::whereIn('id',explode(",",$ids))->get();
        $get_barcode = Barcode::where('status',1)->first();
        $company = Company::first();
        $atr = "<table class='table' style='width:40%;border:none;font-weight: 700;'><tbody>";

        foreach($stockAll as $stock)
        {

           $atr.= '<tr style="border:none;padding:10px;"><td style="border:none;text-align: center;">'.$stock->design.'</td><td style="border:none;text-align: center;">'.$company->barcode_com.'</td></tr><tr style="border:none;"><td style="border:none;text-align: center;">GW '.$stock->gw.'</td><td style="border:none;width: 120px;height: 22px;"><img src="data:image/png;base64,' .\DNS1D::getBarcodePNG($stock->barcode, $get_barcode->barcode).'"></td></tr><tr style="border:none;"><td style="border:none;padding-bottom: 30px;text-align: center;">DW '.$stock->dw.'</td><td style="border:none;padding-bottom: 30px;text-align: center;">'.$stock->barcode.'</td></tr>';
        }
        $atr.= "</tbody></table>";
        
        return response()->json($atr);

    }

    public function stocks_upsert(Request $request)
    {   
        if ($request->hasFile('excel')) {
            if ($request->file('excel')->isValid()){
                $validator = Validator::make($request->all(), [
                    'excel' => 'mimes:csv,xlsx,xls|max:20000|file',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }  

                
                $imageArray[] = "";   
                $file = $request->file('excel');
                $extension = $file->getClientOriginalExtension();

                $destinationPath = public_path('upload/stock'); // upload path
                $fileName1 = rand(11111, 99999) . time() . '.' . $extension;
                $file->move($destinationPath, $fileName1); 
                $excel = Importer::make('Excel');
                $filePath = $destinationPath."/".$fileName1;
                $excel->load($filePath);
                $collection = $excel->getCollection();
                $settings = Setting::first();  
                $objPHPExcel = PHPExcel_IOFactory::load($filePath);
                $i = 0;
                foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $drawing) {
                if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
                ob_start();
                call_user_func(
                    $drawing->getRenderingFunction(),
                    $drawing->getImageResource()
                );

                $imageContents = ob_get_contents();
                ob_end_clean();
                $extension = 'png';
            } else {
                $zipReader = fopen($drawing->getPath(),'r');

                $imageContents = '';
            while (!feof($zipReader)) {
                $imageContents .= fread($zipReader,1024);
            }   
            fclose($zipReader);
            $extension = $drawing->getExtension();
        }
        $myFileName = rand(11111, 99999).time().'.'.$extension;
        $imageArray[] = $myFileName;
        
         file_put_contents('public/'.$myFileName,$imageContents);
    
    }          
                for($i=1;$i<count($collection);$i++)
                {

                    $barcode = '0000'.rand(111,9999);
                    $code = $collection[$i][1];
                    $design = $collection[$i][2];
                    $image = $imageArray[$i];
                    $gw = $collection[$i][4];
                    $dw = $collection[$i][5];
                    $ngw = $collection[$i][6];
                    $dp = $collection[$i][7];
                    $dv = $collection[$i][8];
                    $total = $collection[$i][9]; 
                    $custom = $collection[$i][10]; 
                    $total_value_d = $collection[$i][11];
                    $total_value_kwd = $collection[$i][12];
                    $stamping = $collection[$i][13];
                    $final_total = $collection[$i][14];
                    $decs = $collection[$i][15];
                    $dq = $collection[$i][16];
                    $cg = $collection[$i][17];
                    $csp = $collection[$i][18];
                    $csw = $collection[$i][19];
                    
                  
                    $data_details = new StockDetail; 
                    $data_details->barcode = $barcode;
                    $data_details->code = $code;
                    $data_details->design = $design; 
                    $data_details->image = $image; 
                    $data_details->gw = $gw;
                    $data_details->dw = $dw;  
                    $data_details->ngw = $ngw; 
                    $data_details->dp = $dp; 
                    $data_details->dv = $dv; 
                    $data_details->total = $total;
                    $data_details->customs = $custom;
                    $data_details->total_value_d = $total_value_d;
                    $data_details->total_value_kwd = $total_value_kwd;
                    $data_details->stamping = $stamping;
                    $data_details->final_total = $final_total;
                    $data_details->description = $decs;
                    $data_details->dq = $dq;
                    $data_details->cg = $cg; 
                    $data_details->csp = $csp; 
                    $data_details->csw = $csw; 
                    $data_details->save();


                     //  $last_barcode = StockDetail::max('barcode');

                // if(empty($last_barcode) || $last_barcode == null)
                // {
                    
                // }else{
                //     var_dump($last_barcode);
                //     $after = $last_barcode + $add;
                //     // echo 'after'.$after."<br>";
                //     $barcode= '0000'.$after;
                //     // echo "barcode".$barcode.'<br>';

                // }
           
                    // $image = $imageArray[$i];
                    // $design = $collection[$i][2];
                    // $pcs = $collection[$i][3];
                    // $gq = $collection[$i][4];
                    // $gw = $collection[$i][5];
                    // $ngw = $collection[$i][6];
                    // $dp = $collection[$i][7];
                    // $dw = $collection[$i][8];
                    // $dq = $collection[$i][9];
                    // $cg = $collection[$i][10];
                    // $csp = $collection[$i][11];
                    // $csw = $collection[$i][12];
                    
                  
                    // $data_details = new StockDetail; 
                    // $data_details->design = $design; 
                    // $data_details->image = $image; 
                    // $data_details->pcs = $pcs; 
                    // $data_details->gq = $gq; 
                    // $data_details->gw = $gw; 
                    // $data_details->ngw = $ngw; 
                    // $data_details->dp = $dp; 
                    // $data_details->dw = $dw; 
                    // $data_details->dq = $dq; 
                    // $data_details->cg = $cg; 
                    // $data_details->csp = $csp; 
                    // $data_details->csw = $csw; 
                    // //Calculations
                    
                    // $gmc = $data_details->gmc = $gw*$settings->gold_mc;
                    // $dmc = $data_details->dmc = $dw*$settings->diamond_mc;
                    // $total = $data_details->total = $gmc+$dmc;
                    // $customs = $data_details->customs = ($total*$settings->custom_charge)/100;
                    // $total_value_d = $data_details->total_value_d = $total+$customs;
                    // $total_value_kwd = $data_details->total_value_kwd = $total_value_d*$settings->purchase_rate_kwd;
                    // $stamping = $data_details->stamping = $settings->stamping_charge;
                    // $final_total = $data_details->final_total = $total_value_kwd+$stamping;
                    // $data_details->save();
                 }

                if($data_details)
                    {
                        \Session::flash('success', UPDATED);
                        return redirect()->route('stocks_list');
                    } else {
                        \Session::flash('error', FAILED);
                        return redirect()->back();
                    }
            }
        } 

        else {
        try {
            $fileName1="";
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $destinationPath = public_path(); // upload path
            $fileName1 = rand(11111, 99999) . time() . '.' . $extension;
            $file->move($destinationPath, $fileName1); 
                }}
           
            $barcode = '0000'.rand(111,9999);
            $data_details = new StockDetail; 
            $data_details->barcode = $barcode;
            $data_details->code = $request->code;
            $data_details->design = $request->design; 
            $data_details->image = $fileName1; 
            $data_details->gw = $request->gw;
            $data_details->dw = $request->dw;  
            $data_details->ngw = $request->gl; 
            $data_details->dp = $request->dp; 
            $data_details->dv = $request->dv; 
            $data_details->total = $request->st;
            $data_details->customs = $request->cus;
            $data_details->total_value_d = $request->tot_vl;
            $data_details->total_value_kwd = $request->tot_val_kwd;
            $data_details->stamping = $request->stm;
            $data_details->final_total = $request->final;
            $data_details->description = $request->item;
            $data_details->dq = $request->cl;
            $data_details->cg = $request->grd; 
            $data_details->csp = $request->pcs; 
            $data_details->csw = $request->stone_wt; 
            $data_details->save();

            // $data_details = new StockDetail;
            // $data_details->image = $fileName1; 
            // $data_details->design = $request->design; 
            // $data_details->description = $request->description; 
            // $data_details->pcs = $request->pcs; 
            // $data_details->gq = $request->gq; 
            // $data_details->gw = $request->gw; 
            // $data_details->ngw = $request->ngw; 
            // $data_details->dp = $request->dp; 
            // $data_details->dw = $request->dw; 
            // $data_details->dq = $request->dq; 
            // $data_details->cg = $request->cg; 
            // $data_details->csp = $request->csp; 
            // $data_details->csw = $request->csw; 
            // $data_details->gmc = (float)str_replace(',','',$request->gmc); 
            // $data_details->dmc = (float)str_replace(',','',$request->dmc); 
            // $data_details->total = (float)str_replace(',','',$request->total); 
            // $data_details->customs = (float)str_replace(',','',$request->customs); 
            // $data_details->total_value_d = (float)str_replace(',','',$request->total_value_d); 
            // $data_details->total_value_kwd = (float)str_replace(',','',$request->total_value_kwd); 
            // $data_details->stamping = $request->stamping; 
            // $data_details->final_total = (float)str_replace(',','',$request->final_total); 
            // $data_details->save();
            if($data_details)
            {
                \Session::flash('success', UPDATED);
                return redirect()->route('stocks_list');
            } else {
                \Session::flash('error', FAILED);
                return redirect()->back();
            }
        } catch (\Exception $e)
        {   
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    }
    }


    public function stocks_history()
    {
        $data = Stock::leftjoin('suppliers','stocks.supplier_id','suppliers.id')
        ->select('stocks.*','suppliers.company_name','suppliers.mobile','suppliers.mobile_code',DB::raw(DB::raw('DATE_FORMAT(stocks.created_at, "%d-%b-%Y") as createdAt')))
        ->get();
        return view('stocks_history',compact('data'));
    }


    public function stocks_history_view(Request $request)
    {
        $data = StockDetail::where('stock_id',$request->id)->get();
        $gmc = $data->sum('gmc');
        $dmc = $data->sum('dmc');
        $total = $data->sum('total');
        $customs = $data->sum('customs');
        $total_value_d = $data->sum('total_value_d');
        $total_value_kwd = $data->sum('total_value_kwd');
        $stamping = $data->sum('stamping');
        $final_total = $data->sum('final_total');
        return view('stocks_history_view',compact('data','gmc','dmc','total','customs','total_value_d','total_value_kwd','stamping','final_total'));
    }

    public function stock_delete($id)
    {
        $detail = StockDetail::findorfail($id);
        if($detail->delete())
        {
            @unlink(public_path($detail->image));
        }

        \Session::flash('success', "Stock Delete Successfully");
        return redirect()->route('stocks_list');
    }

    public function delete_stock(Request $request)
    {
        $ids = $request->ids;
        $arr = '';
        $deleteAll = StockDetail::whereIn('id',explode(",",$ids))->get();
        foreach($deleteAll as $data)
        {
            if($data->delete())
            {
                @unlink(public_path($data->image)); 
            }
        }
        return response()->json(['success'=>"Stock Deleted successfully."]);
    }
    
    public function settings()
    {
        $data = Setting::first();
        return view('settings',compact('data'));
    }

    public function company_details()
    {
        $data = Company::first();
        return view('company_details',compact('data'));
    }

    public function settings_update(Request $request)
    {
        try {
            $data = Setting::find($request->id);
            $data->custom_charge = $request->custom_charge;
            $data->stamping_charge = $request->stamping_charge;
            $data->sales_rate_kwd = $request->sales_rate_kwd;
            $data->purchase_rate_kwd = $request->purchase_rate_kwd;
            $data->vat = $request->vat;
            // $data->gold_lab = $request->gold_lab;
            $data->diamond_mc = $request->diamond_mc;
            $data->gold_mc = $request->gold_mc;
            $data->save();
            if($data)
            {
                \Session::flash('success', UPDATED);
                return redirect()->back();
            } else {
                \Session::flash('error', FAILED);
                return redirect()->back();
            }
    }
    catch (\Exception $e)
        {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    }

    public function company_details_update(Request $request)
    {
        try {
            $data = Company::find($request->id);
            if ($request->has('logo')) {
                $file = $request->file('logo');
                $extension = $file->getClientOriginalExtension();
                $destinationPath = public_path('logo'); // upload path
                $fileName1 = rand(11111, 99999) . time() . '.' . $extension;
                $file->move($destinationPath, $fileName1); 
                $data->logo = $fileName1;
            }
            $data->company_name = $request->company_name;
            $data->address = $request->address;
            $data->contact_phone = $request->contact_phone;
            $data->contact_email = $request->contact_email;
            $data->cr_no = $request->cr_no;
            $data->website = $request->website;
            $data->barcode_com = $request->barcode;
            $data->save();
            if($data)
            {
                \Session::flash('success', UPDATED);
                return redirect()->back();
            } else {
                \Session::flash('error', FAILED);
                return redirect()->back();
            }
    }
    catch (\Exception $e)
        {
            \Session::flash('error', FAILED);
            return redirect()->back();
        }
    }



//----------------------------- Sales ---------------------------------

    public function sales_list()
    {
        $sales_details = SaleDetail::leftjoin('retailers','retailers.id','sale_details.retailer_id')
        ->select('sale_details.*','retailers.company_name','retailers.mobile','retailers.mobile_code')
        ->orderBy('id','desc')
        ->get();
        $settings = Setting::first();
        $accounts = Account::where('is_active','1')->get();
        return view('sales',compact('sales_details','settings','accounts'));
    }

    public function sales_view(Request $request)
    {
        $data = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
        ->select('stock_details_new.*','sales.*')
        ->where('sales.sale_detail_id',$request->id)
        ->get();
        $id = $request->id;
        $retailer = SaleDetail::findorfail($id);
        $gw_total = $data->sum('gw');
        $dw_total = $data->sum('dw');
        $gl = $data->sum('gold_lab');
        $dp = $data->sum('dia_pct');
        $totsub_total = $data->sum('sub_total');
        $cs = $data->sum('customer_val');
        $total_value = $data->sum('tot_val');
        $tot_val_kwd = $data->sum('tot_val_kwd');
        $tot_stm_kwd = $data->sum('stm_kwd');
        $total_tot = $data->sum('tot_amt');

        return view('sales_view',compact('data','gw_total','dw_total','totsub_total','total_value','total_tot','id','retailer','gl','dp','cs','tot_val_kwd','tot_stm_kwd'));
    }

    public function sales_details()
    {
        $retailers = Retailer::get();
        $stocks = StockDetail::where('used',false)->get();
        $countries = Country::get();
        $settings = Setting::first();
        $accounts = Account::where('is_active','1')->get();
        // $stocks = StockDetail::whereHas('stock' , fn(Builder $query)=>$query->whereHas('sale'))->get();
        return view('sales_details',compact('retailers','stocks','settings','countries','accounts'));
    }

    public function sales_add(Request $request)
    {

        $amount = 0;
        $data = new SaleDetail; 
        if($request->retailer_or_walking=='retailer') {
            $data->retailer_id = $request->retailer_id;
            $d_retailer = $request->retailer_id;
        }
        else {
            $retailer_data = new Retailer;
            $retailer_data->company_name = $request->customer_name;
            $retailer_data->mobile_code = $request->mobile_code;
            $retailer_data->mobile = $request->mobile_no;
            $retailer_data->address = $request->address;
            $retailer_data->civil_no = $request->civil_no;
            $retailer_data->retailer_or_walking = 2;
            $retailer_data->save();
            $data->retailer_id = $retailer_data->id;
            $d_retailer = $retailer_data->id;
        }  
        if($request['account_first'] != null && $request['cash_first'] != 0){
            $amount = $request['cash_first'];
        }
        if($request['account_sec'] != null && $request['cash_sec'] != 0)
        {
            $amount = $amount + $request['cash_sec'];
        }

            $orderNo = 'sr-'.date("Ymd").'-'.date("his");
            $data->master_gold = $request->mas_gold;
            $data->master_dia = $request->mas_dia;
            $data->master_cus = $request->mas_cus;
            $data->master_kwd = $request->mas_kwd;
            $data->master_stm = $request->mas_stmp_kwd;
            $data->discount = $request->discount;
            $data->freight = $request->freight;
            $data->sub_total = $request->tot_sub_total;
            $data->vat = $request->vat;
            $data->total = $request->total;
            $data->amount = $amount;
            $data->due = $request->due;
            $data->note = $request->note; 
            $data->invoiceno = $orderNo; 
            $data->save();

            $sale_detail_id = $data->id;
            $invoice = 'spr-'.date("Ymd").'-'.date("his");
            if($request['account_first'] != null && $request['cash_first'] != 0){
                $history = new History;
                $history->invoice_no = $invoice;
                $history->sale_details_id = $sale_detail_id;
                $history->account_id = $request['account_first'];
                $history->tot_amount = $request['amount'];
                $history->amount = $request['cash_first'];
                if($history->save())
                {
                    $account = Account::findorfail($request['account_first']);
                    $bal = $account->balance + $request['cash_first'];
                    $ini_bl = $account->initial_balance + $request['cash_first'];
                    $account->balance = $bal;
                    $account->initial_balance = $ini_bl;
                    $account->save();
                }
            }
            if($request['account_sec'] != null && $request['cash_sec'] != 0)
            {
                $history = new History;
                $history->invoice_no = $invoice;
                $history->sale_details_id = $sale_detail_id;
                $history->account_id = $request['account_sec'];
                $history->tot_amount = $request['due_first'];
                $history->amount = $request['cash_sec'];
                if($history->save())
                {
                    $account_sec = Account::findorfail($request['account_sec']);
                    $bal_sec = $account_sec->balance + $request['cash_sec'];
                    $ini_bl_sec = $account_sec->initial_balance + $request['cash_sec'];
                    $account_sec->balance = $bal_sec;
                    $account_sec->initial_balance = $ini_bl_sec;
                    $account_sec->save();
                }
            }
            
            for($i=0;$i<count($request->stock_id);$i++) {
                $data_details = New Sale;
                $data_details->rtl_id = $d_retailer;
                $data_details->sale_detail_id = $sale_detail_id;
                $data_details->stock_id = $request->stock_id[$i];
                $data_details->gold_per = $request->gold_per[$i];
                $data_details->gold_lab = $request->gold_lab[$i];
                $data_details->dia_per = $request->dia_per[$i];
                $data_details->dia_pct = $request->dia_pct[$i];
                $data_details->sub_total = $request->sub_total[$i];
                $data_details->customer_per = $request->custom[$i];
                $data_details->customer_val = $request->cus_value[$i];
                $data_details->tot_val = $request->tot_val[$i];
                $data_details->kwd_conver = $request->kwd_convertion[$i];
                $data_details->tot_val_kwd = $request->total_kwd_val[$i];
                $data_details->stm_kwd = $request->stamping[$i];
                $data_details->tot_amt = $request->total_amount[$i];
                $data_details->save();
                $stockdetails=StockDetail::find($request->stock_id[$i]);
                $stockdetails->used=1;
                $stockdetails->save();
            }
            
            \Session::flash('success', "Sale Successfully.");
            return redirect()->to(route('sales_list'));
    }

    public function add_order(Request $request)
    {
        $stock = StockDetail::where('id',$request->id)->first();
        $gw_total = 0;
        $dw_total = 0;
        $gmc_total = 0;
        $dmc_total = 0;
        $total_total = 0;
        $customs_total = 0;
        $total_value_d_total = 0;
        $total_value_kwd_total = 0;
        $final_total = 0;
        if(isset($request->idarray) && count($request->idarray)>0){
        $stocktotal = StockDetail::whereIn('id',$request->idarray)->get();
        $gw_total = $stocktotal->sum('gw');
        $dw_total = $stocktotal->sum('dw');
        $gmc_total = $stocktotal->sum('gmc');
        $dmc_total = $stocktotal->sum('dmc');
        $total_total = $stocktotal->sum('total');
        $total_value_kwd_total = $stocktotal->sum('total_value_kwd');
        $total_value_d_total = $stocktotal->sum('total_value_d');
        $customs_total = $stocktotal->sum('customs');
        $final_total = $stocktotal->sum('final_total');
        };
        $settings_data = Setting::first();
        return response()->json([
            'data' => $stock,
            'tot_gw' => $gw_total,
            'tot_dw' => $dw_total,
            'customs_total' => $customs_total,
            'dmc_total' => $dmc_total,
            'total_total' => $total_total,
            'total_value_d_total' => $total_value_d_total,
            'total_value_kwd_total' => $total_value_kwd_total,
            'gmc_total' => $gmc_total,
            'final_total' => $final_total,
            'settings_data' => $settings_data,
        ]);
    }



    public function sales_edit($id)
    {
        $sale_details = array();
        $retailers = Retailer::get();
        $stocks = StockDetail::where('used',false)->get();
        $countries = Country::get();
        $settings = Setting::first();
        $accounts = Account::where('is_active','1')->get();
        $details = SaleDetail::findorfail($id);
        $sales = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
        ->select('stock_details_new.*','sales.*')
        ->where('sales.sale_detail_id',$id)
        ->get();
        $gw_total = $sales->sum('gw');
        $dw_total = $sales->sum('dw');
        $gl = $sales->sum('gold_lab');
        $dp = $sales->sum('dia_pct');
        $sub_total = $sales->sum('sub_total');
        $cs = $sales->sum('customer_val');
        $total_value_d_total = $sales->sum('tot_val');
        $total_value_kwd_total = $sales->sum('tot_val_kwd');
        $total_amt = $sales->sum('tot_amt');
        $tot_stm_kwd = $sales->sum('stm_kwd');

        return view('sales_edit_details',compact('retailers','stocks','settings','countries','accounts','sales','details','gw_total','gl','dp','cs','tot_stm_kwd','dw_total','sub_total','total_value_kwd_total','total_value_d_total','total_amt'));
    }

    public function sales_editPost(Request $request,$id)
    {
        $data = SaleDetail::findorfail($id); 
        $data->discount = $request->discount;
        $data->freight = $request->freight;
        $data->sub_total = $request->sub_total;
        $data->vat = $request->vat;
        $data->total = $request->total;
        $data->due = $request->total - $data->amount;
        $data->save();
        \Session::flash('success', UPDATED);
        return redirect()->to(route('sales_list'));
    }


    public function sale_add_payment(Request $request)
    {
        $id = $request['id'];
        $detail = SaleDetail::findorfail($id);
        return response()->json($detail);
    }

    public function post_sale_payment(Request $request)
    {
            $id = $request['id'];
            $invoice = 'spr-'.date("Ymd").'-'.date("his");
            if($request['account_first'] != null && $request['cash_first'] != 0){
                $history = new History;
                $history->invoice_no = $invoice;
                $history->sale_details_id = $id;
                $history->account_id = $request['account_first'];
                $history->tot_amount = $request['amount'];
                $history->amount = $request['cash_first'];
                if($history->save())
                {
                    $account = Account::findorfail($request['account_first']);
                    $bal = $account->balance + $request['cash_first'];
                    $ini_bl = $account->initial_balance + $request['cash_first'];
                    $account->balance = $bal;
                    $account->initial_balance = $ini_bl;
                    if($account->save())
                    {
                        $saleDetail = SaleDetail::findorfail($id);
                        $saleDetail->amount = $saleDetail->amount + $request['cash_first'];
                        $saleDetail->due = $saleDetail->due - $request['cash_first'];
                        $saleDetail->save();
                    }
                }
            }
            if($request['account_sec'] != null && $request['cash_sec'] != 0)
            {
                $history = new History;
                $history->invoice_no = $invoice;
                $history->sale_details_id = $id;
                $history->account_id = $request['account_sec'];
                $history->tot_amount = $request['due_first'];
                $history->amount = $request['cash_sec'];
                if($history->save())
                {
                    $account_sec = Account::findorfail($request['account_sec']);
                    $bal_sec = $account_sec->balance + $request['cash_sec'];
                    $ini_bl_sec = $account_sec->initial_balance + $request['cash_sec'];
                    $account_sec->balance = $bal_sec;
                    $account_sec->initial_balance = $ini_bl_sec;
                    if($account_sec->save())
                    {
                        $saleDetail = SaleDetail::findorfail($id);
                        $saleDetail->amount = $saleDetail->amount + $request['cash_sec'];
                        $saleDetail->due = $saleDetail->due - $request['cash_sec'];
                        $saleDetail->save();
                    }
                }
            }

            return redirect()->to(route('sales_list'))->with('success','Payment Add Successfully.');
    }



    public function sale_paymentView(Request $request)
    {
        $id = $request['id'];
        $payments = History::where('sale_details_id',$id)->get();

         $atr = '<table class="table table-bordered"><thead><th>Date</th><th>Invoice No</th><th>Account</th><th>Amount</th><th>Action</th></thead><tbody>';
        foreach($payments as $payment)
        {
            $atr.= '<tr><td>'. date("d/M/Y", strtotime($payment->created_at)).'</td><td>'.$payment->invoice_no.'</td><td>'.$payment->account->name.'</td><td>'.$payment->amount.'</td><td><a href="#" class="btn btn-icon btn-success btn-sm mr-2" data-toggle="modal" data-target="#updatePaymentModel" onclick="updatePayment('.$payment->id.')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                <a onclick="return confirm('."'Are you sure?'".')" href="'.route("sale_payment_delete",$payment->id).'" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
                </tr>';
        }
        $atr.= '</tbody></table>';
        //<a href="#" class="btn btn-icon btn-primary btn-sm mr-2" data-toggle="modal" data-target="#invoiceModelsec-'.$id.'" onclick="myfunction('.$id.')"><i class="fa fa-bell" aria-hidden="true"></i></a>
        return response($atr);

    }


    public function sale_payment_detail(Request $request)
    {
        $id = $request['id'];
        $accounts = Account::where('is_active','1')->get();
        $history = History::findorfail($id);
        $data = SaleDetail::findorfail($history->sale_details_id);
        $arr = '<form method="post" action="sale-payment-update/'.$id.'"> <input type="hidden" name="_token" value="'.csrf_token().'">
                      <div class="row">
                        <div class="col-md-6">
                            <label>Received Amount *</label>
                            <input type="text" name="paying_amount" id="paying_amount" class="form-control numkey paying_amountUpdate" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45" value="'.(round($history->tot_amount)).'">
                        </div>
                        <div class="col-md-4 mt-1">
                            <label>Change : </label>
                            <p class="changeUpdate ml-2">'.($history->tot_amount - $history->amount).'</p>
                        </div>
                        </div>

                        <div class="row">
                        <div class="col-md-6" style="margin-top:15px">
                         <label>Sale Account *</label>
                           <select name="accountUpdate" class="form-control numkey" >
                    <option value="">Select Account..</option>';
                    foreach($accounts as $account){
                    $arr.= '<option value="'.$account->id.'"';
                     if($history->account_id == $account->id){ $arr.='selected'; }
                      $arr.= '>'.$account->name .'</option>';
                    }
                   $arr.= '</select>
                        </div>
                    <div class="col-md-6" style="margin-top:15px">
                        <label>Amount *</label>
                        <input type="text" name="amountUpdate" class="form-control numkey amountUpdate" value="'.$history->amount.'" onkeyup="change_val()">
                      </div>
                      </div>
                  <button type="submit" class="btn btn-primary">Update Payment</button>
          </form>';
          return response($arr);
    }


    public function sale_payment_update(Request $request,$id)
    {
        $history = History::findorfail($id);            
        $SaleDetail = SaleDetail::findorfail($history->sale_details_id);
        $update_amount = $SaleDetail->amount - $history->amount;
        $due = $SaleDetail->total  - $update_amount;
        $SaleDetail->amount = $update_amount + $request['amountUpdate'];
        $SaleDetail->due = $due - $request['amountUpdate'];
        if($SaleDetail->save()){
            $history_sec = History::findorfail($id);
            if($history_sec->account_id == $request['accountUpdate']){
                $history_sec->amount = $request['amountUpdate'];
                if($history_sec->save()){
                   if($request['amountUpdate'] != $history->amount)
                    {
                        $next = History::where([
                            ['sale_details_id', '=', $history->sale_details_id],
                            ['id', '>', $id]
                        ])
                        ->first();
                        $find_id = History::findorfail($next->id);
                        $find_id->tot_amount = $history->tot_amount - $request['amountUpdate'];
                        $find_id->save();
                    }
                    $account = Account::findorfail($history->account_id);
                    $amt_total = $account->balance - $history->amount;
                    $ini_total = $account->initial_balance - $history->amount;
                    $account->balance = $amt_total + $request['amountUpdate'];
                    $account->initial_balance = $ini_total + $request['amountUpdate'];
                    $account->save();
                    return redirect()->to(route('sales_list'))->with('success','Payment Update Successfully.');
                }
            }else{
                    $history_sec->account_id = $request['accountUpdate'];
                    $history_sec->amount = $request['amountUpdate'];
                    if($history_sec->save()){
                        if($request['amountUpdate'] != $history->amount)
                    {
                        $next = History::where([
                            ['sale_details_id', '=', $history->sale_details_id],
                            ['id', '>', $id]
                        ])
                        ->first();
                        $find_id = History::findorfail($next->id);
                        $find_id->tot_amount = $history->tot_amount - $request['amountUpdate'];
                        $find_id->save();
                    }
                        $account = Account::findorfail($history->account_id);
                        $account->balance = $account->balance - $history->amount;
                        $account->initial_balance = $account->initial_balance - $history->amount;
                        if($account->save())
                        {
                            $account_sec = Account::findorfail($request['accountUpdate']);
                            $account_sec->balance = $account_sec->balance + $request['amountUpdate'];
                            $account_sec->initial_balance = $account_sec->initial_balance + $request['amountUpdate'];
                            $account_sec->save();
                            return redirect()->to(route('sales_list'))->with('success','Payment Edit Successfully.');
                        }
                }

            }
         
        }

    }

    public function sale_payment_delete($id)
    {
        $history = History::findorfail($id);
        if($history->delete())
        {
            $seleDetail = SaleDetail::findorfail($history->sale_details_id);
            $deduct = $seleDetail->amount - $history->amount;
            $due = $seleDetail->due + $history->amount;
            $seleDetail->amount = $deduct;
            $seleDetail->due = $due;
            if($seleDetail->save())
            {
                $account = Account::findorfail($history->account_id);
                $amt_deduct = $account->balance - $history->amount;
                $ini_deduct = $account->initial_balance - $history->amount;
                $account->balance = $amt_deduct;
                $account->initial_balance = $ini_deduct;
                $account->save();
            }
        }

        return redirect()->to(route('sales_list'))->with('success','Payment Delete Successfully.');
    }


    public function sale_delete($id)
    {
        $data = SaleDetail::findorfail($id); 
        if($data->delete()){
            $historys = History::where('sale_details_id',$data->id)->get();
            foreach($historys as $history){
                $account = Account::findorfail($history['account_id']);
                $account->balance = $account->balance - $history->amount;
                $account->initial_balance = $account->initial_balance - $history->amount;
                if($account->save()){
                    $del_his = History::findorfail($history->id);
                    $del_his->delete();
                }
            }
            $sales = Sale::where('sale_detail_id',$data->id)->get();
            foreach($sales as $sale)
            {
                $stock =StockDetail::find($sale->stock_id);
                $stock->used=0;
                if($stock->save())
                {
                    $del_sale  = Sale::findorfail($sale->id);
                    $del_sale->delete();
                }
            }
            \Session::flash('success', "Sale Delete Successfully.");
            return redirect()->to(route('sales_list'));
        }
       
    }


    public function sale_invoice(Request $request)
    {
       $body = "";
       $id = $request->id;
       $retailer = SaleDetail::findorfail($id);
       $data = StockDetail::leftjoin('sales','sales.stock_id','stock_details_new.id')
        ->select('stock_details_new.*','sales.*', 'sales.sub_total as sale_sub')
        ->where('sales.sale_detail_id',$request->id)
        ->get();

        $details = $data;
        $gold_wt = $data->sum('gw');
        $dia_wt = $data->sum('dw');
        $gold_lab = $data->sum('gold_lab');
        $dv = $data->sum('dia_pct');
        $sub_total = $data->sum('sale_sub');
        $custom = $data->sum('customer_val');
        $total = $data->sum('tot_val');
        $total_kwd = $data->sum('tot_val_kwd');
        $total_stm = $data->sum('stm_kwd');
        $tot_amt = $data->sum('tot_amt');

        $totamt = $data->sum('tot_amt');
        $total_sub = ($totamt+$retailer->discount)-$retailer->freight;
       
        $head = '<strong>Date: </strong>'.date('d/m/Y',strtotime($retailer->created_at)).'<br>Reference: '.$retailer->invoiceno.'</div><br><br><div class="row"><div class="col-md-6"><strong>To,</strong><br>'.$retailer->retailer->company_name.',<br>'.$retailer->retailer->address.',<br>Mobile: +'.$retailer->retailer->mobile.'</div></div>';
        
            $body.= '<tr><td style="width:5px"><strong>1</strong></td><td style="width:25%">'.$retailer->note.'</td><td>'.$gold_wt.'</td><td>'.$dia_wt.'</td><td>$'.$gold_lab.'</td><td>$'.$dv.'</td><td>$'.$sub_total.'</td><td>$'.$custom.'</td><td>'.$total.'</td><td>'.$total_kwd.' KD</td><td>'.$total_stm.' KD</td><td>'.$tot_amt.'</td></tr>';

        $body.=  '<tr style="height: 100px;"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
          
              <tr>
              <td><strong>Discount:</strong></td><td><strong>Freight:</strong></td><td colspan="2"><strong>Sub Total:</strong></td>
              <td colspan="2"><strong>VAT(5%)</strong></td><td colspan="2"><strong>Grand Total:</strong></td><td colspan="2"><strong >Paid Amount:</strong></td><td colspan="2"><strong>Due Amount:</strong></td>/<tr>
              <tr>
              <td>'.round($retailer->discount).'</td>
              <td>'.round($retailer->freight).'</td>
              <td colspan="2">'.round($total_sub).'</td>
              <td colspan="2">'.round($retailer->vat).'</td>
              <td colspan="2">'.round($retailer->total).'</td>
              <td colspan="2">'.round($retailer->amount).'</td>
              <td colspan="2">'.round($retailer->due).'</td> 
              </tr>';
        return response()->json(['head' => $head,'body' => $body]);

      
    }



//-------------------------- Purchase section -----------------------
    public function purchase_new()
    {
        $suppliers = Supplier::get();
        $settings = Setting::first();
        $accounts = Account::where('is_active','1')->get();
        return view('purchase',compact('suppliers','settings','accounts'));
    }

    public function purchase_post(Request $request)
    {
        $this->validate($request,[
            'supllier_id' => 'required',
            'vat' => 'required',
            'paying_amount' => 'required',
            'paid_by' => 'required',
            'account_type' => 'required',
        ]);
         
         $orderNo = "pr-".date("Ymd").'-'.date("his");
         for($i=0;$i<count($request['particular']);$i++)
         {
            $purchase = new PurchaseDetails;
            $purchase->supplier_id = $request['supllier_id'];
            $purchase->order_no = $orderNo; 
            $purchase->particular = $request['particular'][$i]; 
            $purchase->qty = $request['qty'][$i];
            $purchase->gross_wt = $request['grs_wt'][$i];
            $purchase->gld_net_wt = $request['gldnet_wt'][$i];
            $purchase->diamond_wt = $request['diamond_wt'][$i];
            $purchase->stone_wt = $request['stone_wt'][$i];
            $purchase->amount = $request['amount'][$i];
            $purchase->kwd_amount = $request['kwd_amt'][$i];
            $purchase->save();
         }

            $detail = new Purchase;
            if($request->hasFile('document')){
                $document = $request->file('document');
                $fileName = date('his').$document->getClientOriginalName();
                $document->move(public_path('document'),$fileName);
                $detail->document = $fileName;
            }   
            $detail->order_no = $orderNo;
            $detail->supplier_id = $request['supllier_id'];
            $detail->tot_qty = $request['tot_qty'];
            $detail->tot_gross_wt = $request['tot_grs_wt'];
            $detail->tot_gld_net_wt = $request['tot_gldnet_wt'];
            $detail->tot_diamond_wt = $request['tot_dmnd_wt'];
            $detail->tot_stone_wt = $request['tot_stn_wt'];
            $detail->tot_amount = $request['tot_amount'];
            $detail->total_cost = $request['tot_kwd_amt'];
            $detail->discount = $request['discount'];
            $detail->freight = $request['freight'];
            $detail->vat = $request['vat'];
            $detail->grand_total = $request['paying_amount'];
            $detail->paid_amount = $request['pay_now'];
            if($request['paying_amount'] == $request['pay_now'] ){
                $detail->status = '1';
            }
            $detail->save();

            if($request['pay_now'] != 0){
                $payment = new Payment;
                $payment->order_no = $orderNo;
                $payment->invoice_no = 'ppr-'.date("Ymd").'-'.date("his");
                $payment->total_amount = $request['paying_amount'];
                $payment->paid = $request['pay_now'];
                $payment->account_id = $request['account_type'];
                $payment->paid_by = $request['paid_by'];
                $payment->save();

                $account = Account::findorfail($request['account_type']);
                $initial_bla = $account->initial_balance - $request['pay_now'];
                $account->initial_balance = $initial_bla;
                $account->save();

            } 
            return redirect()->to(route('purchase_list'))->with('success','Purchase Add Successfully.');
    }

    public function purchase_edit($id)
    {
        $suppliers = Supplier::get();
        $settings = Setting::first();
        $accounts = Account::where('is_active','1')->get();
        $purchase = Purchase::findorfail($id);
        $details = PurchaseDetails::where('order_no',$purchase->order_no)->get();
        return view('edit_purchase',compact('details','purchase','suppliers','settings','accounts'));
    }

    public function purchase_editPost(Request $request,$id)
    {
        $this->validate($request,[
            'supllier_id' => 'required',
            'vat' => 'required',
        ]);

        $order_no = $request['order_no'];

        $detalis = PurchaseDetails::where('order_no',$order_no);

        if($detalis->delete())
        {
         for($i=0;$i<count($request['particular']);$i++)
         {
            $purchase = new PurchaseDetails;
            $purchase->supplier_id = $request['supllier_id'];
            $purchase->order_no = $order_no; 
            $purchase->particular = $request['particular'][$i]; 
            $purchase->qty = $request['qty'][$i];
            $purchase->gross_wt = $request['grs_wt'][$i];
            $purchase->gld_net_wt = $request['gldnet_wt'][$i];
            $purchase->diamond_wt = $request['diamond_wt'][$i];
            $purchase->stone_wt = $request['stone_wt'][$i];
            $purchase->amount = $request['amount'][$i];
            $purchase->kwd_amount = $request['kwd_amt'][$i];
            $purchase->save();
         }
        }

        $detail = Purchase::findorfail($id);
            if($request->hasFile('document')){
                $document = $request->file('document');
                $fileName = date('his').$document->getClientOriginalName();
                $document->move(public_path('document'),$fileName);
                $detail->document = $fileName;
            }   
            $detail->supplier_id = $request['supllier_id'];
            $detail->tot_qty = $request['tot_qty'];
            $detail->tot_gross_wt = $request['tot_grs_wt'];
            $detail->tot_gld_net_wt = $request['tot_gldnet_wt'];
            $detail->tot_diamond_wt = $request['tot_dmnd_wt'];
            $detail->tot_stone_wt = $request['tot_stn_wt'];
            $detail->tot_amount = $request['tot_amount'];
            $detail->total_cost = $request['tot_kwd_amt'];
            $detail->discount = $request['discount'];
            $detail->freight = $request['freight'];
            $detail->vat = $request['vat'];
            $detail->grand_total = $request['total'];
            $detail->save();
            return redirect()->to(route('purchase_list'))->with('success','Purchase Edit Successfully.');
    }

    public function purchase_list()
    {
        $accounts = Account::where('is_active','1')->get();
        $purchases = Purchase::orderBy('id','desc')->get();
        return view('purchase_List',compact('purchases','accounts'));
    }

    public function view_Details(Request $request)
    {
        $order_no = $request['order_no'];
        $purchases = PurchaseDetails::where('order_no',$order_no)->get();
        $atr = '<table class="table table-bordered"><thead><th>Particular</th><th>Qty</th><th>Gross Wt. (Gms)</th><th>Gold Net Wt. (Gms)</th><th>Diamond Wt. (cts)</th><th>Stone Wt. (cts)</th><th>$ Amount</th><th>KWD Amount</th></thead><tbody>';
        foreach($purchases as $purchase)
        {
            $atr.= '<tr><td>'.$purchase->particular.'</td><td>'.$purchase->qty.'</td><td>'.$purchase->gross_wt.'</td><td>'.$purchase->gld_net_wt.'</td><td>'.$purchase->diamond_wt.'</td><td>'.$purchase->stone_wt.'</td><td>'.$purchase->amount.'</td><td>'.$purchase->kwd_amount.'</td></tr>';
        }
        $atr.= '</tbody></table>';
        return response($atr);
    }

    public function purchase_delete($id)
    {
        $purchase = Purchase::findorfail($id);
        if($purchase->document != null){
             @unlink(public_path('document/'.$purchase->document));   
        } 
        $payments = Payment::where('order_no',$purchase->order_no)->get();
        $details = PurchaseDetails::where('order_no',$purchase->order_no);
        foreach($payments as $payment)
        {
            $account_id = $payment->account_id;
            $account = Account::findorfail($account_id);
            $account->initial_balance = $account->initial_balance + $payment->paid;
            $amt = $account->initial_balance + $payment->paid;
            if($account->save()){
            $payment_id = Payment::findorfail($payment->id);
            $payment_id->delete();
            }
        }
        if($purchase->delete())
        {
            $details->delete();
        }
        return redirect()->to(route('purchase_list'))->with('success','Purchase Delete Successfully.');
    }


    public function purchase_invoice(Request $request)
    {
        $body = [];
        $purchase = Purchase::where('order_no',$request->id)->first();
        $details = PurchaseDetails::where('order_no',$request->id)->get();
        $total_qty = $details->sum('qty');
        $total_gross_wt = $details->sum('gross_wt');
        $total_gld_net_wt = $details->sum('gld_net_wt');
        $total_diamond_wt = $details->sum('diamond_wt');
        $total_stone_wt = $details->sum('stone_wt');
        $total_kwd_amt = $details->sum('kwd_amount');
        $head = '<strong>Date: </strong>'.date('d/m/Y',strtotime($purchase->created_at)).'<br>Reference: '.$purchase->order_no.'<br><br>
            <div class="row"><div class="col-md-6"><strong>From,</strong><br>'.$purchase->supplier->company_name.','
            .'<br>'.$purchase->supplier->address.',<br>Mobile: +'.$purchase->supplier->mobile.'</div></div>';
            $i = 1;
        foreach($details as $detail){
            $body = '<tr><td style="width:10px;"><strong>'.$i++.'</strong></td><td style="width:30%;">'.$detail->particular.'</td><td>'.$detail->qty.'</td><td>'.$detail->gross_wt.'</td><td>'.$detail->gld_net_wt.'</td><td>'.$detail->diamond_wt.'</td><td>'.$detail->stone_wt.'</td></tr>';
        }

        $body.=  '<tr style="height: 100px;"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>

              <tr><td colspan="2"><strong>Total:</strong></td><td>'.$total_qty.'</td><td>'.$total_gross_wt.'</td><td>'.$total_gld_net_wt.'</td><td>'.$total_diamond_wt.'</td><td>'.$total_stone_wt.'</td></tr>
              <tr><td colspan="6"><strong>Total:</strong></td><td>'.$total_kwd_amt.'</td></tr>
              <tr><td colspan="6"><strong>Discount:</strong></td><td>'.$purchase->discount.'</td></tr>
              <tr><td colspan="6"><strong>Sub Total:</strong></td><td>'.($total_kwd_amt - $purchase->discount).'</td></tr>
              <tr><td colspan="6"><strong>VAT – 5%:</strong></td><td>'.$purchase->vat.'</td></tr>
              <tr><td colspan="6"><strong>Freight:</strong></td><td>'.$purchase->freight.'</td>tr>
              <tr><td colspan="6"><strong>Grand Total:</strong></td><td>'.$purchase->grand_total.'</td></tr>
              <tr><td colspan="6"><strong>Paid Amount:</strong></td><td>'.$purchase->paid_amount.'</td></tr>
              <tr><td colspan="6"><strong>Due:</strong></td><td>'.($purchase->grand_total - $purchase->paid_amount).'</td></tr>';
        return response()->json(['head' => $head,'body' => $body]);
    }


//-------------------------- Account section -----------------------------
    public function account_add()
    {
        return view('add_account');
    }

    public function account_post(Request $request)
    {
        $data = $this->validate($request,[
            'name' => 'required|string|max:255',
            'account' => 'required|max:255',
        ]);

        $account = new Account;
        $account->name = $request->name;
        $account->account_no = $request->account;
        $account->balance = $request->balance;
        $account->initial_balance = $request->balance;
        $account->note = $request->note;
        $account->save();
        return redirect()->back()->with('success', 'Account Add successfully.');
    }

    public function accounts_list()
    {
        $accounts = Account::where('is_active','1')->get();
        return view('account_list',compact('accounts'));
    }


    public function account_update(Request $request,$id)
    {
        $this->validate($request,[
            'account_no' => 'required|string',
            'name' => 'required|string',
        ]);
        $account = Account::findorfail($id);
        $deb_bal = $account->balance - $account->initial_balance;
        $ini_bal = $request['balance']- $deb_bal;
        $account->name = $request['name'];
        $account->account_no = $request['account_no'];
        $account->balance = $request['balance'];
        $account->initial_balance = $ini_bal;
        $account->note = $request['note'];
        $account->save();
        return redirect()->to(route('accounts_list'))->with('success','Account Update Successfully.');
    }

    public function account_delete($id)
    {
        $account = Account::findorfail($id);
        $account->is_active = '0';
        $account->save();
        return redirect()->to(route('accounts_list'))->with('success','Account Delete Successfully.');
    }

    public function money_transfer()
    {
        $accounts = Account::where('is_active','1')->get();
        $transfers = MoneyTransfer::get();
        return view('money_transfer',compact('accounts','transfers'));
    }

    public function money_add(Request $request)
    {
        $data = $this->validate($request,[
            'form_account' => 'required|string',
            'to_account' => 'required|string',
            'balance' => 'required|string'
        ]);

        $refer_no = "mtr-".date("Ymd").'-'.date("his");

        $transfer = new MoneyTransfer;
        $transfer->reference_no = $refer_no;
        $transfer->from_account = $data['form_account'];
        $transfer->to_account = $data['to_account'];
        $transfer->amount = $data['balance'];
        if($transfer->save())
        {
            $min_bal = Account::findorfail($data['form_account']);
            $monk = $min_bal->initial_balance - $data['balance'];
            $key = $min_bal->balance - $data['balance'];
            $min_bal->initial_balance = $monk;
            // $min_bal->balance = $key;
            if($min_bal->save())
            {
            $add_bal = Account::findorfail($data['to_account']);
            $monk_sec = $add_bal->initial_balance + $data['balance'];
            $key_sec = $add_bal->balance + $data['balance'];
            $add_bal->initial_balance = $monk_sec;
            $add_bal->balance = $key_sec;
            $add_bal->save();
            }

        }
        return redirect()->to(route('money_transfer'))->with('success','Money Transfer Successfully.');
    }

    public function account_statement(Request $request)
    {
        $payments = [];
        $debits = [];
        $credits = [];
        $historys = [];
        $data = $this->validate($request,[
        'statement' => 'required',
        'type' => 'required',
        'date' => 'required',
        'start_date' => 'required',
        'end_date' => 'required'
         ]);

       $account = Account::findorfail($data['statement']);

       if($data['type'] == 'all'){

        $payments = Payment::where('account_id', $data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=' ,$data['end_date'])->get();

        $debits = MoneyTransfer::where('from_account', $data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=',$data['end_date'])->get();

        $credits = MoneyTransfer::where('to_account', $data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=',$data['end_date'])->get();

        $historys = History::where('account_id',$data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=',$data['end_date'])->get();
       }

       if($data['type'] == 'debit')
       {

         $payments = Payment::where('account_id', $data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=' ,$data['end_date'])->get();

          $debits = MoneyTransfer::where('from_account', $data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=',$data['end_date'])->get();

       }

       if($data['type'] == 'credit')
       {
        
          $credits = MoneyTransfer::where('to_account', $data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=',$data['end_date'])->get();

          $historys = History::where('account_id',$data['statement'])->whereDate('created_at', '>=', $data['start_date'])->whereDate('created_at', '<=',$data['end_date'])->get();
       }

       return view('statement',compact('payments','debits','credits','account','historys'));
    }

    public function delete_transfer($id)
    {
        $transfer = MoneyTransfer::findorfail($id);
        if($transfer->delete())
        {
            $from_acc = Account::findorfail($transfer->from_account);
            $credit_acc  = $from_acc->initial_balance + $transfer->amount;
            $from_acc->initial_balance = $credit_acc; 
            $from_acc->save();

            $to_account = Account::findorfail($transfer->to_account);
            $debit_acc  = $to_account->initial_balance - $transfer->amount;
            $to_account->initial_balance = $debit_acc;
            $to_account->save();
        }
        return redirect()->to(route('money_transfer'))->with('success','Transfered Money Delete Successfully.');
    }


    public function balance_sheet()
    {
        $accounts = Account::where('is_active','1')->get();
        return view('balance_sheet',compact('accounts'));
    }




//----------------------------- payment section --------------------------

    public function payment_detali(Request $request)
    {
        $order_no = $request['order_no'];
        $payment = Purchase::where('order_no',$order_no)->first();
        return response()->json(['data' => $payment]);
    }

    public function payment_add(Request $request)
    {
        $id = $request['payment_id'];
        $detail = Purchase::findorfail($id);
        $paid_amount = $detail->paid_amount + $request['amount'];
        $detail->paid_amount = $paid_amount;
        if($detail->grand_total == $paid_amount ){
                $detail->status = '1';
        }else{
                $detail->status = '0';
        }
        $detail->save();

        if($request['amount'] != 0){
            $payment = new Payment;
            $payment->order_no = $detail->order_no;
            $payment->invoice_no = 'ppr-'.date("Ymd").'-'.date("his");
            $payment->total_amount = $request['paying_amount'];
            $payment->paid = $request['amount'];
            $payment->account_id = $request['account_type'];
            $payment->paid_by = $request['paid_by'];
            $payment->save();

            $account = Account::findorfail($request['account_type']);
            $initial_bla = $account->initial_balance - $request['amount'];
            $account->initial_balance = $initial_bla;
            $account->save();
        } 
        return redirect()->to(route('purchase_list'))->with('success','Purchase Add Successfully.');
    }

    public function payment_view(Request $request)
    {
         $order_no = $request['order_no'];
         $payments = Payment::where('order_no',$order_no)->get();
        $arr = '<table class="table table-bordered"><thead><th>Date</th><th>Order No</th><th>Account</th><th>Amount</th><th>Paid By</th><th>Action</th></thead><tbody>';
        foreach($payments as $payment)
        {
            $arr.= '<tr><td>'. date("d/M/Y", strtotime($payment->created_at)).'</td><td>'.$payment->invoice_no.'</td><td>'.$payment->account->name.'</td><td>'.$payment->paid.'</td><td>'.$payment->paid_by.'</td><td> <a href="#" class="btn btn-icon btn-success btn-sm mr-2" data-toggle="modal" data-target="#updatePaymentModel" onclick="updatePayment('.$payment->id.')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                <a onclick="return confirm('."'Are you sure?'".')" href="'.route("payment_delete",$payment->id).'" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>';
        }
        $arr.= '</tbody></table>';
        return response($arr);
    }

    public function payment_update(Request $request)
    {
        $accounts = Account::get();
        $id = $request['id'];
        $payment = Payment::findorfail($id);
        $arr = '<form method="post" action="payment_post/update">
        <input type="hidden" name="_token" value="'.csrf_token().'">
                      <div class="row">
                        <input type="hidden" name="payment_id" id="deduct_amount" class="deduct_amount" value="'.$id.'">
                        <div class="col-md-6">
                            <label>Received Amount *</label>
                            <input type="text" name="paying_amount" id="paying_amount" class="form-control numkey paying_amountUpdate" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45" value="'.$payment->total_amount.'">
                        </div>
                        <div class="col-md-6">
                            <label>Paying Amount *</label>
                            <input type="text" id="amount" name="amount" class="form-control numkey amountUpdate" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45" onkeyup="change_valUpdate()" value="'.$payment->paid.'">
                        </div>
                      </div>

                       <div class="row">
                        <div class="col-md-6 mt-1">
                            <label>Change : </label>
                            <p class="changeUpdate ml-2">'.($payment->total_amount - $payment->paid).'</p>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-6" style="margin-top:15px">
                        <label>Paid By*</label>
                        <select name="paid_by" id="paid_by" class="form-control numkey">
                          <option value="cash"';
                          if($payment->paid_by == "cash"){ $arr.='selected'; } 
                          $arr.= '>Cash</option>
                          <option value="bank"';
                           if($payment->paid_by == "bank"){ $arr.='selected'; } 
                          $arr.= '>Bank</option>
                        </select>
                      </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <label>Account *</label>
                      <select name="account_type" id="account_type" class="form-control numkey">';
                        foreach($accounts as $account){
                        $arr.= '<option value="'.$account->id.'"'; 
                        if($payment->account_id == $account->id){ $arr.='selected'; } 
                        $arr.='>'.$account->name.' [ '.$account->account_no.']</option>';
                        }
                      $arr.= '</select>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Update Payment</button>
          </form>';
          return response($arr);
    }

    public function payment_postUpdate(Request $request)
    {
        $id = $request['payment_id'];
        $payment = Payment::findorfail($id);
         if($request['amount'] != 0){
         if($request['account_type'] != $payment->account_id)
         {
            if($request['amount'] != $payment->paid)
            {
                $next = Payment::where([
                    ['order_no', '=', $payment->order_no],
                    ['id', '>', $id]
                ])
                ->first();
                $find_id = Payment::findorfail($next->id);
                $find_id->total_amount = $payment->total_amount - $request['amount'];
                $find_id->save();
            }

            $account = Account::findorfail($payment->account_id);
            $update_amount = $account->initial_balance + $payment->paid;
            $account->initial_balance = $update_amount;
            if($account->save()){

            $account_2 = Account::findorfail($request['account_type']);
            $initial_bla = $account_2->initial_balance - $request['amount'];
            $account_2->initial_balance = $initial_bla;
            $account_2->save();
         }
         }
         if($request['amount'] != $payment->paid)
            {
                $next = Payment::where([
                    ['order_no', '=', $payment->order_no],
                    ['id', '>', $id]
                ])
                ->first();
                $find_id = Payment::findorfail($next->id);
                $find_id->total_amount = $payment->total_amount - $request['amount'];
                $find_id->save();
            }
            $account = Account::findorfail($payment['account_id']);
            $initial_bla = $account->initial_balance + $payment->paid - $request['amount'];
            $account->initial_balance = $initial_bla;
            $account->save();


            $purchase = Purchase::where('order_no',$payment->order_no)->first();
            $paid_amount = $purchase->paid_amount - $payment->paid + $request['amount'];
            $detail = Purchase::findorfail($purchase->id);
            $detail->paid_amount = $paid_amount;
            if($detail->grand_total == $paid_amount ){
            $detail->status = '1';
            }else{
            $detail->status = '0';
            }
             if($detail->save()){
                $payment->total_amount = $request['paying_amount'];
                $payment->paid = $request['amount'];
                $payment->account_id = $request['account_type'];
                $payment->paid_by = $request['paid_by'];
                $payment->save();
             }
             }
             return redirect()->to(route('purchase_list'))->with('success','Update Payment Successfully.'); 
            }

    public function payment_delete(Request $request, $id)
    {
        $payment = Payment::findorfail($id);
        $account_id = $payment->account_id;
        $account = Account::findorfail($account_id);
        $purchase = Purchase::where('order_no',$payment->order_no)->first();
        if($payment->delete())
        {
            $account->initial_balance = $account->initial_balance + $payment->paid;
            $account->save();

            $update_purchase = Purchase::findorfail($purchase->id);
            $update_paid_amt = $update_purchase->paid_amount - $payment->paid;
            $update_purchase->paid_amount = $update_paid_amt;
            if($update_purchase->grand_total == $update_paid_amt)
            {
                $update_purchase->status = '1';
            }else{
                $update_purchase->status = '0';
            }
            $update_purchase->save();
        }
        return redirect()->to(route('purchase_list'))->with('success','Delete Payment Successfully.');
    }

   
}
