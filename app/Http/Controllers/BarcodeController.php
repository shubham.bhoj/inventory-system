<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Barcode;
use App\Models\BarcodeSize;

use Illuminate\Support\Str;
use Log;
class BarcodeController extends Controller
{
    public function index(Request $request)
    {
        $sort_search =null;
        $data['barcodes'] = Barcode::whereIn('isShow',[1])->orderBy('id', 'desc')->get();
        return view('barcode.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return view('backend.setup_configurations.barcode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        $validator = Validator::make($request->all(), [
            'barcode' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            // flash(translate('Suppplier type already exists'))->error();
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->except(['_token','status']);
        $input['status'] = $request->status=='on' ? 1 : 0;
        $barcode = Barcode::create($input);
        if($barcode->status==1){
            Barcode::where('id', '!=', $barcode->id)->update(['status'=>0]);
        }
        \Session::flash('success','Barcode has been inserted successfully');
        return redirect()->route('barcode');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data['barcode'] = Barcode::findOrFail($id);
		$data['barcode_sizes'] = BarcodeSize::where('barcode_id', $id)->get();
        return view('barcode.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {          
        $input = $request->except(['_token','status']);
        $bank = Barcode::findOrFail($request->id);
		if(!empty($request->status)){
			$input['status'] = $request->status=='on' ? 1 : 0;
		
        
        if($barcode->status=='on'){
            Barcode::where('id', '!=', $request->id)->update(['status'=>0]);
        }
		}
		$barcode = $bank->fill($input)->save();
        if(!empty($request->bsize_id)){
            for($j=0;$j<count($request->bsize_id);$j++){
                $bsize_update = BarcodeSize::findOrFail($request->bsize_id[$j]);
                $bsize_update->width=$request->bwidth[$j];
                $bsize_update->height=$request->bheight[$j];
                $bsize_update->no_of_items_in_page=$request->bno_of_items_in_page[$j];
                $bsize_update->item_gap=$request->bitem_gap[$j];
                $bsize_update->item_gap_height=$request->bitem_gap_height[$j];
                $bsize_update->margin_top=$request->bmargin_top[$j];
                $bsize_update->margin_bottom=$request->bmargin_bottom[$j];
                $bsize_update->margin_left=$request->bmargin_left[$j];
                $bsize_update->margin_right=$request->bmargin_right[$j];

                //$bsize_update->pages_gap=$request->bpages_gap[$j];
                $bsize_update->save();
            }
        }
        if(!empty($request->width)){
            for($j=0;$j<count($request->width);$j++){
                if($request->width>0 and $request->height>0){
                    $bsize_update = new BarcodeSize;
                    $bsize_update->barcode_id=$request->id;
                    $bsize_update->width=$request->width[$j];
                    $bsize_update->height=$request->height[$j];
                    $bsize_update->no_of_items_in_page=$request->no_of_items_in_page[$j];
                    $bsize_update->item_gap=$request->item_gap[$j];
                    $bsize_update->item_gap_height=$request->item_gap_height[$j];
                    $bsize_update->margin_top=$request->margin_top[$j];
                    $bsize_update->margin_bottom=$request->margin_bottom[$j];
                    $bsize_update->margin_left=$request->margin_left[$j];
                    $bsize_update->margin_right=$request->margin_right[$j];

                    //$bsize_update->pages_gap=$request->pages_gap[$j];
                    $bsize_update->save();
                }
            }
        }
        \Session::flash('success','Barcode has been updated successfully!');
        return redirect()->route('barcode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = Barcode::findOrFail($id);
        $row->delete();

        \Session::flash('success','Barcode code has been deleted successfully');
        return back();
    }


    public function status(Request $request)
    {
        $row = Barcode::findOrFail($request->id);
        $row->status = $request->status;
        $row->save();
        if($request->status==1){
            Barcode::where('id', '!=', $row->id)->update(['status'=>0]);
        }
        
        return 1;
    }

    public function remove($id){
        $row = BarcodeSize::findOrFail($id);
        $row->delete();

        \Session::flash('success','Barcode size row has been deleted successfully');
        return back();
    }
    public function barcode_printing(){
            // $lims_product_list = Product::groupBy('product_code')->get();
            // $lims_brand_list = Brand::get();
            // $lims_category_list = Category::get();
            // $lims_tax_list = Tax::get();
            $sizes_list = Barcode::join('barcode_sizes','barcode_sizes.barcode_id','=','barcodes.id')->where('barcodes.status', 1)
            ->select('barcode_sizes.width','barcode_sizes.height','barcode_sizes.item_gap','barcode_sizes.item_gap_height','barcode_sizes.no_of_items_in_page'
            ,'barcode_sizes.margin_top','barcode_sizes.margin_bottom','barcode_sizes.margin_left','barcode_sizes.margin_right')->get();
            return view('barcode-printing.index',compact('sizes_list'));
        // return view('barcode-printing.index',compact('sizes_list', 'lims_product_list', 'lims_brand_list', 'lims_category_list', 'lims_tax_list'));
    }
    public function limsProductSearch($productId)
    {
        $todayDate = date('Y-m-d');
        $product_code = explode(" ", $productId);

        $lims_product_data = Product::where('product_code', $product_code[0])->first();
        $product[] = $lims_product_data->name;
        $product[] = $lims_product_data->product_code;
        $product[] = $lims_product_data->unit_price;        
        $get_barcode = Barcode::where('status',1)->first();
        // return $get_barcode;
        $product[] = \DNS1D::getBarcodePNG($lims_product_data->product_code, $get_barcode->barcode);
        $product[] = config('currency');
        $product[] = config('currency_position');
        $product[] = $lims_product_data->qty;
        return $product;

        $todayDate = date('Y-m-d');
        $product_code = explode(" ", $productId);

        $lims_product_data = Product::where('product_code', $product_code[0])->first();
        //return $lims_product_data;
        $product[] = !empty($lims_product_data->name)?$lims_product_data->name:'-'; //0
        $product[]=!empty($lims_product_data->product_code)?$lims_product_data->product_code:'-';
        $product[]=!empty($lims_product_data->quantity)?$lims_product_data->quantity:'-';
        $product[]=!empty($lims_product_data->purchase_price)?$lims_product_data->purchase_price:'-';

        $product[] = ProductUnits::where('status', 1)->get();    

        $get_barcode = Barcode::where('status',1)->first();
        //log::info("pCode".$lims_product_data[0]->product_code);log::info("bCode".$get_barcode->barcode);
        $product[] = \DNS1D::getBarcodePNG($lims_product_data->product_code, $get_barcode->barcode);
        // $product[] = \DNS1D::getBarcodePNG($lims_product_data[0]->product_code, $get_barcode->barcode);
        $product[] = config('currency');
        $product[] = config('currency_position');
        //$product[] = $lims_product_data[0]->qty;
        
        return $product;
    }

}