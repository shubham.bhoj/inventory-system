<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => ['auth']], function () {

//--------------------------------------  Return ------------------------------------

    Route::get('sale-return',[App\Http\Controllers\ReturnController::class, 'sale_return'])->name('sale-return');
    Route::get('new-sales-return',[App\Http\Controllers\ReturnController::class, 'new_saleReturn'])->name('new-sales-return');
    Route::post('get-invioce',[App\Http\Controllers\ReturnController::class, 'invoice_return'])->name('get-invioce');
    Route::post('get-product',[App\Http\Controllers\ReturnController::class, 'get_product'])->name('get-product');
    Route::post('return-detalis',[App\Http\Controllers\ReturnController::class, 'return_detalis'])->name('return-detalis');
    Route::post('add_return',[App\Http\Controllers\ReturnController::class, 'add_return'])->name('add_return');
    Route::post('sales-return-add',[App\Http\Controllers\ReturnController::class, 'add_saleReturn'])->name('sales-return-add');
    Route::get('return-view/{invoice}',[App\Http\Controllers\ReturnController::class, 'view_return'])->name('return-view');
    Route::get('return-edit/{invoice}',[App\Http\Controllers\ReturnController::class, 'edit_return'])->name('return-edit');
    Route::post('return-invoice', [App\Http\Controllers\ReturnController::class, 'return_invoice'])->name('return-invoice');
    Route::get('return-delete/{id}',[App\Http\Controllers\ReturnController::class, 'delete_return'])->name('return-delete');
    Route::get('/print-return-invoice/{invoice}', [App\Http\Controllers\MainController::class, 'downloadReturnInvoice'])->name('print-return-invoice');
    Route::get('/sale-return-print/{invoice}', [App\Http\Controllers\MainController::class, 'downloadReturnViewInvoice'])->name('sale-return-print');

//-------------------------------- Barcode  -----------------------------------------------

    Route::get('barcode',[App\Http\Controllers\BarcodeController::class, 'index'])->name('barcode');
    Route::get('barcode/create',[App\Http\Controllers\BarcodeController::class,'create'])->name('barcode.create');
    Route::post('barcode/store',[App\Http\Controllers\BarcodeController::class, 'store'])->name('barcode.store');
    Route::get('barcode/edit/{id}',[App\Http\Controllers\BarcodeController::class, 'edit'])->name('barcode.edit');
    Route::post('barcode/edit/{id}',[App\Http\Controllers\BarcodeController::class, 'update'])->name('barcode.update');
    Route::get('barcode/delete/{id}',[App\Http\Controllers\BarcodeController::class, 'destroy'])->name('barcode.delete');
    Route::post('barcode/status',[App\Http\Controllers\BarcodeController::class, 'status'])->name('barcode.status');
    Route::post('barcodesize-remove/{id}',[App\Http\Controllers\BarcodeController::class, 'remove'])->name('remove');
    Route::get('barcode-printing',[App\Http\Controllers\BarcodeController::class, 'barcode_printing'])->name('barcode-printing');
    

//--------------------------------------------- Purchase -----------------------------------------
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/dashboard-filter/{start_date}/{end_date}', [App\Http\Controllers\HomeController::class,'dashboardFilter']);
Route::get('/purchasers', [App\Http\Controllers\HomeController::class, 'purchasers_list'])->name('purchasers_list');
Route::get('/purchase_new', [App\Http\Controllers\HomeController::class, 'purchase_new'])->name('purchase_new');
Route::post('/purchase_new', [App\Http\Controllers\HomeController::class, 'purchase_post'])->name('purchase_post');
Route::get('/purchase_list', [App\Http\Controllers\HomeController::class, 'purchase_list'])->name('purchase_list');
Route::get('/purchase_edit/{id}',[App\Http\Controllers\HomeController::class, 'purchase_edit'])->name('purchase_edit');
Route::post('/purchase_edit/{id}',[App\Http\Controllers\HomeController::class, 'purchase_editPost'])->name('purchase_editPost');
Route::post('/view_Details',[App\Http\Controllers\HomeController::class, 'view_Details'])->name('view_Details');
Route::post('/purchase_delete/{id}', [App\Http\Controllers\HomeController::class, 'purchase_delete'])->name('purchase_delete');
Route::post('/purchase-invoice', [App\Http\Controllers\HomeController::class, 'purchase_invoice'])->name('purchase-invoice');
Route::get('/print-invoice/{id}', [App\Http\Controllers\MainController::class, 'downloadPurchaseInvoice'])->name('downloadPurchaseInvoice');


//---------------------------------- payment -------------------------------

Route::post('/payment_detail',[App\Http\Controllers\HomeController::class, 'payment_detali'])->name('payment_detali');
Route::post('/payment_add',[App\Http\Controllers\HomeController::class, 'payment_add'])->name('payment_add');
Route::post('/payment_view',[App\Http\Controllers\HomeController::class, 'payment_view'])->name('payment_view');
Route::post('/payment_update',[App\Http\Controllers\HomeController::class, 'payment_update'])->name('payment_update');
Route::post('/payment_post/update',[App\Http\Controllers\HomeController::class, 'payment_postUpdate'])->name('payment_post/update');
Route::get('/payment_delete/{id}',[App\Http\Controllers\HomeController::class, 'payment_delete'])->name('payment_delete');


//--------------------------------- accounts ---------------------------------------------

Route::get('/accounts', [App\Http\Controllers\HomeController::class, 'accounts_list'])->name('accounts_list');
Route::get('/account_add', [App\Http\Controllers\HomeController::class, 'account_add'])->name('account_add');
Route::post('/account_post', [App\Http\Controllers\HomeController::class, 'account_post'])->name('account_post');
Route::post('/account_update/{id}', [App\Http\Controllers\HomeController::class, 'account_update'])->name('account_update');
Route::post('/account_delete/{id}', [App\Http\Controllers\HomeController::class, 'account_delete'])->name('account_delete');
Route::get('/balance_sheet', [App\Http\Controllers\HomeController::class, 'balance_sheet'])->name('balance_sheet');
Route::get('/money_transfer', [App\Http\Controllers\HomeController::class, 'money_transfer'])->name('money_transfer');
Route::post('/money_add', [App\Http\Controllers\HomeController::class, 'money_add'])->name('money_add');
Route::post('/delete_transfer/{id}', [App\Http\Controllers\HomeController::class, 'delete_transfer'])->name('delete_transfer');
Route::post('/account_statement', [App\Http\Controllers\HomeController::class, 'account_statement'])->name('account_statement');




Route::get('/retailers', [App\Http\Controllers\HomeController::class, 'retailers_list'])->name('retailers_list');
Route::get('/retailers_details/{id?}', [App\Http\Controllers\HomeController::class, 'retailers_details'])->name('retailers_details');
Route::get('/retailers_view/{id?}', [App\Http\Controllers\HomeController::class, 'retailers_view'])->name('retailers_view');
Route::post('/retailers_upsert', [App\Http\Controllers\HomeController::class, 'retailers_upsert'])->name('retailers_upsert');
Route::post('/retailers_delete', [App\Http\Controllers\HomeController::class, 'retailers_delete'])->name('retailers_delete');

Route::get('/suppliers', [App\Http\Controllers\HomeController::class, 'suppliers_list'])->name('suppliers_list');
Route::get('/suppliers_details/{id?}', [App\Http\Controllers\HomeController::class, 'suppliers_details'])->name('suppliers_details');
Route::get('/suppliers_view/{id}', [App\Http\Controllers\HomeController::class, 'suppliers_view'])->name('suppliers_view');
Route::post('/suppliers_upsert', [App\Http\Controllers\HomeController::class, 'suppliers_upsert'])->name('suppliers_upsert');
Route::post('/suppliers_delete', [App\Http\Controllers\HomeController::class, 'suppliers_delete'])->name('suppliers_delete');

Route::get('/stocks', [App\Http\Controllers\HomeController::class, 'stocks_list'])->name('stocks_list');
Route::get('/stocks_details/{id?}', [App\Http\Controllers\HomeController::class, 'stocks_details'])->name('stocks_details');
Route::post('/stocks_upsert', [App\Http\Controllers\HomeController::class, 'stocks_upsert'])->name('stocks_upsert');
Route::post('/stocks_delete', [App\Http\Controllers\HomeController::class, 'stocks_delete'])->name('stocks_delete');
Route::get('/stocks_history', [App\Http\Controllers\HomeController::class, 'stocks_history'])->name('stocks_history');
Route::get('/stocks_history_view/{id}', [App\Http\Controllers\HomeController::class, 'stocks_history_view'])->name('stocks_history_view');
Route::post('print-barcode', [App\Http\Controllers\HomeController::class, 'print_barcode'])->name('print-barcode');
Route::get('/stock-delete/{id}', [App\Http\Controllers\HomeController::class, 'stock_delete'])->name('stock-delete');
Route::delete('/delete-stocks', [App\Http\Controllers\HomeController::class, 'delete_stock'])->name('delete-stocks');



Route::get('/settings', [App\Http\Controllers\HomeController::class, 'settings'])->name('settings');
Route::patch('/settings_update', [App\Http\Controllers\HomeController::class, 'settings_update'])->name('settings_update');

Route::get('/company_details', [App\Http\Controllers\HomeController::class, 'company_details'])->name('company_details');
Route::patch('/company_details_update', [App\Http\Controllers\HomeController::class, 'company_details_update'])->name('company_details_update');



//--------------------------------------- sales --------------------------------------------------

Route::get('/sales', [App\Http\Controllers\HomeController::class, 'sales_list'])->name('sales_list');
Route::get('/sales_details', [App\Http\Controllers\HomeController::class, 'sales_details'])->name('sales_details');
Route::post('/sales_add', [App\Http\Controllers\HomeController::class, 'sales_add'])->name('sales_add');
Route::get('/sales_view/{id}', [App\Http\Controllers\HomeController::class, 'sales_view'])->name('sales_view');
Route::post('/add_order', [App\Http\Controllers\HomeController::class, 'add_order'])->name('add_order');
Route::get('/sales_edit/{id}', [App\Http\Controllers\HomeController::class, 'sales_edit'])->name('sales_edit');
Route::post('/sales_edit_Post/{id}', [App\Http\Controllers\HomeController::class, 'sales_editPost'])->name('sales_editPost');
Route::post('/sale-payment-view', [App\Http\Controllers\HomeController::class, 'sale_paymentView'])->name('sale-payment-view');
Route::post('/sale-payment-detail', [App\Http\Controllers\HomeController::class, 'sale_payment_detail'])->name('sale-payment-detail');
Route::post('/sale_add_payment', [App\Http\Controllers\HomeController::class, 'sale_add_payment'])->name('sale_add_payment');
Route::post('/post-sale-payment', [App\Http\Controllers\HomeController::class, 'post_sale_payment'])->name('post-sale-payment');
Route::post('/sale-payment-update/{id}', [App\Http\Controllers\HomeController::class, 'sale_payment_update'])->name('sale-payment-update');
Route::get('/sale-payment-delete/{id}', [App\Http\Controllers\HomeController::class, 'sale_payment_delete'])->name('sale_payment_delete');
Route::post('/sale-delete/{id}', [App\Http\Controllers\HomeController::class, 'sale_delete'])->name('sale-delete');
Route::post('/sales-invoice', [App\Http\Controllers\HomeController::class, 'sale_invoice'])->name('sales-invoice');

Route::get('/invoice_sales_download/{id}', [App\Http\Controllers\MainController::class, 'downloadSalesInvoice'])->name('invoice_sales_download');
Route::get('/salesDetails-download/{id}', [App\Http\Controllers\MainController::class, 'downloadSalesDetailsInvoice'])->name('salesDetails-download');


Route::get('/profile', [App\Http\Controllers\MainController::class, 'profile'])->name('profile');
Route::post('/profile_update', [App\Http\Controllers\MainController::class, 'profile_update'])->name('profile_update');
Route::post('/dues_update', [App\Http\Controllers\MainController::class, 'dues_update'])->name('dues_update');


Route::get('/creditor_list', [App\Http\Controllers\MainController::class, 'creditor_list'])->name('creditor_list');
Route::get('/history', [App\Http\Controllers\MainController::class, 'history'])->name('history');

Route::get('/banks_list', [App\Http\Controllers\MainController::class, 'banks_list'])->name('banks_list');
Route::get('/banks_details/{id?}', [App\Http\Controllers\MainController::class, 'banks_details'])->name('banks_details');
Route::get('/banks_view/{id?}', [App\Http\Controllers\MainController::class, 'banks_view'])->name('banks_view');
Route::post('/banks_upsert', [App\Http\Controllers\MainController::class, 'banks_upsert'])->name('banks_upsert');
Route::post('/banks_delete', [App\Http\Controllers\MainController::class, 'banks_delete'])->name('banks_delete');

Route::get('/expense_list', [App\Http\Controllers\MainController::class, 'expense_list'])->name('expense_list');
Route::get('/expense_details/{id?}', [App\Http\Controllers\MainController::class, 'expense_details'])->name('expense_details');
Route::get('/expense_view/{id?}', [App\Http\Controllers\MainController::class, 'expense_view'])->name('expense_view');
Route::post('/expense_upsert', [App\Http\Controllers\MainController::class, 'expense_upsert'])->name('expense_upsert');
Route::get('/expense_delete/{id?}', [App\Http\Controllers\MainController::class, 'expense_delete'])->name('expense_delete');

Route::get('/walking_customers', [App\Http\Controllers\MainController::class, 'walking_customers'])->name('walking_customers');
Route::get('/walking_customers_view/{id?}', [App\Http\Controllers\MainController::class, 'walking_customers_view'])->name('walking_customers_view');

});