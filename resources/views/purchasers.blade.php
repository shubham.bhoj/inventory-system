@extends('layouts.app')
@section('content')
<div class="card-header">
                <h4 class="card-title float-left"> Product List</h4>
                <a href="#" data-toggle="modal" data-target="#staticBackdrop" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add New
					</a>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table">
                    <thead class="text-primary">
                      <th>
                        Name
                      </th>
                      <th>
                        Country
                      </th>
                      <th>
                        City
                      </th>
                      <th>
                        Salary
                      </th>
                      <th>
                        Action
                      </th>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          Dakota Rice
                        </td>
                        <td>
                          Niger
                        </td>
                        <td>
                          Oud-Turnhout
                        </td>
                        <td>
                          $36,738
                        </td>
                        <td>
                        <a href="#" id="modal" data-toggle="modal" data-target="#editModal_1" class="btn btn-icon btn-success btn-sm mr-2"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							          <a onclick="return confirm('Are you sure?')" href="#" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Dakota Rice
                        </td>
                        <td>
                          Niger
                        </td>
                        <td>
                          Oud-Turnhout
                        </td>
                        <td>
                          $36,738
                        </td>
                        <td>
                        <a href="#" id="modal" data-toggle="modal" data-target="#editModal_1" class="btn btn-icon btn-success btn-sm mr-2"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							          <a onclick="return confirm('Are you sure?')" href="#" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                    </tbody>
                    <!-- Model edit -->
							<div class="modal fade" id="editModal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Update</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<form method="POST" action="#" enctype="multipart/form-data">
											@method('PATCH')
											@csrf
											<div class="modal-body">
												<div class="form-group">
													<label>Event Name</label>
													<input type="text" name="event_name" class="form-control @error('event_name') is-invalid @enderror" placeholder="Event Name" value="1"  autocomplete="event_name" autofocus>
													@error('event_name')
													<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
													</span>
													@enderror
													<span class="form-text text-muted"></span>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary">Update</button>
											</div>
										</form>
									</div>
								</div>
							</div>
                  </table>
                </div>
              </div>
<!--Modal Add New-->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST" action="#">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label>Category Name</label>
						<input type="text" name="title" class="form-control" placeholder="Category Name" required>
						@error('title')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
				</div>
				<div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection