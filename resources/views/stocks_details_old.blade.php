@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Stock Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('stocks_upsert') }}">
	@csrf
	<input type="hidden" value="{{isset($data->id) ? $data->id : ''}}" name="id">
    <div class="row">
        <div class="col-sm-4 col-lg-6">
			<div class="form-group">
			    <label>Supplier*</label>
				<select name="supplier_id" id="" class="form-control" required>
		        <option value="">Select Supplier</option>
				@foreach($suppliers as $supplier)
		        <option value="{{$supplier->id}}" {{isset($data->supplier_id) && $data->supplier==$supplier->id ? 'Selected' : ''}} >{{$supplier->company_name}} (+{{$supplier->mobile_code}} {{$supplier->mobile}})</option>
		        @endforeach
			    </select>
				@error('supplier_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		</div>
		<div class="table-responsive">
		<table class="table" id="mytable">
		    <thead class="text-primary text-center">
				<th>
					Code
				</th>
				<th>
					Design
				</th>
				<th>
					GW
				</th>
				<th>
					DW
				</th>
				<th>
					Gold + Lab
				</th>
				<th>
					Gold + Lab(in %)
				</th>
				<th>
					Dia P/Ct
				</th>
				<th>
					Dia Value
				</th>
				<th>
					Sub Total
				</th>
				<th>
					Customs
				</th>
				<th>
					Total Value($)
				</th>
				<th>
					Total Value(KWD)
				</th>
				<th>
					Stamping(KWD)
				</th>
				<th>
					Final Total
				</th>
				<th>
					Item
				</th>
				<th>
					Cost
				</th>
			</thead>
			<tbody>
				<tr>
				    <td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="code" id="code" class="form-control" placeholder="Code">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="design" id="design" class="form-control" placeholder="Design">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="gw" id="gw" class="form-control" placeholder="GW">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="dw" id="dw" class="form-control" placeholder="DW">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="gl" id="gl" class="form-control" placeholder="Gold + Lab">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="glp" id="glp" class="form-control" placeholder="Gold + Lab(in %)" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="diap" id="diap" class="form-control" placeholder="Dia P/Ct">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="diav" id="diav" class="form-control" placeholder="Dia Value" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="sub_total" id="sub_total" class="form-control" placeholder="Sub Total" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="customs" id="customs" class="form-control" placeholder="Customs" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="total_value_d" id="total_value_d" class="form-control" placeholder="Total Value($)" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="total_value_kwd" id="total_value_kwd" class="form-control" placeholder="Total Value(KWD)" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="stamping" id="stamping" class="form-control" placeholder="Stamping(KWD)">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" value="" name="final_total" id="final_total" class="form-control" placeholder="Final Total" disabled>
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="item" id="item" class="form-control" placeholder="Item">
		        	</td>
					<td>
		            	<input style="width: 150px;" type="text" onkeyup="myFunction()" value="" name="cost" id="cost" class="form-control" placeholder="Cost">
		        	</td>
                </tr>
			</tbody>
		</table>
        </div>
		<div style="display:block">
    <a type="button" href="#" id="insert-more" class="btn btn-success font-weight-bold">Add</a>
       </div>
	<a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>	
$("#insert-more").click(function() {
   $("#mytable").each(function() {
     var tds = '<tr>';
     jQuery.each($('tr:last td', this), function() {
       tds += '<td>' + $(this).html() + '</td>';
     });
     tds += '</tr>';
     if ($('tbody', this).length > 0) {
       $('tbody', this).append(tds);
     } else {
       $(this).append(tds);
     }
   });
 });

 function myFunction() {
	//m
	var code = $("#code").val(); 
	//m
	var design = $("#design").val(); 
	//m
	var gw = $("#gw").val(); 
	//m
	var dw = $("#dw").val(); 
	//m
	var gl = $("#gl").val(); 
	//m
	var diap = $("#diap").val(); 
	// (gw*gl)/100
	var glp_cal = ($("#gw").val()*$("#gl").val())/100;
	var glp = $("#glp").val(glp_cal); 
	//dw*diap
	var diav_cal = $("#dw").val()*$("#diap").val();
	var diav = $("#diav").val(diav_cal); 
	//gl+diav
	var sub_total_cal = $("#glp").val()+$("#diav").val();
	var sub_total = $("#sub_total").val(sub_total_cal); 
	//sub_total/100
	var customs_cal = $("#sub_total").val()/100;
	var customs = $("#customs").val(customs_cal); 
	//sub_total+customs
	var total_value_d_cal = $("#sub_total").val()+$("#customs").val();
	var total_value_d = $("#total_value_d").val(total_value_d_cal); 
	//total_value_d*304/1000
	var total_value_kwd_cal = $("#total_value_d").val()*(304/1000);
	var total_value_kwd = $("#total_value_kwd").val(total_value_kwd_cal); 
	//m
	var stamping = $("#stamping").val(); 
	//total_value_kwd+stamping
	var final_total_cal = $("#total_value_kwd").val()+$("#stamping").val();
	var final_total = $("#final_total").val(final_total_cal); 
	//m
	var item = $("#item").val(); 
	//m
	var cost = $("#cost").val(); 
 }
 </script>
@endsection