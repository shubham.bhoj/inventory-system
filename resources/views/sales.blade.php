@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Sales List</h4>
                <a href="{{ route('sales_details') }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add New
					</a>
</div>
</div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <!-- <div class="table-responsive"> -->
                  <table class="table data-table">
                    <thead class="text-primary">
                        
                    <th>No </th>
                    <th>Date</th>
                    <th>Invoice No.</th>
                    <th>Customer Name</th>
                    <th>Total Amount</th>            
                    <th>Paid</th>
                    <th>Due</th>
                    <th>Payment Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($sales_details as $key=>$st)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{date('d/M/Y', strtotime($st->created_at))}}</td>
                        <td>{{$st->invoiceno}}</td>
                        <td>{{$st->company_name}}</td>
                        <td>{{round($st->total,3)}}</td>
                        <td>{{round($st->amount,3)}}</td>
                        <td>{{round($st->due,3)}} </td>

                        <td style="text-align: center;"><?= ($st->total == $st->amount )?'<div class="btn btn-icon btn-success" style="line-height: 35px;">Paid</div>':'<div class="btn btn-icon btn-danger" style="line-height: 35px;">Due</div>' ?> </td>
                        <td>
                        <!-- <a href="{{ route('sales_view',$st->id) }}" class="btn btn-icon btn-info btn-sm mr-2"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a href="{{ route('invoice_sales_download',$st->id) }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-download" aria-hidden="true"></i></a> -->

                        <div class="dropdown">
                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action</button>
                          <ul class="dropdown-menu">
                             <li><a class="btn btn-link invoiceModel" data-toggle="modal" data-target="#invoiceModel-{{ $st->id}}" data-order_no="{{ $st->id }}"><i class="dripicons-trash"></i>Invoice</a></li>
                            <li><a class="btn btn-link viewDetails" href="{{ route('sales_view',$st->id) }}"><i class="dripicons-document-edit" ></i>View</a></li>
                            <li><a class="btn btn-link" href="{{ route('sales_edit',$st->id) }}"><i class="dripicons-document-edit"></i>Edit</a></li>
                            <li><a class="btn btn-link payment" data-order_no="{{ $st->id }}" data-toggle="modal" data-target="#paymentModal-{{ $st->id }}"><i class="dripicons-document-edit"></i>Add Payment</a></li>
                            <li><a class="btn btn-link view_Payment" data-toggle="modal" data-target="#viewPaymentModel-{{ $st->id}}" data-id="{{ $st->id}}"><i class="dripicons-document-edit"></i>View Payment</a></li>
                            <li><a class="btn btn-link" data-toggle="modal" data-target="#deletePurchaseModal-{{ $st->id}}"><i class="dripicons-trash"></i>Delete</a></li>
                          </ul>
                        </div> 
							          


                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
              <!--   </div> -->
              </div>

<!---------- View sales Payment Model-------------->
@foreach($sales_details as $key=>$st)
<div class="modal fade" id="viewPaymentModel-{{$st->id}}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader_thr" class="preloader_thr"></div>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Sales Payment Details</i>
                </div>
            </div>
        </div>
        <div class="modal-body modal_payment" id="modal_payment">

        </div>
      </div>
    </div>
  </div>
@endforeach

<!---------- Add Payment Model-------------->
@foreach($sales_details as $key=>$st)
<div class="modal fade" id="paymentModal-{{ $st->id }}" >
      <div class="modal-content" style="max-width: 53%;margin-left: 28%;">
         <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Add Sales Payment</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
            </div>
        <div class="modal-body" >
          <div class="modal-dialog">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto; " id="preloader_sec" class="preloader_sec"></div>
          </div>
          <form method="post" action="{{ route('post-sale-payment')}}">
            @csrf
          <table class="table" id="myTable_2">
            <tbody>
            <tr>
                <td style="border-top:1px;">Amount</td>
                <td style="border-top:1px;">Sale Account</td>
                <td style="border-top:1px;">Pay Amount</td>
                <td style="border-top:1px;">Sale Account</td>
                <td style="border-top:1px;">Pay Amount</td>
                <td style="border-top:1px;">Due</td>
            </tr>
            <tr class="line_items">
                <td>
                <input type="hidden" value="{{ $st->id}}" name="id" readonly>   
                <input type="text" value="0" name="amount" id="amountAdd" class="form-control amountAdd" placeholder="Amount"readonly>
                </td>
                <td>
                <select name="account_first" class="form-control" required>
                    <option value="">Select Account..</option>
                    @foreach($accounts as $account)
                    <option value="{{ $account->id}}">{{ $account->name}}</option>
                    @endforeach
                </select>
                </td>
                <td>
                <input type="text" value="0" name="cash_first" id="cash_first" class="form-control cash_first" placeholder="Pay Amount " onkeyup="due_cal()" required>
                <input type="hidden" value="0" name="due_first" id="due_first" readonly>
                </td>
                <td>
                    <select name="account_sec" class="form-control" >
                    <option value="">Select Account..</option>
                    @foreach($accounts as $account)
                    <option value="{{ $account->id}}">{{ $account->name}}</option>
                    @endforeach
                </select>
                </td>
                <td>
                <input type="text" value="0" name="cash_sec" id="cash_sec" class="form-control cash_sec" placeholder="Pay Amount " onkeyup="due_cal_sec()">
                </td>
                <td>
                <input type="text" value="0" name="due" id="due" class="form-control due" placeholder="Due" readonly>
                </td>
            </tr>
            <tr>
                <td colspan="6"><button type="submit" class="btn btn-primary">Add Payment</button></td>
            </tr>
        </tbody>
    </table>
</form>
        </div>
        </div>
      </div>
      
    </div>
  </div>
@endforeach


<!---------- Update sales Payment Model-------------->

<div class="modal fade" id="updatePaymentModel" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader" class="preloader_for"></div>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Update Sales Payment</i>
                </div>
            </div>
        </div>
        <div class="modal-body modal_paymentUpdate" id="modal_paymentUpdate">
          
        
        </div>
      </div>
      
    </div>
  </div>


  <!---------- Delete Sale Model-------------->
@foreach($sales_details as $key=>$st)
<div class="modal fade" id="deletePurchaseModal-{{ $st->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
        <div class="modal-header">
          <h4 class="modal-title">Are you sure to delete {{ $st->invoiceno }}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="viewModal-{{ $st->id }}" method="post" action="{{ route('sale-delete',$st->id)}}">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

<!---------- View Invoice Model-------------->
@foreach($sales_details as $key=>$st)
<div style="padding-right: 222px !important;" class="modal fade" id="invoiceModel-{{ $st->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel" >
    <div class="modal-dialog" style="margin-top: -51px; margin-right: 560px">
        
      <div class="modal-content" style="width: 900px; max-width: 900px;" id="modal_content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
          <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none;margin-left: 300px;
height: 97%;" id="preloader_fif" class="preloader_fif"></div>
         <div class="container mt-3 pb-2 border-bottom">
            <!-- <input type="button" value="Print" onClick="printDiv('modal_content')" id="print-btn"> -->
            <a class="btn btn-primary" href="{{route('invoice_sales_download',$st->id)}}" target="_blank">Print</a>
            <div class="row">
                <div class="col-md-12 text-center head-sl">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>

                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Sales Invoice</i>
                </div>
            </div>
            </div>

            <div id="purchase-content" class="modal-body purchase-content"></div>
        
        <div class="modal-body modal_invoice" id="modal_invoice">
          <table class="table table-bordered product-purchase-list">
                <thead>
                    <tr>
                    <th style="border:0.5px solid #fff;">Sr No.</th>
                    <th style="border:0.5px solid #fff;">Particular</th>
                    <th style="border:0.5px solid #fff;">GW</th>
                    <th style="border:0.5px solid #fff;">DW</th>
                    <th style="border:0.5px solid #fff;">Gold&nbsp;+&nbsp;Lab</th>
                    <th style="border:0.5px solid #fff;">Dia&nbsp;Value</th>
                    <th style="border:0.5px solid #fff;">Sub&nbsp;Total</th>
                    <th style="border:0.5px solid #fff;">Custom</th>
                    <th style="border:0.5px solid #fff;">$&nbsp;Total</th>
                    <th style="border:0.5px solid #fff;">Amount</th>
                    <th style="border:0.5px solid #fff;">Stamping</th>
                    <th style="border:0.5px solid #fff;">Total Amount</th>
                </tr>
              </thead>
                
            <tbody class="showDetail">
             

              
            
        </tbody>
          </table>

        </div>
      </div>
      
    </div>
  </div>
@endforeach


<!---------- View payment Invoice Model-------------->
@foreach($sales_details as $key=>$st)
<div class="modal fade" id="invoiceModelsec-{{ $st->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel" >
    <div class="modal-dialog" style="margin-top: -51px; margin-right: 560px">
        
      <div class="modal-content" style="width: 900px; max-width: 900px;" id="modal_content_sec">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
          <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none;margin-left: 300px;
height: 97%;" id="preloader_fif" class="preloader_fif"></div>
         <div class="container mt-3 pb-2 border-bottom">
           <!--  <input type="button" value="Print" onClick="printDiv('modal_content_sec')" id="print-btn" class="print-btn"> -->
           <a class="btn btn-primary" href="{{route('invoice_sales_download',$st->id)}}" target="_blank">Print</a>
            <div class="row">
                <div class="col-md-12 text-center head-sl">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>

                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Sales Invoice</i>
                </div>
            </div>
            </div>

            <div id="purchase-content-sec" class="modal-body purchase-content-sec"></div>
        
        <div class="modal-body modal_invoice" id="modal_invoice">
          <table class="table table-bordered product-purchase-list">
                <thead>
                    <tr>
                    <th>No.</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Gold <br>Wt</th>
                    <th>DIA<br>Wt</th>
                    <th>MC<br>$58-GM</th>
                    <th>DIA <br>RATE <br>$350/CT</th>
                    <th>Total <br>($)</th>
                    <th>Custom<br>(7%)</th>
                    <th>Total<br>($)</th>
                    <th>Total<br>KWD</th>
                </tr>
              </thead>
                
            <tbody class="showDetail_sec">
             

              
            
        </tbody>
          </table>

        </div>
      </div>
      
    </div>
  </div>
@endforeach

@endsection
@section('script')
<script>

$('.invoiceModel').click(function(e){

  let order_no = $(this).attr('data-order_no');
  let total_amount = 0;
  $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          url:"sales-invoice",
          type:"POST",
          data:{id:order_no,},
          cache:'false',
          beforeSend: function() {
              $(".preloader_fif").show();
           },
          success:function(e)
          {
            $(".purchase-content").html(e.head)
            $(".showDetail").html(e.body)
            $(".preloader_fif").hide();
          },
          error:function(e)
          {  
            $(".preloader_fif").hide();
            console.log(e);
          }
        });
});



 function printDiv(divName) {
        $('#print-btn').hide()
        $('.print-btn').hide()
        $('.btn-primary').hide()
        $('.close').hide()
        $('.head-sl').hide()
        var printContents = document.getElementById(divName).innerHTML;
        w=window.open();
        w.document.write('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" /><style type="text/css"> #top-head{padding-left: 300px;}@media  print {.modal-dialog { max-width: 1000px;} }</style><body onload="window.print()" style="height:100px;"><div class="card-body">'+printContents + '</div></body>');
        w.print();
        w.close();
        $('#print-btn').show()
        $('.print-btn').show()
        $('.close').show()
        $('.head-hd').show()
        $('.btn-primary').show()
    
    }

$('.payment').click(function(e){
  let order_no = $(this).attr('data-order_no');
  let total_amount = 0;
  $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          url:"sale_add_payment",
          type:"POST",
          data:{id:order_no,},
          cache:'false',
          beforeSend: function() {
              $(".preloader_sec").show();
           },
          success:function(e)
          {

            $('.amountAdd').val(parseFloat(e.due).toFixed(2));
            $('.due').val(parseFloat(e.due).toFixed(2));
            $(".preloader_sec").hide();
          },
          error:function(e)
          {  
            $('.showMODAL').show();
            $(".preloader_sec").hide();
            console.log(e);
          }
        });
});

function due_cal() {
    var cash = parseFloat($(".cash_first").val()).toFixed(2);
    var amount = parseFloat($(".amountAdd").val()).toFixed(2);
    var due = parseFloat(parseFloat(amount)-(parseFloat(cash))).toFixed(2);
    $(".due").val(due);
    $("#due_first").val(due);
}

function due_cal_sec() {
    var cash = parseFloat($(".cash_first").val()).toFixed(2);
    var cash_sec = parseFloat($(".cash_sec").val()).toFixed(2);
    var amount = parseFloat($(".amountAdd").val()).toFixed(2);
    var due = parseFloat(parseFloat(amount)-(parseFloat(cash)+parseFloat(cash_sec))).toFixed(2);
    $(".due").val(due);
}

</script>
<script>
$('.view_Payment').click(function(e){
  let id = $(this).attr('data-id');
  let token = '<?php echo csrf_token() ?>';
  $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
          url:"sale-payment-view",
          type:"POST",
          data:{id:id,_token:token},
          cache:'false',
           beforeSend: function() {
              $(".preloader_thr").show();
           },
          success:function(e)
          {
            $(".preloader_thr").hide();
            $('.modal_payment').html(e);
          },
          error:function(e)
          {
            $(".preloader_thr").hide();
            console.log(e);
          }
        });
});

function updatePayment(aru)
  {
  let token = '<?php echo csrf_token() ?>';
  $.ajax({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
          url:"sale-payment-detail",
          type:"POST",
          data:{id:aru},
          cache:'false',
           beforeSend: function() {
              $(".preloader_for").show();
           },
          success:function(e)
          {
            $(".preloader_for").hide();
            $('.modal_paymentUpdate').html(e);
          },
          error:function(e)
          {
            $(".preloader_for").hide();
            console.log(e);
          }
        });
}

function change_val()
  {
    let grand_tot = $('.paying_amountUpdate').val();
    let pay_amt = $('.amountUpdate').val();
    let change = grand_tot - pay_amt;
    $('.changeUpdate').text(change);
  }

  function myfunction(arr)
  {

  let total_amount = 0;
  $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          url:"sales-invoice",
          type:"POST",
          data:{id:arr,},
          cache:'false',
          beforeSend: function() {
              $(".preloader_fif").show();
           },
          success:function(e)
          {
            $(".purchase-content-sec").html(e.head)
            $(".showDetail_sec").html(e.body)
            $(".preloader_fif").hide();
          },
          error:function(e)
          {  
            $(".preloader_fif").hide();
            console.log(e);
          }
        });
  }
</script>
@endsection
