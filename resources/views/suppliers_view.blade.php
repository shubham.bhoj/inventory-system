@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Supplier Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Supplier Code: </label><label class="col-md-10 col-form-label">{{$data->supplier_code}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Company Name: </label><label class="col-md-10 col-form-label">{{$data->company_name}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Contact Person: </label><label class="col-md-10 col-form-label">{{$data->contact_person}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Address: </label><label class="col-md-10 col-form-label">{{$data->address}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Mobile No.: </label><label class="col-md-10 col-form-label">+{{$data->mobile_code}} {{$data->mobile}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Email ID: </label><label class="col-md-10 col-form-label">{{$data->email}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Total Purchase: </label><label class="col-md-10 col-form-label">{{$total}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Due: </label><label class="col-md-10 col-form-label">{{$due}}</label></div>
  <!-- <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a> -->
</div>

<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Suppplier History</h4>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table data-table">
        <thead class="text-primary">
            <th>Sr. No</th>
            <th>Date</th>
            <th>Order No.</th>
            <th>Total Amount</th>            
            <th>Paid</th>
            <th>Due</th>
            <th>Payment Status</th>
            <th>Action</th>
        </thead>
        <tbody>
            @php $sub_total=0;$total=0;$amount=0;$cash=0;$knet=0;$due=0; @endphp
            @foreach($secondData as $key=>$dt)
            <tr>
            <td>{{$key+1}}</td>
            <td>{{date("d/M/Y", strtotime($dt->created_at))}}</td>
            <td>{{$dt->order_no}}</td>
            <td>{{round($dt->grand_total,3)}}</td>
            <td>{{round($dt->paid_amount,3)}}</td>
            <td><?= round($dt->grand_total,3) - round($dt->paid_amount,3) ?></td>
            <td style="text-align: center;"><?= ($dt->grand_total == $dt->paid_amount )?'<div class="btn btn-icon btn-success" style="line-height: 35px;">Paid</div>':'<div class="btn btn-icon btn-danger" style="line-height: 35px;">Due</div>' ?> </td>
            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#paymentModal-{{$dt->id}}">Credit</button>
            <!-- <a href="{{ route('invoice_sales_download',$dt->id) }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-download" aria-hidden="true"></i></a> -->
            </td>
            </tr>
            @php $sub_total+=$dt->sub_total;$total+=$dt->grand_total;$amount+=$dt->paid_amount;$due+=$dt->grand_total - $dt->paid_amount; @endphp
            @endforeach
            <tr style="background: #f96332;color:white;">
                <td colspan="3" style="text-align: center;">{{'TOTAL'}}</td>
                <td>{{$total}}</td>
                <td>{{$amount}}</td>
                <td>{{$due}}</td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        </table>
    </div>
</div>
  <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
</div>

<!---------- Add Payment Model-------------->
 @foreach($secondData as $purchase)
<div class="modal fade" id="paymentModal-{{ $purchase->id }}">
      <div class="modal-content" style="width:800px;">
         <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Add Payment</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
            </div>
        <div class="modal-body">
          <div class="modal-dialog" style="margin:0px !important">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader_sec" class="preloader_sec"></div>
          </div>
               <form method="post" action="{{ route('payment_add')}}">
                      @csrf
                      <div class="row">
                        <input type="hidden" name="payment_id" id="deduct_amount" class="deduct_amount" value="{{  $purchase->id }}">
                        <div class="col-md-6">
                            <label>Received Amount *</label>
                            <input type="text" name="paying_amount" id="paying_amount" class="form-control numkey paying_amount" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45" value="{{ $purchase->grand_total - $purchase->paid_amount }}">
                        </div>
                        <div class="col-md-6">
                            <label>Paying Amount *</label>
                            <input type="text" id="amount" name="amount" class="form-control numkey amount" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45" onkeyup="change_val()" value="{{ $purchase->grand_total - $purchase->paid_amount }}">
                        </div>
                      </div>

                       <div class="row">
                        <div class="col-md-6 mt-1">
                            <label>Change : </label>
                            <p class="change ml-2">{{ $purchase->grand_total - $purchase->paid_amount }}</p>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-6" style="margin-top:15px">
                        <label> Paid By</label>
                        <select name="paid_by" id="paid_by" class="form-control numkey">
                          <option value="cash">Cash</option>
                          <option value="bank">Bank</option>
                        </select>
                      </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <label>Account *</label>
                      <select name="account_type" id="account_type" class="form-control numkey">
                        @foreach($accounts as $account)
                        <option value="{{ $account->id }}">{{ $account->name}} [{{ $account->account_no}}]</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Add Payment</button>
          </form>
        </div>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
@endsection
@section('script')
<script type="text/javascript">
function change_val()
  {
    let grand_tot = $('.paying_amount').val();
    let pay_amt = $('.amount').val();
    let change = grand_tot - pay_amt;
    $('.change').text(change);
  }
  </script>
@endsection