@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Sales Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('sales_add') }}">
	@csrf
    <div class="row">
	<div class="col-sm-12">
	<div class="form-group">
        <label for="value"></label>
        <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="retailer_or_walking" id="retailer_or_walking" checked="checked" onclick="hideorshow(1)" value="retailer">
  <label class="form-check-label" style="padding-left: 5px;" for="inlineRadio1">Retailer</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" style="margin-left: 20px;" type="radio" name="retailer_or_walking" id="retailer_or_walking" onclick="hideorshow(2)" value="walking_customer">
  <label class="form-check-label" style="padding-left: 5px;" for="inlineRadio2">Walking Customer</label>
  </div>
</div>
</div>
</div>
  <div class="row" id="retailerrow" style="display:flex;">
        <div class="col-sm-4">
			<div class="form-group">
			<label>Retailer*</label>
				<select name="retailer_id" id="retailer_id" class="form-control" required="">
		        <option value="">Select Retailer</option>
				@foreach($retailers as $retailer)
		        <option value="{{$retailer->id}}">{{$retailer->company_name}} (+{{$retailer->mobile_code}} {{$retailer->mobile}})</option>
		        @endforeach
			    </select>
				@error('retailer_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
    </div>
	<div class="row" id="customerrow" style="display:none;">
	<div class="col-sm-4">
					<div class="form-group">
						<label>Customer Name*</label>
						<input type="text" value="{{old('customer_name')}}" name="customer_name" class="form-control" placeholder="Customer Name" >
						@error('customer_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
			    <label class="mobile">Mobile No.*</label>
				<select name="mobile_code" id="" class="form-control mobile-code">
		        <option value="965" Selected>KW (+965)</option>
				@foreach($countries as $ct)
		        <option value="{{$ct->phonecode}}">{{$ct->iso}} (+{{$ct->phonecode}})</option>
		        @endforeach
			    </select>
				<input type="number" onkeypress="return isNumberKey(event)" value="{{old('mobile_no')}}" name="mobile_no" class="form-control mobile-form-control" placeholder="Mobile No.">
				@error('mobile')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
						<label>Civil/CR. No.*</label>
						<input type="number" onkeypress="return isNumberKey(event)" value="{{old('civil_no')}}" name="civil_no" class="form-control" placeholder="Civil/CR. No." >
						@error('civil_no')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
						<label>Address*</label>
						<input type="text" value="{{old('mobile_no')}}" name="address" class="form-control" placeholder="Address" >
						@error('civil_no')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
</div>
<div class="row">
<div class="col-sm-4">
			<div class="form-group">
			<label>Stock*</label>
				<select name="stock_detail_id" id="stocks_list" class="form-control" onchange="addStock(this.value)" >
		        <option value="">Select Stock</option>
				@foreach($stocks as $stock)
		        <option value="{{$stock->id}}">{{$stock->description}} {{$stock->design}}</option>
		        @endforeach
			    </select>
				@error('stock_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>

        <div class="col-sm-8">
        	<div class="form-group">
     		</div>
        </div>
</div>
<div class="tab-pane fade show active"  style="display:block" id="tabletab" role="tabpanel" aria-labelledby="table-tab">

		<div class="table-responsive">
			<table class="table">
        		<thead class="text-primary">
        			<th style="font-size: 12px;">Gold MC</th>
        			<th style="font-size: 12px;">Dia P/Ct</th>
        			<th style="font-size: 12px;">Custom</th>
        			<th style="font-size: 12px;">Kwd Convert</th>
        			<th style="font-size: 12px;">Stamping KWD</th>
        		</thead>
        		<tbody>
        			<tr>
        			<td><input type="text" class="form-control mas_gold" name="mas_gold" value="0" onkeyup="master_glod()" style="border: 2px solid #f96332;width: 80px;" required></td>	
        			<td><input type="text" class="form-control mas_dia" value="0" name="mas_dia" onkeyup="master_dia()" style="border: 2px solid #f96332;width: 80px;" required></td>
        			<td><input type="text" class="form-control mas_cus" name="mas_cus" value="0" onkeyup="master_custom()" style="border: 2px solid #f96332;width: 80px;" required></td>
        			<td><input type="text" class="form-control mas_kwd" name="mas_kwd" value="0" onkeyup="master_kwd()" style="border: 2px solid #f96332;width: 80px;" required></td>
        			<td><input type="text" class="form-control mas_stmp_kwd" name="mas_stmp_kwd" value="0" onkeyup="master_stmp_kwd()" style="border: 2px solid #f96332;width: 80px;" required></td>
        			</tr>
        		</tbody>
        	</table>
		<table class="table data-table no-footer" id="saledetails">
		<thead class="text-primary">
			<tr>
				<th style="font-size: 12px;">Code</th>
				<th style="font-size: 12px;">Design</th>
				<th style="font-size: 12px;">Image</th>
				<th style="font-size: 12px;">GW</th>
				<th style="font-size: 12px;">DW</th>
				<th style="font-size: 12px;">Gold+Lab</th>
				<th style="font-size: 12px;">Dia&nbsp;P/Ct</th>
				<th style="font-size: 12px;">Sub&nbsp;Total</th>
				<th style="font-size: 12px;">Cust&nbsp;Value</th>
				<th style="font-size: 12px;">Total&nbsp;Value<br>($)</th>
				<th style="font-size: 12px;">Total&nbsp;Value<br>KWD</th>
				<th style="font-size: 12px;">Total&nbsp;Amount</th>
				<th style="font-size: 12px;">Item</th>
				<th style="font-size: 12px;">Clarity</th>
				<th style="font-size: 12px;">Grade</th>
				<th style="font-size: 12px;">St&nbsp;Pcs</th>
				<th style="font-size: 12px;">St&nbsp;Wt</th>
				<th style="font-size: 12px;">Action</th>
			</tr>
			</thead>
			<tbody>
				<tr>
			<td colspan="3" style="text-align: right; font-weight:bold; border: 1px solid;">Total:</td>
			<td style="border: 1px solid;">0</td>
			<td style="border: 1px solid;">0</td>
			<td style="border: 1px solid;" colspan="5" >0</td>
		<td style="border: 1px solid;" colspan="3" >0</td>
		<td style="border: 1px solid;" colspan="4" >0</td>
		<td style="border: 1px solid;"></td>
		</tr>
		</tbody>
	</table>
      </div>
		<div style=""></div>
		<table class="table" >
			<tbody>
				<tr>
				<td style="border-top:1px;">Description</td>
				<td style="border-top:1px;">Discount</td>
				<td style="border-top:1px;">Freight</td>
				<td style="border-top:1px;">Sub Total</td>
				<td style="border-top:1px;">VAT - ({{$settings->vat}}%)</td>
				<td style="border-top:1px;">Total Value</td>
			</tr>
			<tr>
				<td width="40%">
				<textarea class="form-control" name="note" id="note" placeholder="Special Note"></textarea>
                </td>
				<td >
				<input type="text" value="0" name="discount" id="discount" class="form-control" placeholder="Discount" onkeyup="total_cal()">
                </td>
				
				<td>
				<input type="text" value="0" name="freight" id="freight" class="form-control" placeholder="Freight" onkeyup="total_cal()">
                </td>

				<td>
				<input type="text" value="0" name="tot_sub_total" id="tot_sub_total" class="form-control" placeholder="Sub Total" readonly>
                </td>
				<td>
				<input type="text" value="0" name="vat" id="vat" class="form-control" placeholder="VAT" readonly>
                </td>
				<td>
				<input type="text" value="0" name="total" id="total" class="form-control" placeholder="Total" readonly>
                </td>
			</tr>
		</tbody>
	</table>
	
		<div>
		<table class="table" id="myTable_2">
			<tbody>
			<tr>
				<td style="border-top:1px;">Amount</td>
				<td style="border-top:1px;">Sale Account</td>
				<td style="border-top:1px;">Pay Amount</td>
				<td style="border-top:1px;">Sale Account</td>
				<td style="border-top:1px;">Pay Amount</td>
				<td style="border-top:1px;">Due</td>
			</tr>
			<tr class="line_items">
				<td>
				<input type="text" value="0" name="amount" id="amount" class="form-control" placeholder="Amount"readonly>
                </td>
                <td>
				<select name="account_first" class="form-control" >
					<option value="">Select Account..</option>
					@foreach($accounts as $account)
					<option value="{{ $account->id}}">{{ $account->name}}</option>
					@endforeach
				</select>
                </td>
				<td>
				<input type="text" value="0" name="cash_first" id="cash_first" class="form-control" placeholder="Pay Amount " onkeyup="due_cal()">
				<input type="hidden" value="0" name="due_first" id="due_first" readonly>
                </td>
				<td>
					<select name="account_sec" class="form-control" >
					<option value="">Select Account..</option>
					@foreach($accounts as $account)
					<option value="{{ $account->id}}">{{ $account->name}}</option>
					@endforeach
				</select>
                </td>
                <td>
				<input type="text" value="0" name="cash_sec" id="cash_sec" class="form-control" placeholder="Pay Amount " onkeyup="due_cal_sec()">
                </td>
				<td>
				<input type="text" value="0" name="due" id="due" class="form-control due" placeholder="Due" readonly>
                </td>
            </tr>
		</tbody>
	</table>
        </div>
    </div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
<script>
	idarray=[];
	var vat_cal = 0;
	function addStock(id) {
		idarray.push(id);
    $.ajax({
		headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      type: "POST",
      url: "{{URL::to('/add_order')}}",
      data: {id: id,
	         idarray : idarray 
	},
      success: function (response) {
		$('#saledetails tr:last').remove();
		$('#saledetails').append('<tr id="remove_tr_'+response.data.id+'">'
		+'<td>'+response.data.code+'</td>'
		+'<td>'
		+response.data.design+ '<input type="hidden" value="'+response.data.id+'" name="stock_id[]">'
		+'</td>'
		+'<td>'+'<img src="'+'..\\'+response.data.image+'"'+'width="120" height="70" style="max-width: none;" alt="No Image">'+'</td>'
		
		+'<td><input id="tot_gold_'+response.data.id+'" data_id="'+response.data.id+'" type="text" name="tot_gold" class="form-control tot_gold" value="'+response.data.gw+'" style="color:#000;width: 60px;" readonly ></td>'
		+'<td><input id="tot_dia" type="text" data_id="'+response.data.id+'" name="tot_gold" class="form-control tot_dia" value="'+response.data.dw+'" readonly style="color:#000;width: 60px;" ></td>'
		+'<td style="display:none;"><input id="gold_per" type="text" name="gold_per[]" class="form-control gold_per" value="0" onkeyup="glod_change('+response.data.gw+','+response.data.id+')" required></td>'
		+'<td><input type="text" name="gold_lab[]" id="gold_lab_'+response.data.id+'" class="form-control gold_lab" value="0" readonly style="color:#000;" ></td>'
		+'<td style="display:none;"><input type="text" name="dia_per[]" id="dia_per" class="form-control dia_per" value="0" onkeyup="dia_change('+response.data.dw+','+response.data.id+')" required data_id="'+response.data.id+'"></td>'
		+'<td><input type="text" name="dia_pct[]" id="dia_pct_'+response.data.id+'" class="form-control dia_pct" value="0" readonly style="color:#000;width: 60px;"></td>'
		+'<td><input type="text" name="sub_total[]" id="sub_total_'+response.data.id+'" class="form-control sub_total" value="'+response.data.gw+'" readonly style="color:#000;width: 70px;"></td>'
		+'<td style="display:none;"><input type="text" name="custom[]" id="custom" data_id="'+response.data.id+'" class="form-control custom" value="0" onkeyup="custom_change('+response.data.id+')" required></td>'
		+'<td><input type="text" name="cus_value[]" id="cus_value_'+response.data.id+'" class="form-control cus_value" value="0" readonly style="color:#000;width: 60px;" ></td>'
		+'<td><input type="text" name="tot_val[]" id="tot_val_'+response.data.id+'" class="form-control tot_val" value="0" readonly style="color:#000;width: 70px;"></td>'
		+'<td style="display:none;"><input type="text" name="kwd_convertion[]" id="kwd_convertion" data_id="'+response.data.id+'" class="form-control kwd_convertion" value="0" onkeyup="kwd_change('+response.data.id+')" required></td>'
		+'<td><input type="text" name="total_kwd_val[]" id="total_kwd_val_'+response.data.id+'" class="form-control total_kwd_val" value="0" readonly style="color:#000;width: 70px;"></td>'
		+'<td style="display:none;"><input type="text" name="stamping[]" id="stamping" data_id="'+response.data.id+'" class="form-control stamping" value="0" onkeyup="add_stamping('+response.data.id+')" required></td>'
		+'<td><input type="text" name="total_amount[]" id="total_amount_'+response.data.id+'" class="form-control total_amount" value="0" readonly style="color:#000;"></td>'
		+'<td>'+response.data.description+'</td>'
		+'<td>'+response.data.dq+'</td>'
		+'<td>'+response.data.cg+'</td>'
		+'<td>'+response.data.csp+'</td>'
		+'<td>'+response.data.csw+'</td>'
		+'<td><a href="#" class="btn btn-danger font-weight-bold" onclick=remove_tr('+response.data.id+')>Remove</button></td>'
		+'</tr>'
		);
		$('#saledetails').append('<tr>'
		+'<td colspan="3" style="text-align: right; font-weight:bold; border: 1px solid;">Total:</td>'
		+'<td style="border: 1px solid;" id="total_gValue">'+parseFloat(response.tot_gw).toFixed(2)+'<input type="hidden" name="total_gwt" id="total_gwt" value="'+parseFloat(response.tot_gw).toFixed(2)+'" readonly style="color:#000;"></td>'
		+'<td style="border: 1px solid;" id="total_dValue">'+parseFloat(response.tot_dw).toFixed(2)+'<input type="hidden" name="total_dwt" id="total_dwt" value="'+Math.round(response.tot_dw)+'" readonly style="color:#000;"></td>'
		+'<td style="border: 1px solid;" colspan="3" id="total_subs_val" class="text-right">0</td>'
		+'<td style="border: 1px solid;" colspan="2" id="total_value_val" class="text-right">0</td>'
		+'<td style="border: 1px solid;" colspan="2" id="total_amount_val" class="text-right">0</td>'
		+'<td style="border: 1px solid;" colspan="6"></td>'
		+'</tr>'
		);

		vat_cal = response.settings_data.vat;
		total_cal();
      },
      error: function(error){
        console.log(error)
      }
    });
}
function total_cal() {
	var total_value_kwd_total = parseFloat($("#total_amount_val").text()).toFixed(2);
	var discount = parseFloat($("#discount").val()).toFixed(2);
	var freight = parseFloat($("#freight").val()).toFixed(2);
	var sub_total = parseFloat(parseFloat(total_value_kwd_total)-parseFloat(discount)+parseFloat(freight)).toFixed(2);
	var vat_total = parseFloat((sub_total*vat_cal)/100).toFixed(2);
	var total = Math.round(parseFloat(parseFloat(sub_total)+parseFloat(vat_total)).toFixed(2));
	$("#vat").val(vat_total);
	$("#tot_sub_total").val(sub_total); 
	$("#total").val(total);
	$("#amount").val(total); 
	$("#due").val(total)
	$("#cash").val(0)
}
function due_cal() {
	var cash = parseFloat($("#cash_first").val()).toFixed(2);
    var amount = parseFloat($("#amount").val()).toFixed(2);
    var due = parseFloat(parseFloat(amount)-(parseFloat(cash))).toFixed(2);
    $("#due").val(due);
    $("#due_first").val(due);
}

function due_cal_sec() {
	var cash = parseFloat($("#cash_first").val()).toFixed(2);
	var cash_sec = parseFloat($("#cash_sec").val()).toFixed(2);
    var amount = parseFloat($("#amount").val()).toFixed(2);
    var due = parseFloat(parseFloat(amount)-(parseFloat(cash)+parseFloat(cash_sec))).toFixed(2);
    $("#due").val(due);
}

</script>

<script>
function glod_change(arr,arr_sec){

	    let allElems = document.querySelectorAll(".gold_per");
		let total = 0;
		allElems.forEach(i => {
			total = arr * Number(i.value)
		})

		$('#gold_lab_'+arr_sec).val(parseFloat(total).toFixed(2));	
		$('#sub_total_'+arr_sec).val(parseFloat(total).toFixed(2));
		$('#tot_val_'+arr_sec).val(parseFloat(total).toFixed(2));
		total_subTotal()
		kwd_change(arr_sec)
}

function dia_change(arr,arr_sec)
{
	 let allElems = document.querySelectorAll(".dia_per");
		let total = 0;
		allElems.forEach(i => {
			total = arr * Number(i.value)
		})
       $('#dia_pct_'+arr_sec).val(total);
		let gw = $('#gold_lab_'+arr_sec).val();
		let sub_tot = parseFloat(parseFloat(total)+parseFloat(gw)).toFixed(2);
		$('#sub_total_'+arr_sec).val(parseFloat(sub_tot).toFixed(2))
		$('#tot_val_'+arr_sec).val(parseFloat(sub_tot).toFixed(2));
		total_subTotal()
		kwd_change(arr_sec)
}

function custom_change(arr)
{
	   let allElems = document.querySelectorAll(".custom");
	   let sub_total = $('#sub_total_'+arr).val();
	   let total = 0
		allElems.forEach(i => {
			total = sub_total * Number(i.value)/100
		})	
		$('#cus_value_'+arr).val(total);
		let tot = parseFloat(parseFloat(sub_total)+parseFloat(total)).toFixed(2);
		$('#tot_val_'+arr).val(tot);
		total_value()
		kwd_change(arr)
}


function kwd_change(arr)
{
	 let allElems = document.querySelectorAll(".kwd_convertion");
	   let tot_total = $('#tot_val_'+arr).val();
	   let total = 0
		allElems.forEach(i => {
			total = tot_total * Number(i.value)
		})	
		$('#total_kwd_val_'+arr).val(parseFloat(total).toFixed(2)); 
		$('#total_amount_'+arr).val(parseFloat(total).toFixed(2));
		total_kwd_con();
		add_stamping(arr)
		total_cal();
}

function add_stamping(arr)
{
	 let allElems = document.querySelectorAll(".stamping");
	   let tot_total = $('#total_kwd_val_'+arr).val();
	   let total = 0
		allElems.forEach(i => {
			total = parseFloat(parseFloat(tot_total) + parseFloat(Number(i.value))).toFixed(2)
			console.log(total)

		})	
		$('#total_amount_'+arr).val(total);
		total_kwd_con();
		total_cal();
}


function total_subTotal()
{
	let allElems = document.querySelectorAll(".sub_total");
	let total = 0
		allElems.forEach(i => {
			total = total + Number(i.value)
		})	
		$('#total_subs_val').text(parseFloat(total).toFixed(2));

	let allElems_sec = document.querySelectorAll(".tot_val");
	let total_sec = 0
		allElems_sec.forEach(i => {
			total_sec = total_sec + Number(i.value)
		})	
		$('#total_value_val').text(parseFloat(total_sec).toFixed(2));
		total_value()
}

function total_value(){
	let allElems = document.querySelectorAll(".tot_val");
	let total = 0
		allElems.forEach(i => {
			total = total + Number(i.value)
		})	
		$('#total_value_val').text(parseFloat(total).toFixed(2));
		total_kwd_con()

}

function total_kwd_con()
{
	let allElems = document.querySelectorAll(".total_amount");
	let total = 0
		allElems.forEach(i => {
			total = total + Number(i.value)
		})	
	$('#total_amount_val').text(parseFloat(total).toFixed(2));
}

function tot_gwAnddw()
{
	let allElems = document.querySelectorAll(".tot_gold");
	let total = 0
		allElems.forEach(i => {
			total = total + Number(i.value)
			console.log(total)
		})	
		$('#total_gValue').text(parseFloat(total).toFixed(2));
		$('#total_gwt').val(parseFloat(total).toFixed(2));

    let allElems_sec = document.querySelectorAll("[id='tot_dia']");
	let total_sec = 0
		allElems_sec.forEach(i => {
			total_sec = total_sec + Number(i.value)
			console.log(total_sec)
		})	
		$('#total_dValue').text(parseFloat(total_sec).toFixed(2));
		$('#total_dwt').val(parseFloat(total_sec).toFixed(2));
		
}

function remove_tr(arr)
{
	$('#remove_tr_'+arr).remove();
	tot_gwAnddw()
	total_subTotal()
	total_value()
	total_kwd_con()
	total_cal()
}

function master_glod()
{
	let gold = $('.mas_gold').val();
	let allElems = document.querySelectorAll(".tot_gold")
	$('.gold_per').val(gold);
	let lab = "";
		allElems.forEach(i => {
			lab = $(i).attr('data_id')
			$('#gold_lab_'+lab).val(parseFloat(i.value * parseFloat(gold)).toFixed(2))
			$('#sub_total_'+lab).val(parseFloat(i.value * parseFloat(gold)).toFixed(2));
		})	
		 master_dia()
		 total_subTotal()
}

function master_dia()
{
	let dia = $('.mas_dia').val();
	let allElems = document.querySelectorAll(".tot_dia")
	$('.dia_per').val(dia);
	let gw = "";
	let sub_tot = "";
	let lab = "";
		allElems.forEach(i => {
			lab = $(i).attr('data_id')
			$('#dia_pct_'+lab).val(parseFloat(i.value * parseFloat(dia)).toFixed(2))
			gw = $('#gold_lab_'+lab).val();
			sub_tot = parseFloat(parseFloat(i.value * parseFloat(dia)+parseFloat(gw))).toFixed(2);
			$('#sub_total_'+lab).val(parseFloat(sub_tot).toFixed(2))
		})
	master_custom()
	total_subTotal()

} 

function master_custom()
{
     
	let custom = $('.mas_cus').val();
	$('.custom').val(custom);
	let allElems = document.querySelectorAll(".custom");
	let lab = "";
	let cus = "";
	let sub_tot = "";
	allElems.forEach(i => {
		lab = $(i).attr('data_id')
		sub_tot = $('#sub_total_'+lab).val();	
		cus = parseFloat(parseFloat(i.value) * parseFloat(sub_tot)/100).toFixed(2)
		$('#cus_value_'+lab).val(cus)
		$('#tot_val_'+lab).val(parseFloat(parseFloat(sub_tot) + parseFloat(cus)).toFixed(2))
	})	

	let allElemsSEC = document.querySelectorAll(".tot_val");
	let tot_val = 0;
	allElemsSEC.forEach(i => {
		tot_val = tot_val + Number(i.value)
	})
	$("#total_value_val").text(parseFloat(tot_val).toFixed(2))
	master_kwd()
} 

function master_kwd()
{
	let kwd = $('.mas_kwd').val();
	$('.kwd_convertion').val(kwd);
	let allElems = document.querySelectorAll(".kwd_convertion");
	let lab = "";
	let cus = "";
	let tot = "";
	allElems.forEach(i => {
		lab = $(i).attr('data_id')
		tot = $('#tot_val_'+lab).val();	
		cus = parseFloat(parseFloat(i.value) * parseFloat(tot)).toFixed(2)
		$('#total_kwd_val_'+lab).val(cus)
		$('#total_amount_'+lab).val(cus)
	})	
	total_kwd_con()
	master_stmp_kwd()
} 

function master_stmp_kwd()
{
	let stm_kwd = $('.mas_stmp_kwd').val();
	$('.stamping').val(stm_kwd);
	let allElems = document.querySelectorAll(".stamping");
	let lab = "";
	let cus = "";
	let tot = "";
	allElems.forEach(i => {
		lab = $(i).attr('data_id')
		tot = $('#total_kwd_val_'+lab).val();	
		cus = parseFloat(parseFloat(i.value) + parseFloat(tot)).toFixed(2)
		$('#total_amount_'+lab).val(cus)
	})	
	total_kwd_con()	
	total_cal()	
}

</script>
@endsection