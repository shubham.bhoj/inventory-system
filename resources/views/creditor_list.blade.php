@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left">Creditor List</h4>
</div></div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table data-table">
                    <thead class="text-primary">
                    <th>Sr. No</th>
                    <th>Date</th>
                    <th>Invoice No.</th>
                    <th>Customer Name</th>
                    <th>Total Amount</th>
                    <th>Paid</th>
                    <th>Credit</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($data as $key=>$st)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{date('d/m/Y', strtotime($st->created_at))}}</td>
                        <td>{{$st->invoiceno}}</td>
                        <td>{{$st->company_name}}</td>
                        <td>{{round($st->total,3)}}</td>
                        <td>{{round($st->cash,3)}}</td>
                        <td>{{round($st->due,3)}}</td>
                        <td><a href="{{ route('retailers_view',$st->retailerId) }}" class="btn btn-icon btn-info btn-sm mr-2"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>

@endsection
