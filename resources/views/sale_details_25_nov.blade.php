@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Sales Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('sales_add') }}">
	@csrf
    <div class="row">
	<div class="col-sm-12">
	<div class="form-group">
        <label for="value"></label>
        <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="retailer_or_walking" id="retailer_or_walking" checked="checked" onclick="hideorshow(1)" value="retailer">
  <label class="form-check-label" style="padding-left: 5px;" for="inlineRadio1">Retailer</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" style="margin-left: 20px;" type="radio" name="retailer_or_walking" id="retailer_or_walking" onclick="hideorshow(2)" value="walking_customer">
  <label class="form-check-label" style="padding-left: 5px;" for="inlineRadio2">Walking Customer</label>
  </div>
</div>
</div>
</div>
  <div class="row" id="retailerrow" style="display:flex;">
        <div class="col-sm-4">
			<div class="form-group">
			<label>Retailer*</label>
				<select name="retailer_id" id="" class="form-control" >
		        <option value="">Select Retailer</option>
				@foreach($retailers as $retailer)
		        <option value="{{$retailer->id}}">{{$retailer->company_name}} (+{{$retailer->mobile_code}} {{$retailer->mobile}})</option>
		        @endforeach
			    </select>
				@error('retailer_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
    </div>
	<div class="row" id="customerrow" style="display:none;">
	<div class="col-sm-4">
					<div class="form-group">
						<label>Customer Name*</label>
						<input type="text" value="{{old('customer_name')}}" name="customer_name" class="form-control" placeholder="Customer Name" >
						@error('customer_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
			    <label class="mobile">Mobile No.*</label>
				<select name="mobile_code" id="" class="form-control mobile-code">
		        <option value="965" Selected>KW (+965)</option>
				@foreach($countries as $ct)
		        <option value="{{$ct->phonecode}}">{{$ct->iso}} (+{{$ct->phonecode}})</option>
		        @endforeach
			    </select>
				<input type="number" onkeypress="return isNumberKey(event)" value="{{old('mobile_no')}}" name="mobile_no" class="form-control mobile-form-control" placeholder="Mobile No.">
				@error('mobile')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
						<label>Civil/CR. No.*</label>
						<input type="number" onkeypress="return isNumberKey(event)" value="{{old('civil_no')}}" name="civil_no" class="form-control" placeholder="Civil/CR. No." >
						@error('civil_no')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
						<label>Address*</label>
						<input type="text" value="{{old('mobile_no')}}" name="address" class="form-control" placeholder="Address" >
						@error('civil_no')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
</div>
<div class="row">
<div class="col-sm-4">
			<div class="form-group">
			<label>Stock*</label>
				<select name="stock_detail_id" id="stocks_list" class="form-control" onchange="addStock(this.value)" >
		        <option value="">Select Stock</option>
				@foreach($stocks as $stock)
		        <option value="{{$stock->id}}">{{$stock->description}} {{$stock->design}}</option>
		        @endforeach
			    </select>
				@error('stock_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
</div>
<div class="table-responsive">
		<table class="table" id="myTable">
		<thead class="text-primary">
				<th>Image</th>
				<th>Design No.</th>
				<th>Description</th>
				<th>Gold Wt.</th>
				<th>Dia Wt.</th>
				<th>Gold Mc</th>
				<th>Dia Rate</th>
				<th>Total($)</th>
				<th>Custom Duty</th>
				<th>Total($)</th>
				<th>Total(KWD)</th>
				<th>Diamond Quality</th>
				<th>Colour Grade</th>
				<th>Colour Stone Pcs</th>
				<th>Colour Stone Weight</th>
			</thead>
			<tbody>
			<td colspan="5" style="text-align: right; font-weight:bold; border: 1px solid;">Total:</td>
			<td style="border: 1px solid;">0</td>
		<td style="border: 1px solid;">0</td>
		<td style="border: 1px solid;">0</td>
		<td style="border: 1px solid;">0</td>
		<td style="border: 1px solid;">0</td>
		<td style="border: 1px solid;">0</td>
		<td style="border: 1px solid;" colspan="4"></td>
		</tr>
		</tbody>
	</table>
        </div>
		<div style="width: 230px;display: block;margin-left: auto;">
		<table class="table" id="myTable">
			<tbody>
			<tr>
				<td style="border-top: 1px;">Discount</td>
				<td style="border-top: 1px;">
				<input type="text" value="0" name="discount" id="discount" class="form-control" placeholder="Discount" onkeyup="total_cal()">
                </td>
			</tr>
			<tr>
				<td>Freight</td>
				<td>
				<input type="text" value="0" name="freight" id="freight" class="form-control" placeholder="Freight" onkeyup="total_cal()">
                </td>
			</tr>
			<tr>
				<td>Sub Total</td>
				<td>
				<input type="text" value="0" name="sub_total" id="sub_total" class="form-control" placeholder="Sub Total" readonly>
                </td>
			</tr>
			<tr>
				<td>VAT - ({{$settings->vat}}%)</td>
				<td>
				<input type="text" value="0" name="vat" id="vat" class="form-control" placeholder="VAT" readonly>
                </td>
			</tr>
			<tr>
				<td>Total Value</td>
				<td>
				<input type="text" value="0" name="total" id="total" class="form-control" placeholder="Total" readonly>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
		<div style="width: 50%;">
		<table class="table" id="myTable">
			<tbody>
			<tr>
				<td style="border-top:1px;">Amount</td>
				<td style="border-top:1px;">Cash</td>
				<td style="border-top:1px;">KNET</td>
				<td style="border-top:1px;">Due</td>
			</tr>
			<tr>
				<td>
				<input type="text" value="0" name="amount" id="amount" class="form-control" placeholder="Amount"readonly>
                </td>
				<td>
				<input type="text" value="0" name="cash" id="cash" class="form-control" placeholder="Cash" onkeyup="due_cal()">
                </td>
				<td>
				<input type="text" value="0" name="knet" id="knet" class="form-control" placeholder="KNET" onkeyup="due_cal()">
                </td>
				<td>
				<input type="text" value="0" name="due" id="due" class="form-control" placeholder="Due" readonly>
                </td>
			</tr>
		</tbody>
	</table>
        </div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
<script>
	idarray=[];
	var vat_cal = 0;
	function addStock(id) {
		idarray.push(id);
    $.ajax({
		headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      type: "POST",
      url: "{{URL::to('/add_order')}}",
      data: {id: id,
	         idarray : idarray 
	},
      success: function (response) {
		$('#myTable tr:last').remove();
		$('#myTable').append('<tr>'
		+'<td>'+'<img src="'+'..\\'+response.data.image+'"'+'width="120" height="70" style="max-width: none;" alt="No Image">'+'</td>'
		+'<td>'
		+response.data.design+
	    +' '+'<input type="hidden" value="'+response.data.id+'" name="stock_id[]">'+' '
		+'</td>'
		+'<td>'+response.data.description+'</td>'
		+'<td>'+response.data.gw+'</td>'
		+'<td>'+response.data.dw+'</td>'
		+'<td>'+response.data.gmc+'</td>'
		+'<td>'+response.data.dmc+'</td>'
		+'<td>'+response.data.total+'</td>'
		+'<td>'+response.data.customs+'</td>'
		+'<td>'+response.data.total_value_d+'</td>'
		+'<td>'+response.data.total_value_kwd+'</td>'
		+'<td>'+response.data.dq+'</td>'
		+'<td>'+response.data.cg+'</td>'
		+'<td>'+response.data.csp+'</td>'
		+'<td>'+response.data.csw+'</td>'
		+'</tr>'
		);
		$('#myTable').append('<tr>'
		+'<td colspan="5" style="text-align: right; font-weight:bold; border: 1px solid;">'+'Total:'+'</td>'
		+'<td style="border: 1px solid;">'+parseFloat(response.gmc_total).toFixed(2)+'</td>'
		+'<td style="border: 1px solid;">'+parseFloat(response.dmc_total).toFixed(2)+'</td>'
		+'<td style="border: 1px solid;">'+parseFloat(response.total_total).toFixed(2)+'</td>'
		+'<td style="border: 1px solid;">'+parseFloat(response.customs_total).toFixed(2)+'</td>'
		+'<td style="border: 1px solid;">'+parseFloat(response.total_value_d_total).toFixed(2)+'</td>'
		+'<td style="border: 1px solid;">'+parseFloat(response.total_value_kwd_total).toFixed(2)+
		' '+'<input type="hidden" value="'+response.total_value_kwd_total+'" name="total_value_kwd_total" id="total_value_kwd_total">'+' '+'</td>'
		+'<td style="border: 1px solid;" colspan="4"></td>'
		+'</tr>'
		);
		vat_cal = response.settings_data.vat;
		total_cal();
      },
      error: function(error){
        console.log(error)
      }
    });
}
function total_cal() {
	var total_value_kwd_total = parseFloat($("#total_value_kwd_total").val()).toFixed(2);
	var discount = parseFloat($("#discount").val()).toFixed(2);
	var freight = parseFloat($("#freight").val()).toFixed(2);
	var sub_total = parseFloat(parseFloat(total_value_kwd_total)-parseFloat(discount)+parseFloat(freight)).toFixed(2);
	var vat_total = parseFloat((sub_total*vat_cal)/100).toFixed(2);
	var total = parseFloat(parseFloat(sub_total)+parseFloat(vat_total)).toFixed(2);
	$("#vat").val(vat_total);
	$("#sub_total").val(sub_total); 
	$("#total").val(total);
	$("#amount").val(total); 
	$("#due").val(total)
	$("#cash").val(0)
	$("#knet").val(0)
}
function due_cal() {
	var cash = parseFloat($("#cash").val()).toFixed(2);
	var knet = parseFloat($("#knet").val()).toFixed(2);
	var amount = parseFloat($("#amount").val()).toFixed(2);
	var due = parseFloat(parseFloat(amount)-(parseFloat(cash)+parseFloat(knet))).toFixed(2);
	$("#due").val(due);
}
</script>
@endsection