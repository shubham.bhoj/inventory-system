@extends('layouts.app')
@section('content')
<div class="card-header">
                <h4 class="card-title float-left"> Balance Sheet</h4>
              </div>
              <div class="card-body">
              <div class="table-responsive">
                  <table class="table">
                    <thead class="text-primary">
                      <th>
                        Name
                      </th>
                      <th>
                        Account No
                      </th>
                      <th>
                        Credit
                      </th>
                      <th>
                        Debit
                      </th>
                      <th>
                        Balance
                      </th>
                    </thead>
                    <tbody>
                      @foreach($accounts as $account)
                      <tr>
                        <td>{{ $account->name }}</td>
                        <td>{{ $account->account_no }}</td>
                        <td>{{ number_format((float)$account->balance, 2, '.', '')}}</td>
                        <td>{{ number_format((float)$account->balance - $account->initial_balance, 2, '.', '')}}</td>
                        <td>{{ number_format((float)$account->initial_balance, 2, '.', '')}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>

@endsection