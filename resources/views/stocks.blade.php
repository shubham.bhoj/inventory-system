@extends('layouts.app')
@section('content')
<style>
    #barcode_view{
        height: 400px;
        overflow: auto;
    }
    table th .font-adjust{
        style="font-weight: 600;font-size: 13px;"
    }
    #barcode_view table tr td{
        padding: 0px;
    }

</style>
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Stock List</h4>
                <a href="{{ route('stocks_details') }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add New
					</a>
</div></div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table data-table">
                    <thead class="text-primary">
                    <th><input type="checkbox" id="master"></th>
                    <th style="font-weight: 600;font-size: 13px;">Code</th>
                    <th style="font-weight: 600;font-size: 13px;">Design</th>
                    <th style="font-weight: 600;font-size: 13px;">Image</th>
                    <th style="font-weight: 600;font-size: 13px;">GW</th>
                    <th style="font-weight: 600;font-size: 13px;">DW</th>
                    <th style="font-weight: 600;font-size: 13px;">Gold+Lab</th>
                    <th style="font-weight: 600;font-size: 13px;">Dia P/Ct</th>
                    <th style="font-weight: 600;font-size: 13px;">Dia Value</th>
                    <th style="font-weight: 600;font-size: 13px;">Sub Total</th>
                    <th style="font-weight: 600;font-size: 13px;">Customs</th>
                    <th style="font-weight: 600;font-size: 13px;">Total Value<br>$</th>
                    <th style="font-weight: 600;font-size: 13px;">Total Value<br>KWD</th>
                    <th style="font-weight: 600;font-size: 13px;">Stamping<br>KWD</th>
                    <th style="font-weight: 600;font-size: 13px;">Final&nbsp;Total</th>
                    <th style="font-weight: 600;font-size: 13px;">Item</th>
                    <th style="font-weight: 600;font-size: 13px;">Clarity</th>
                    <th style="font-weight: 600;font-size: 13px;">Grade</th>
                    <th style="font-weight: 600;font-size: 13px;">St&nbsp;PCS</th>
                    <th style="font-weight: 600;font-size: 13px;">St&nbsp;Wt </th>
                    <th style="font-weight: 600;font-size: 13px;">Action</th>
                    </thead>
                    <tbody>
                        <?php $i = 1 ?>
                      @foreach($data as $dt)
                      <tr>
                      <td><input type="checkbox" class="sub_chk" data-id="{{$dt->id}}"></td>
                       <td>{{$dt->code}}</td>
                       <td>{{$dt->design}}</td>
                        <td><img src="..\{{$dt->image}}"  id="myImg{{$dt->id}}" onclick="getImageId('myImg{{$dt->id}}')" width="120" height="70" style="max-width: none;" alt="No Image" class="image_set"></td>
                        <td>{{$dt->gw}}</td>
                        <td>{{$dt->dw}}</td>
                        <td>${{$dt->ngw}}</td>
                        <td>${{$dt->dp}}</td>
                        <td>${{$dt->dv}}</td>
                        <td>${{$dt->total}}</td>
                        <td>${{$dt->customs}}</td>
                        <td>${{$dt->total_value_d}}</td>
                        <td>{{$dt->total_value_kwd}}</td>
                        <td>{{$dt->stamping}}</td>
                        <td>{{$dt->final_total}}</td>
                        <td>{{$dt->description}}</td>
                        <td>{{$dt->dq}}</td>
                        <td>{{$dt->cg}}</td>
                        <td>{{$dt->csp}}</td>
                        <td>{{$dt->csw}}</td>
                        <td>
                            <a class="btn btn-danger font-weight-bold" href="{{route('stock-delete',$dt->id)}}">Remove</a>
                        </td>
                      </tr>
                      @endforeach
                      <tr>
                      <button style="margin-bottom: 10px;font-family: cambria !important;" class="btn btn-primary font-weight-bolder btn-md text-left mr-5 print_all" data-toggle="modal" data-target="#barcodeModel">Print Barcode</button>
                      <button style="margin-bottom: 10px;font-family: cambria !important;" class="btn btn-primary font-weight-bolder btn-md text-left mr-5 delete_all" >Delete All </button>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="myModal" class="modal">
<span class="close">&times;</span>
<img class="modal-content" id="img01">
</div>

  <!----------Barcode Model-------------->
<div class="modal fade" id="barcodeModel" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader" class="preloader_for"></div>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <a class="btn btn-primary" target="_blank" style="color: #fff;" onClick="printDiv('barcode_view')">Print</a>
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Barcode Priting</i>
                </div>
            </div>
        </div>
        <div class="modal-body barcode_view" id="barcode_view">
          
        
        </div>
      </div>
      
    </div>
  </div>
<script>
function getImageId(imageId){
var modal = document.getElementById("myModal");
var img = document.getElementById(imageId);
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  $(".displaynav").css("display",'none');
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  modal.style.display = "none";
  $(".displaynav").css("display",'block');
}}
</script>
@endsection
@section('script')

<script type="text/javascript">

    $(document).ready(function () {


        $('#master').on('click', function(e) {

         if($(this).is(':checked',true))  

         {

            $(".sub_chk").prop('checked', true);  

         } else {  

            $(".sub_chk").prop('checked',false);  

         }  

        });


        $('.delete_all').on('click', function(e) {


            var allVals = [];  

            $(".sub_chk:checked").each(function() {  

                allVals.push($(this).attr('data-id'));

            });  


            if(allVals.length <=0)  

            {  

                alert("Please select row.");  

            }  else {  


                var check = confirm("Are you sure you want to delete this row?");  

                if(check == true){  


                    var join_selected_values = allVals.join(","); 


                    $.ajax({

                        url: 'delete-stocks',

                        type: 'DELETE',

                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

                        data: 'ids='+join_selected_values,

                        success: function (data) {
                            console.log(data)

                              if (data['success']) {

                                $(".sub_chk:checked").each(function() {  

                                    $(this).parents("tr").remove();

                                });

                            } else if (data['error']) {

                                console.log(data['error']);

                            } else {

                                console.log(e)

                            }

                        },

                        error: function (data) {

                            console.log(e)

                        }

                    });


                  $.each(allVals, function( index, value ) {

                      $('table tr').filter("[data-row-id='" + value + "']").remove();

                  });

                }  

            }  

        });


        $('.print_all').on('click', function(e) {


            var allVals = [];  

            $(".sub_chk:checked").each(function() {  

                allVals.push($(this).attr('data-id'));

            });  


            if(allVals.length <=0)  

            {  
                alert("Please select atleast one Product.");  
                return false;

            }  else {    

                var join_selected_values = allVals.join(","); 

                    $.ajax({

                        url: 'print-barcode',

                        type: 'post',

                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

                        data: 'ids='+join_selected_values,

                        beforeSend: function() {
                          $(".preloader_for").show();
                       },

                        success: function (data) {
                            $(".preloader_for").hide();
                            $(".barcode_view").html(data)
                        },

                        error: function (data) {
                            $(".preloader_for").hide();
                            console.log(e)

                        }

                    });                 

            }  

        });


        
        });

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        w=window.open();
        w.document.write('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" /><style> #barcode_print table tr td{padding: 0px;}@media  print {.modal-dialog { max-width: 1000px;} }</style><body onload="window.print()" style="height:100px;"><div class="card-body" id="barcode_print">'+printContents + '</div></body>');
        w.print();
        w.close();
    }

</script>
@endsection

