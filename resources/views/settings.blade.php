@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Settings</h4>
</div>
</div>
            </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
            <form method="POST" action="{{ route('settings_update') }}">
            @method('PATCH')
				@csrf
        <div class="row">
						<input type="hidden" value="{{$data->id}}" name="id">
<div class="col-sm-4">
					<div class="form-group">
						<label>Custom Charge(in %)*</label>
						<input type="text" value="{{$data->custom_charge}}" name="custom_charge" class="form-control" placeholder="Custom Charge" required>
						@error('custom_charge')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Stamping Charge*</label>
						<input type="text" name="stamping_charge" class="form-control" value="{{$data->stamping_charge}}" placeholder="Stamping Charge" required>
						@error('stamping_charge')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<!-- <div class="col-sm-4">
					<div class="form-group">
						<label>Sales Rate KWD*</label>
						<input type="text" value="{{$data->sales_rate_kwd}}" name="sales_rate_kwd" class="form-control" placeholder="Sales Rate KWD" required>
						@error('sales_rate_kwd')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Purchase Rate KWD*</label>
						<input type="text" value="{{$data->purchase_rate_kwd}}" name="purchase_rate_kwd" class="form-control" placeholder="Purchase Rate KWD" required>
						@error('purchase_rate_kwd')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div> -->

<!-- <div class="col-sm-4">
					<div class="form-group">
						<label>USD Rate KWD*</label>
						<input type="text" value="{{$data->sales_rate_kwd}}" name="sales_rate_kwd" class="form-control" placeholder="Sales Rate KWD" required>
						@error('sales_rate_kwd')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div> -->
<div class="col-sm-4">
					<div class="form-group">
						<label>KWD Rate USD*</label>
						<input type="text" value="{{number_format((float)$data->purchase_rate_kwd, 3, '.', '')}}" name="purchase_rate_kwd" class="form-control" placeholder="Purchase Rate KWD" required>
						@error('purchase_rate_kwd')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>

<div class="col-sm-4">
					<div class="form-group">
						<label>VAT(in %)*</label>
						<input type="text" value="{{$data->vat}}" name="vat" class="form-control" placeholder="VAT(in %)" required>
						@error('vat')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<!-- <div class="col-sm-4">
					<div class="form-group">
						<label>Gold+Lab(in %)*</label>
						<input type="text" value="{{$data->gold_lab}}" name="gold_lab" class="form-control" placeholder="Gold+Lab" required>
						@error('gold_lab')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div> -->
<div class="col-sm-4">
					<div class="form-group">
						<label>Gold MC*</label>
						<input type="text" value="{{$data->gold_mc}}" name="gold_mc" class="form-control" placeholder="Gold+Lab" required>
						@error('gold_mc')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Diamond MC*</label>
						<input type="text" value="{{$data->diamond_mc}}" name="diamond_mc" class="form-control" placeholder="Diamond MC" required>
						@error('diamond_mc')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
      </div>
<button type="submit" class="btn btn-primary font-weight-bold">Save</button>

      </form>
      </div>
@endsection