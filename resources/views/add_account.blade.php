@extends('layouts.app')
@section('content')

<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('account_post') }}">
	@csrf
    <div class="row">
        <div class="col-sm-8">
					<div class="form-group">
						<label>Account No.*</label>
						<input type="text" name="account" class="form-control" placeholder="Account No." required onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45">
						@error('account')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>

        <div class="col-sm-8">
			<div class="form-group">
				<label>Name*</label>
				<input type="text" value="" name="name" class="form-control" placeholder="Name" required>
				@error('name')
				<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>

        <div class="col-sm-8">
			<div class="form-group">
				<label>Initial Balance</label>
				<input type="text" value="" name="balance" class="form-control" placeholder="Initial Balance" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45">
				@error('name')
				<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
       
		<div class="col-sm-8">
					<div class="form-group">
						<label>Note</label>
						<textarea name="note" class="form-control" placeholder="note"></textarea>
						@error('note')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
@endsection