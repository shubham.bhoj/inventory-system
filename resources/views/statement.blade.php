@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left">Account Statement</h4>
        </div>
        <div class="col-sm-12" style="display: flex;">
                <h5>Account:</h5> &nbsp;<p style="margin-top: 7px;">{{ $account->name}}[{{$account->account_no }}]</p>
        </div>
</div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif

              <div class="table-responsive">
                  <table class="table data-table">
                    <thead class="text-primary">
                    <th>Sr. No</th>
                    <th>Date</th>
                    <th>Reference No.</th>
                    <th>Credit</th>
                    <th>Debit</th>
                    </thead>
                    <tbody>
                    @foreach($payments as $key=>$st)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{date('d/m/Y', strtotime($st->created_at))}}</td>
                        <td>{{ $st->order_no}}</td>
                        <td> 0.00 </td>
                        <td>{{ number_format((float) $st->paid, 2, '.', '')}}</td>
                      </tr>
                      @endforeach

                      @foreach($debits as $key=>$stk)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{date('d/m/Y', strtotime($stk->created_at))}}</td>
                        <td>{{  $stk->reference_no}}</td>
                        <td> 0.00 </td>
                        <td>{{ number_format((float) $stk->amount, 2, '.', '')}}</td>
                      </tr>
                      @endforeach

                      @foreach($credits as $key=>$credit)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{date('d/m/Y', strtotime($credit->created_at))}}</td>
                        <td>{{  $credit->reference_no}}</td>
                        <td>{{ number_format((float) $credit->amount, 2, '.', '')}}</td>
                        <td> 0.00 </td>
                      </tr>
                      @endforeach
                      @foreach($historys as $key=>$history)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{date('d/m/Y', strtotime($history->created_at))}}</td>
                        <td>{{  $history->invoice_no}}</td>
                        <td>{{ number_format((float) $history->amount, 2, '.', '')}}</td>
                        <td> 0.00 </td> 
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>

@endsection
