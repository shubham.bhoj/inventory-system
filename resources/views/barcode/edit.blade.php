@extends('layouts.app')

@section('content')
<style>
 .row-border{
    border: 1px solid #ddd;
    padding: 13px 0;
}
</style>
<div class="card-header">
    <h4 class="card-title float-left">Barcode Information</h4>
</div>


<div class="card-body p-0">
   
            <table class="table" style="border: none;">
                <form class="p-4" action="{{ route('barcode.edit', $barcode->id) }}" method="POST">
         <!--            <input name="_method" type="hidden" value="PATCH"> -->
                    
                    @csrf
                    <input name="id" type="hidden" value="{{$barcode->id}}">
                    <tr>
                    <td style="border: none;"><b>Barcode</b></td>
                    <td style="border: none;"><b>{{ $barcode->barcode }}</b></td>   
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>  
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>                
                    </tr>
					 <div class="form-group row">
                        <label class="col-md-3 col-from-label"></label>
                        <div class="col-md-12 ">
                        <div class="row mt-2">
                            <div class="col-md-11">
                            <table  class="table" id="barcodeTable">
  <thead>
    <th></th>
    <th></th>
    <th>CM</th>
    <th>MM</th>
    <th>Inch</th>
    <th>Remove</th>
</thead>
  
  @if(count($barcode_sizes)>0)
    @foreach($barcode_sizes as $bsizes)
    <input type="hidden" name="bsize_id[]" value="{{$bsizes->id}}">
    <tbody>
        <tr>
          <th>No. of barcode in a page</th>
          <th><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->no_of_items_in_page}}" placeholder="No. of items in a page" name="bno_of_items_in_page[]" class="form-control qty_data" required=""> <i class="fa fa-refresh" onClick="refreshAll({{$bsizes->id}}, 'Exist')"></i> </th>
          <td style="border: none;"></td>
          <td style="border: none;"></td>
          <td style="border: none;"></td>
          <td><button type="button" class="btn btn-xs btn-danger" onClick="removeSize({{$bsizes->id}})">-</button></td>        
        </tr>
        <tr style="padding:3px 0;border-top:1px solid #ddd; border: none;"><th style="border: none;"></th><th style="border: none;"></th><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td> <td style="border: none;"></td></tr><tr style="border: none;"><th style="border: none;"></th><th style="border: none;"></th><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td> <td style="border: none;"></td></tr>

        <tr>
          <th></th>
          <th>Barcode Width</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->width * 0.1}}" placeholder="Width" onKeyup="exist_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} width_mm cm{{$bsizes->id}}"></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->width}}" placeholder="Width" name="bwidth[]" onKeyup="exist_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} width_mm mm{{$bsizes->id}}"></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->width * 0.0393701}}" placeholder="Width" onKeyup="exist_inch_to_mm({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} width_inch inch{{$bsizes->id}}"></td>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th>Barcode Height</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->height * 0.1}}" placeholder="Height" onKeyup="exist_height_cm_to_mminch({{$bsizes->id}})" class="form-control height_mm qty_data eitemrow{{$bsizes->id}} hcm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->height}}" placeholder="Height" name="bheight[]" onKeyup="exist_height_mm_to_inch({{$bsizes->id}})" class="form-control height_mm qty_data eitemrow{{$bsizes->id}} hmm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->height * 0.0393701}}" placeholder="Height" onKeyup="exist_height_inch_to_mm({{$bsizes->id}})" class="form-control height_inch qty_data eitemrow{{$bsizes->id}} hinch{{$bsizes->id}}" required=""></td>
          <td></td>
        </tr>
        <tr>
          <th>Label</th>
          <th>Label Gap Height (Top <span>&#8593;</span> &nbsp;&nbsp; Bottom <span>&#8595;</span>)</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->item_gap_height * 0.1}}" placeholder="Items height in cm" onKeyup="exist_gapitemheight_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_item_gapheight_cm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->item_gap_height}}" placeholder="Items height in mm" name="bitem_gap_height[]" onKeyup="exist_gapitemheight_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_item_gapheight_mm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->item_gap_height * 0.039370125}}" placeholder="Items height in inch" onKeyup="exist_gapitemheight_inch_to_mm({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_item_gapheight_inch{{$bsizes->id}}" required=""></td>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th>Label Gap Width (Left <span>&#x2190;</span>  &nbsp;&nbsp; Right <span>&#x2192;</span> )</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->item_gap * 0.1}}" placeholder="Items gap in cm" onKeyup="exist_gapitem_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_item_gap_cm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->item_gap}}" placeholder="Items gap in mm" name="bitem_gap[]" onKeyup="exist_gapitem_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_item_gap_mm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->item_gap * 0.039370125}}" placeholder="Items gap in inch" onKeyup="exist_gapitem_inch_to_mm({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_item_gap_inch{{$bsizes->id}}" required=""></td>
          <td></td>
        </tr>     
        <tr style="padding:3px 0;border-top:1px solid #ddd;"><th ></th><th></th><td></td><td></td><td></td><td></td></tr><tr><th style="border-top: none;"></th><th style="border-top: none;"></th><td style="border-top: none;"></td><td style="border-top: none;"></td><td style="border-top: none;"></td> <td style="border-top: none;"></td></tr>
   
        <tr>
        <th></th>
          <th>Top</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_top * 0.1}}" placeholder="Top margin in cm" onKeyup="exist_margintop_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_margintop_cm{{$bsizes->id}}"></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_top}}" placeholder="Top margin in mm" name="bmargin_top[]" onKeyup="exist_margintop_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_margintop_mm{{$bsizes->id}}"></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_top * 0.0393701}}" placeholder="Top margin in inch" onKeyup="exist_margintop_inch_to_mm({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} width_inch e_margintop_inch{{$bsizes->id}}"></td>
          <td></td>
        </tr>
        <tr>
        <th></th>
          <th>Bottom</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_bottom * 0.1}}" placeholder="Bottom margin in cm" onKeyup="exist_marginbottom_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginbottom_cm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_bottom}}" placeholder="Bottom margin in mm" name="bmargin_bottom[]" onKeyup="exist_marginbottom_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginbottom_mm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_bottom * 0.0393701}}" placeholder="Bottom margin in inch" onKeyup="exist_marginbottom_inch_to_mm({{$bsizes->id}})" class="form-control height_inch qty_data eitemrow{{$bsizes->id}} e_marginbottom_inch{{$bsizes->id}}" required=""></td>
          <td></td>
        </tr>
        <tr>
        <th>Barcode Alignment</th>
          <th>Left</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_left * 0.1}}" placeholder="Left margin in cm" onKeyup="exist_marginleft_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginleft_cm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_left}}" placeholder="Left margin in mm" name="bmargin_left[]" onKeyup="exist_marginleft_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginleft_mm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_left * 0.0393701}}" placeholder="Left margin in inch" onKeyup="exist_marginleft_inch_to_mm({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginleft_inch{{$bsizes->id}}" required=""></td>
          <td></td>
        </tr>
        <tr>
        <th></th>
          <th>Right</th>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_right * 0.1}}" placeholder="Right margin in cm" onKeyup="exist_marginright_cm_to_mminch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginright_cm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_right}}" placeholder="Right margin in mm" name="bmargin_right[]" onKeyup="exist_marginright_mm_to_inch({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginright_mm{{$bsizes->id}}" required=""></td>
          <td><input type="text" style="width:140px;display:inline;" lang="en" value="{{$bsizes->margin_right * 0.039370125}}" placeholder="Right margin in inch" onKeyup="exist_marginright_inch_to_mm({{$bsizes->id}})" class="form-control qty_data eitemrow{{$bsizes->id}} e_marginright_inch{{$bsizes->id}}" required=""></td>
          <td></td>
        </tr>
        <tr style="padding:15px 0;border-top:2px solid #333;"><th></th><th></th><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td> <td></td></tr><tr><th style="border: none;"></th><th style="border: none;"></th><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td> <td style="border: none;"></td></tr>
    </tbody>
@endforeach
    @endif
</table>



							<div class="row mt-2">
                            <div class="col-md-6"></div><div class="col-md-5"></div>
                                <div class="col-md-1 text-center">
                                        <button type="button" class="btn btn-xs btn-primary add_quantity">+</button>
                                </div>
</div>

                    <div class="form-group mb-0 mt-2 text-center">
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </form>
            </table>
</div>
</div>
</div>

@endsection
@section('script')

<script type="text/javascript">
function remove_quantity(id){
        $(".bsize_remove_"+id).remove();
    }
    var counter=0;
    $(document).on('click','.add_quantity',function(){
        
            counter++;
        var totalQuantity = $('input[name="current_stock"]').val();
        var total = 0;
      
        $('.qty_data').each(function(){
            total+=Number($(this).val());
        });
        
            var html='';
            html+='<tbody class="bsize_remove_'+counter+'"><tr><th>No. of barcode in a page</th><th><input type="text" style="width:140px;display:inline;" lang="en" value="1" placeholder="No. of barcode in a page" name="no_of_items_in_page[]" class="form-control qty_data" required=""> <i class="fa fa-refresh" onClick="refreshAll('+counter+', 1)"></i></th>\
          <td td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"><button type="button" class="btn btn-xs btn-danger remove-quantity" onClick="remove_quantity('+counter+')">-</button></td></tr>\
          <tr style="padding:3px 0;border-top:1px solid #ddd;"><th style="border: none;"></th><th style="border: none;"></th><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td> <td style="border: none;"></td></tr><tr style="border: none;"><th style="border: none;"></th><th style="border: none;"></th><td style="border: none;"></td><td style="border: none;"></td><td style="border: none;"></td> <td style="border: none;"></td></tr><tr><th></th>\<th>Barcode Width</th>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Width in cm" onKeyup="new_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' width_mm newcm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Width in mm" name="width[]" onKeyup="new_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' width_mm newmm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Width in inch" onKeyup="new_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' width_inch newinch'+counter+'" required>\
                            </td>\
                            <td></td>\
                            </tr>\
                        <tr><th></th>\<th>Barcode Height</th>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Height in cm" onKeyup="new_height_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' eheight_mm newhcm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Height in mm" name="height[]" onKeyup="new_height_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' eheight_mm newhmm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Height in inch" onKeyup="new_height_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' eheight_mm newhinch'+counter+'" required>\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr><th>Label</th>\<th>Label Gap Height (Top <span>&#8593;</span> &nbsp;&nbsp; Bottom <span>&#8595;</span>)</th>\
                            <td>\
                               <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Item height in cm" onKeyup="new_gapitemheight_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapmm new_item_gap_hcm'+counter+'" required>\
                            </td>\
                            <td>\
                               <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Item height in mm" name="item_gap_height[]" onKeyup="new_gapitemheight_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapmm new_item_gap_hmm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Item height in inch" onKeyup="new_gapitemheight_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapinch new_item_gap_hinch'+counter+'" required>\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr><th></th>\<th>Label Gap Width (Left <span>&#x2190;</span>  &nbsp;&nbsp; Right <span>&#x2192;</span> )</th>\
                            <td>\
                               <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Item width in cm" onKeyup="new_gapitem_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapmm new_item_gap_cm'+counter+'" required>\
                            </td>\
                            <td>\
                               <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Item width in mm" name="item_gap[]" onKeyup="new_gapitem_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapmm new_item_gap_mm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Item width in inch" onKeyup="new_gapitem_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapinch new_item_gap_inch'+counter+'" required>\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr style="padding:3px 0;border-top:1px solid #ddd;"><th></th><th></th><td></td><td></td><td></td> <td></td></tr><tr style="border-top: none;"><th style="border-top: none;"></th><th style="border-top: none;"></th><td style="border-top: none;"></td><td style="border-top: none;"></td><td style="border-top: none;"></td> <td style="border-top: none;"></td></tr><tr><th></th>\<th>TOP</th>\
                        <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Top margin in cm" onKeyup="new_margintop_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapinch new_margintop_cm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Top margin in mm" name="margin_top[]" onKeyup="new_margintop_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' new_margintop_mm'+counter+'">\
                            </td>\
                            <td>\
                            <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Top margin in inch" onKeyup="new_margintop_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' width_inch new_margintop_inch'+counter+'">\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr><th></th>\<th>BOTTOM</th>\
                        <td>\
                                <input type="text" style="width:140px;display:inline;" value="0" lang="en" placeholder="Bottom margin in cm" onKeyup="new_marginbottom_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapinch new_marginbottom_cm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Bottom margin in mm" name="margin_bottom[]" onKeyup="new_marginbottom_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' new_marginbottom_mm'+counter+'">\
                            </td>\
                            <td>\
                            <input type="text" style="width:140px;display:inline;"Bottom margin in inch" onKeyup="new_marginbottom_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' width_inch new_marginbottom_inch'+counter+'">\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr><th>Barcode Alignment</th>\<th>LEFT</th>\
                        <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Left margin in cm" onKeyup="new_marginleft_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapinch new_marginleft_cm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Left margin in mm" name="margin_left[]" onKeyup="new_marginleft_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' new_marginleft_mm'+counter+'">\
                            </td>\
                            <td>\
                            <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Left margin in inch" onKeyup="new_marginleft_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' width_inch new_marginleft_inch'+counter+'">\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr><th></th>\<th>RIGHT</th>\
                        <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Right margin in cm" onKeyup="new_marginright_cm_to_mminch('+counter+')" class="form-control qty_data itemrow'+counter+' item_gapinch new_marginright_cm'+counter+'" required>\
                            </td>\
                            <td>\
                                <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Right margin in mm" name="margin_right[]" onKeyup="new_marginright_mm_to_inch('+counter+')" class="form-control qty_data itemrow'+counter+' new_marginright_mm'+counter+'">\
                            </td>\
                            <td>\
                            <input type="text" style="width:140px;display:inline;" lang="en" value="0" placeholder="Right margin in inch" onKeyup="new_marginright_inch_to_mm('+counter+')" class="form-control qty_data itemrow'+counter+' width_inch new_marginright_inch'+counter+'">\
                            </td>\
                            <td></td>\
                        </tr>\
                        <tr><th></th>\<th></th><td></td><td></td><td></td><td></td></tr>\
                        <tr style="padding:15px 0;border-top:2px solid #333;"><th></th><th></th><td td style="border: none;"></td><td td style="border: none;"></td><td td style="border: none;"></td> <td></td></tr><tr td style="border: none;"><th td style="border: none;"></th><th td style="border: none;"></th><td td style="border: none;"></td><td td style="border: none;"></td><td td style="border: none;"></td> <td td style="border: none;"></td></tr></tbody>';
            $(html).insertAfter($("#barcodeTable").find("tbody:last-child"));
            $(".aiz-selectpicker").selectpicker();
        
    });

    function removeSize(id){
        if(confirm('Are you sure you want to delete this item.')){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           method: "post",
           url: "{{ url('admin/barcodesize-remove') }}/"+id,
           success: function(data) {
              
               if (data) {
                window.location.reload();
                        } else {

                           
                        }                           
           }
       });
	}
    }
    function exist_cm_to_mminch(val){
        $(".inch"+val).val($(".cm"+val).val() * 0.393701);
        $(".mm"+val).val($(".cm"+val).val() * 10);
    }
    function exist_mm_to_inch(val){
        $(".inch"+val).val($(".mm"+val).val() * 0.039370125);
        $(".cm"+val).val($(".mm"+val).val() * 0.10);
    }
    function exist_inch_to_mm(val){
        $(".mm"+val).val($(".inch"+val).val() * 25.4);
        $(".cm"+val).val($(".inch"+val).val() * 2.54);
    }

    function new_cm_to_mminch(val){
        $(".newinch"+val).val($(".newcm"+val).val() * 0.393701);
        $(".newmm"+val).val($(".newcm"+val).val() * 10);
    }
    function new_mm_to_inch(val){
        $(".newinch"+val).val($(".newmm"+val).val() * 0.0393701);
        $(".newcm"+val).val($(".newmm"+val).val() * 0.10);

    }
    function new_inch_to_mm(val){
        $(".newmm"+val).val($(".newinch"+val).val() * 25.4);
        $(".newcm"+val).val($(".newinch"+val).val() * 2.54);
    }

    function exist_height_cm_to_mminch(val){
        $(".hinch"+val).val($(".hcm"+val).val() * 0.39370125);
        $(".hmm"+val).val($(".hcm"+val).val() * 10);
    }
    function exist_height_mm_to_inch(val){
        $(".hinch"+val).val($(".hmm"+val).val() * 0.039370125);
        $(".hcm"+val).val($(".hmm"+val).val() * 0.10);
    }
    function exist_height_inch_to_mm(val){
        $(".hmm"+val).val($(".hinch"+val).val() * 25.4);
        $(".hcm"+val).val($(".hinch"+val).val() * 2.54);
    }

    function new_height_cm_to_mminch(val){
        $(".newhinch"+val).val($(".newhcm"+val).val() * 0.393701);
        $(".newhmm"+val).val($(".newhcm"+val).val() * 10);
    }
    function new_height_mm_to_inch(val){
        $(".newhinch"+val).val($(".newhmm"+val).val() * 0.0393701);
        $(".newhcm"+val).val($(".newhmm"+val).val() * 0.10);

    }
    function new_height_inch_to_mm(val){
        $(".newhmm"+val).val($(".newhinch"+val).val() * 25.4);
        $(".newhcm"+val).val($(".newhinch"+val).val() * 2.54);
    }



    function exist_gapitem_cm_to_mminch(val){
        $(".e_item_gap_inch"+val).val($(".e_item_gap_cm"+val).val() * 0.39370125);
        $(".e_item_gap_mm"+val).val($(".e_item_gap_cm"+val).val() * 10);
    }
    function exist_gapitem_mm_to_inch(val){
        $(".e_item_gap_inch"+val).val($(".e_item_gap_mm"+val).val() * 0.039370125);
        $(".e_item_gap_cm"+val).val($(".e_item_gap_mm"+val).val() * 0.10);
    }
    function exist_gapitem_inch_to_mm(val){
        $(".e_item_gap_mm"+val).val($(".e_item_gap_inch"+val).val() * 25.4);
        $(".e_item_gap_cm"+val).val($(".e_item_gap_inch"+val).val() * 2.54);
    }

    function new_gapitem_cm_to_mminch(val){
        $(".new_item_gap_inch"+val).val($(".new_item_gap_cm"+val).val() * 0.393701);
        $(".new_item_gap_mm"+val).val($(".new_item_gap_cm"+val).val() * 10);
    }
    function new_gapitem_mm_to_inch(val){
        $(".new_item_gap_inch"+val).val($(".new_item_gap_mm"+val).val() * 0.0393701);
        $(".new_item_gap_cm"+val).val($(".new_item_gap_mm"+val).val() * 0.10);

    }
    function new_gapitem_inch_to_mm(val){
        $(".new_item_gap_mm"+val).val($(".new_item_gap_inch"+val).val() * 25.4);
        $(".new_item_gap_cm"+val).val($(".new_item_gap_inch"+val).val() * 2.54);
    }


    function exist_gapitemheight_cm_to_mminch(val){
        $(".e_item_gapheight_inch"+val).val($(".e_item_gapheight_cm"+val).val() * 0.39370125);
        $(".e_item_gapheight_mm"+val).val($(".e_item_gapheight_cm"+val).val() * 10);
    }
    function exist_gapitemheight_mm_to_inch(val){
        $(".e_item_gapheight_inch"+val).val($(".e_item_gapheight_mm"+val).val() * 0.039370125);
        $(".e_item_gapheight_cm"+val).val($(".e_item_gapheight_mm"+val).val() * 0.10);
    }
    function exist_gapitemheight_inch_to_mm(val){
        $(".e_item_gapheight_mm"+val).val($(".e_item_gapheight_inch"+val).val() * 25.4);
        $(".e_item_gapheight_cm"+val).val($(".e_item_gapheight_inch"+val).val() * 2.54);
    }

    function new_gapitemheight_cm_to_mminch(val){
        $(".new_item_gap_hinch"+val).val($(".new_item_gap_hcm"+val).val() * 0.393701);
        $(".new_item_gap_hmm"+val).val($(".new_item_gap_hcm"+val).val() * 10);
    }
    function new_gapitemheight_mm_to_inch(val){
        $(".new_item_gap_hinch"+val).val($(".new_item_gap_hmm"+val).val() * 0.0393701);
        $(".new_item_gap_hcm"+val).val($(".new_item_gap_hmm"+val).val() * 0.10);

    }
    function new_gapitemheight_inch_to_mm(val){
        $(".new_item_gap_hmm"+val).val($(".new_item_gap_hinch"+val).val() * 25.4);
        $(".new_item_gap_hcm"+val).val($(".new_item_gap_hinch"+val).val() * 2.54);
    }


    function exist_margintop_cm_to_mminch(val){
        $(".e_margintop_inch"+val).val($(".e_margintop_cm"+val).val() * 0.39370125);
        $(".e_margintop_mm"+val).val($(".e_margintop_cm"+val).val() * 10);
    }
    function exist_margintop_mm_to_inch(val){
        $(".e_margintop_inch"+val).val($(".e_margintop_mm"+val).val() * 0.039370125);
        $(".e_margintop_cm"+val).val($(".e_margintop_mm"+val).val() * 0.10);
    }
    function exist_margintop_inch_to_mm(val){
        $(".e_margintop_mm"+val).val($(".e_margintop_inch"+val).val() * 25.4);
        $(".e_margintop_cm"+val).val($(".e_margintop_inch"+val).val() * 2.54);
    }

    function new_margintop_cm_to_mminch(val){
        $(".new_margintop_inch"+val).val($(".new_margintop_cm"+val).val() * 0.393701);
        $(".new_margintop_mm"+val).val($(".new_margintop_cm"+val).val() * 10);
    }
    function new_margintop_mm_to_inch(val){
        $(".new_margintop_inch"+val).val($(".new_margintop_mm"+val).val() * 0.0393701);
        $(".new_margintop_cm"+val).val($(".new_margintop_mm"+val).val() * 0.10);

    }
    function new_margintop_inch_to_mm(val){
        $(".new_margintop_mm"+val).val($(".new_margintop_inch"+val).val() * 25.4);
        $(".new_margintop_cm"+val).val($(".new_margintop_inch"+val).val() * 2.54);
    }

    function exist_marginbottom_cm_to_mminch(val){
        $(".e_marginbottom_inch"+val).val($(".e_marginbottom_cm"+val).val() * 0.39370125);
        $(".e_marginbottom_mm"+val).val($(".e_marginbottom_cm"+val).val() * 10);
    }
    function exist_marginbottom_mm_to_inch(val){
        $(".e_marginbottom_inch"+val).val($(".e_marginbottom_mm"+val).val() * 0.039370125);
        $(".e_marginbottom_cm"+val).val($(".e_marginbottom_mm"+val).val() * 0.10);
    }
    function exist_marginbottom_inch_to_mm(val){
        $(".e_marginbottom_mm"+val).val($(".e_marginbottom_inch"+val).val() * 25.4);
        $(".e_marginbottom_cm"+val).val($(".e_marginbottom_inch"+val).val() * 2.54);

    }
    
    function new_marginbottom_cm_to_mminch(val){
        $(".new_marginbottom_inch"+val).val($(".new_marginbottom_cm"+val).val() * 0.393701);
        $(".new_marginbottom_mm"+val).val($(".new_marginbottom_cm"+val).val() * 10);
    }
    function new_marginbottom_mm_to_inch(val){
        $(".new_marginbottom_inch"+val).val($(".new_marginbottom_mm"+val).val() * 0.0393701);
        $(".new_marginbottom_cm"+val).val($(".new_marginbottom_mm"+val).val() * 0.10);

    }
    function new_marginbottom_inch_to_mm(val){
        $(".new_marginbottom_mm"+val).val($(".new_marginbottom_inch"+val).val() * 25.4);
        $(".new_marginbottom_cm"+val).val($(".new_marginbottom_inch"+val).val() * 2.54);
    }


    function exist_marginleft_cm_to_mminch(val){
        $(".e_marginleft_inch"+val).val($(".e_marginleft_cm"+val).val() * 0.39370125);
        $(".e_marginleft_mm"+val).val($(".e_marginleft_cm"+val).val() * 10);
    }
    function exist_marginleft_mm_to_inch(val){
        $(".e_marginleft_inch"+val).val($(".e_marginleft_mm"+val).val() * 0.039370125);
        $(".e_marginleft_cm"+val).val($(".e_marginleft_mm"+val).val() * 0.10);
    }
    function exist_marginleft_inch_to_mm(val){
        $(".e_marginleft_mm"+val).val($(".e_marginleft_inch"+val).val() * 25.4);
        $(".e_marginleft_cm"+val).val($(".e_marginleft_inch"+val).val() * 2.54);

    }

    function new_marginleft_cm_to_mminch(val){
        $(".new_marginleft_inch"+val).val($(".new_marginleft_cm"+val).val() * 0.393701);
        $(".new_marginleft_mm"+val).val($(".new_marginleft_cm"+val).val() * 10);
    }
    function new_marginleft_mm_to_inch(val){
        $(".new_marginleft_inch"+val).val($(".new_marginleft_mm"+val).val() * 0.0393701);
        $(".new_marginleft_cm"+val).val($(".new_marginleft_mm"+val).val() * 0.10);

    }
    function new_marginleft_inch_to_mm(val){
        $(".new_marginleft_mm"+val).val($(".new_marginleft_inch"+val).val() * 25.4);
        $(".new_marginleft_cm"+val).val($(".new_marginleft_inch"+val).val() * 2.54);
    }

    function exist_marginright_cm_to_mminch(val){
        $(".e_marginright_inch"+val).val($(".e_marginright_cm"+val).val() * 0.39370125);
        $(".e_marginright_mm"+val).val($(".e_marginright_cm"+val).val() * 10);
    }
    function exist_marginright_mm_to_inch(val){
        $(".e_marginright_inch"+val).val($(".e_marginright_mm"+val).val() * 0.039370125);
        $(".e_marginright_cm"+val).val($(".e_marginright_mm"+val).val() * 0.10);
    }
    function exist_marginright_inch_to_mm(val){
        $(".e_marginright_mm"+val).val($(".e_marginright_inch"+val).val() * 25.4);
        $(".e_marginright_cm"+val).val($(".e_marginright_inch"+val).val() * 2.54);

    }

    function new_marginright_cm_to_mminch(val){
        $(".new_marginright_inch"+val).val($(".new_marginright_cm"+val).val() * 0.393701);
        $(".new_marginright_mm"+val).val($(".new_marginright_cm"+val).val() * 10);
    }
    function new_marginright_mm_to_inch(val){
        $(".new_marginright_inch"+val).val($(".new_marginright_mm"+val).val() * 0.0393701);
        $(".new_marginright_cm"+val).val($(".new_marginright_mm"+val).val() * 0.10);

    }
    function new_marginright_inch_to_mm(val){
        $(".new_marginright_mm"+val).val($(".new_marginright_inch"+val).val() * 25.4);
        $(".new_marginright_cm"+val).val($(".new_marginright_inch"+val).val() * 2.54);
    }

    function refreshAll(row, type){
        if(type=='Exist'){
            $(".eitemrow"+row).val(0);
        }
        if(type==1){
            $(".itemrow"+row).val(0);
        }
    }
	/* $(document).on('keyup','.width_mm',function(){

        $(".width_inch").val($(".width_mm").val() * 25.4);

    });
    $(document).on('keyup','.width_inch',function(){

        $(".width_mm").val($(".width_inch").val() * 0.0393701);

        }); */
</script>
@endsection
