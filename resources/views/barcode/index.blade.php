@extends('layouts.app')
@section('content')
<div class="card-header">
                <h4 class="card-title float-left">Barcode List</h4>
                </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table">
                    <thead class="text-primary">
                      <th style="text-align:center;">
                      Sr No.
                      </th>
                      <th style="text-align:center;">
                        Barcode
                      </th>
                      <th style="text-align:center;">
                      Formate
                      </th>
                      <th style="text-align:center;">
                       Status 
                      </th>
                      <th style="text-align:center;">
                        Action
                      </th>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                     @foreach($barcodes as $barcode)
                      <tr>
                        <td align="center">
                         {{ $i++ }}
                        </td>
                          <td align="center">{{ $barcode->barcode }}</td>
                    <td align="center"><img src="{{asset('public/barcodes/'.$barcode->barcode_image)}}"> <i class="fa fa-info" title="{{$barcode->description}}"></i></td>
                    <td align="center">
                        <label class="aiz-switch aiz-switch-success mb-0">
                            <input onchange="update_barcode_status(this)" value="{{ $barcode->id }}" name="checkk" type="radio" <?php if ($barcode->status == 1) echo "checked"; ?>>
                            <span class="slider round"></span>
                        </label>
                        
                    </td>
                    <td align="center">
                        <a href="{{route('barcode.edit', $barcode->id )}}" class="btn btn-icon btn-success btn-sm mr-2" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                
                        <!-- <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('barcode.delete', $barcode->id)}}" title="Delete">
                            <i class="las la-trash"></i>
                        </a> -->
                    </td>
                      </tr>
                      @endforeach
                    </tbody>

                  </table>
                </div>
              </div>
      
    </div>
  </div>

@endsection

@section('script')
    <script type="text/javascript">
        function sort_pickup_points(el){
            $('#sort_pickup_points').submit();
        }
        
        function update_barcode_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route("barcode.status") }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    alert('Barcode status updated successfully');
                }
                else{
                    alert('Something went wrong');
                }
            });
        }
    </script>
@endsection
