@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Stock History</h4>
                <!-- <a href="{{ route('stocks_details') }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add New
					</a> -->
</div></div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
              <table border=1 class="table data-table">
                    <thead class="text-primary">
                    <th>ID</th>
                    <th>Image</th>
                    <th>Design No.</th>
                    <th>Description</th>
                    <th>Pcs</th>
                    <th>Gold Quality</th>
                    <th>Gross Weight</th>
                    <th>Net Gold Weight</th>
                    <th>Diamond Pcs</th>
                    <th>Diamond Weight</th>
                    <th>Diamond Quality</th>
                    <th>Colour Grade</th>
                    <th>Colour Stone Pcs</th>
                    <th>Colour Stone Weight</th>
                    <th>Gold MC</th>
                    <th>Diamond MC</th>
                    <th>Total</th>
                    <th>Custom</th>
                    <th>Total Amount($)</th>
                    <th>Total Amount(KWD)</th>
                    <th>Stamping(KWD)</th>
                    <th>Final Amount(KWD)</th>
                    </thead>
                    <tbody>
                      @foreach($data as $dt)
                      <tr>
                      <td>{{$dt->id}}</td>
                        <td><img src="..\{{$dt->image}}" id="myImg{{$dt->id}}" onclick="getImageId('myImg{{$dt->id}}')" width="120" height="70" style="max-width: none;" alt="No Image"></td>
                        <td>{{$dt->design}}</td>
                        <td>{{$dt->description}}</td>
                        <td>{{$dt->pcs}}</td>
                        <td>{{$dt->gq}}</td>
                        <td>{{$dt->gw}}</td>
                        <td>{{$dt->ngw}}</td>
                        <td>{{$dt->dp}}</td>
                        <td>{{$dt->dw}}</td>
                        <td>{{$dt->dq}}</td>
                        <td>{{$dt->cg}}</td>
                        <td>{{$dt->csp}}</td>
                        <td>{{$dt->csw}}</td>
                        <td>{{$dt->gmc}}</td>
                        <td>{{$dt->dmc}}</td>
                        <td>{{$dt->total}}</td>
                        <td>{{$dt->customs}}</td>
                        <td>{{$dt->total_value_d}}</td>
                        <td>{{$dt->total_value_kwd}}</td>
                        <td>{{$dt->stamping}}</td>
                        <td>{{$dt->final_total}}</td>
                      </tr>
                      @endforeach
                      <tr>
                      <td colspan="14" style="text-align: right; font-weight:bold">Total:</td>
                      <td>{{$gmc}}</td>
                      <td>{{$dmc}}</td>
                      <td>{{$total}}</td>
                      <td>{{$customs}}</td>
                      <td>{{$total_value_d}}</td>
                      <td>{{$total_value_kwd}}</td>
                      <td>{{$stamping}}</td>
                      <td>{{$final_total}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="myModal" class="modal">
<span class="close">&times;</span>
<img class="modal-content" id="img01">
</div>
<script>
function getImageId(imageId){
var modal = document.getElementById("myModal");
var img = document.getElementById(imageId);
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  $(".displaynav").css("display",'none');
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  modal.style.display = "none";
  $(".displaynav").css("display",'block');
}}
</script>
@endsection