@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Stock Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" id="cart" action="{{ route('stocks_upsert') }}" enctype="multipart/form-data">
	@csrf
    <div class="row">
        <div class="col-sm-4 col-lg-6">
			<div class="form-group">
			    <label>Supplier*</label>
				<select name="supplier_id" id="" class="form-control" required>
		        <option value="">Select Supplier</option>
				@foreach($suppliers as $supplier)
		        <option value="{{$supplier->id}}">{{$supplier->company_name}} (+{{$supplier->mobile_code}} {{$supplier->mobile}})</option>
		        @endforeach
			    </select>
				@error('supplier_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		</div>
		<ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item" onclick="hidetab(1)">
            <a class="nav-link active" id="table-tab" data-toggle="tab" href="#tabletab" role="tab" aria-controls="tabletab" aria-selected="true">Table</a>
          </li>
          <li class="nav-item" onclick="hidetab(2)">
            <a class="nav-link" id="excel-tab" data-toggle="tab" href="#exceltab" role="tab" aria-controls="exceltab" aria-selected="false">Import Excel</a>
          </li>
        </ul>

		<div class="tab-pane fade show active"  style="display:block" id="tabletab" role="tabpanel" aria-labelledby="table-tab">
		<div class="table-responsive">
		<table border=1 style="margin-top:5%;" class="table-view" name="cart">
		<thead class="text-primary text-center">
				<th>
					Code
				</th>
				<th>
					Design
				</th>
				<th>
					Image
				</th>
				<th>
					GW
				</th>
				<th>
					DW
				</th>
				<th>
					Gold + Lab
				</th>
				<!-- <th>
					Gold + Lab(in %)
				</th> -->
				<th>
					Dia P/Ct
				</th>
				<th>
					Dia Value
				</th>
				<th>
					Sub Total
				</th>
				<th>
					Customs
				</th>
				<th>
					Total Value($)
				</th>
				<th>
					Total Value(KWD)
				</th>
				<th>
					Stamping(KWD)
				</th>
				<th>
					Final Total
				</th>
				<th>
					Item
				</th>
				<th>
					Cost
				</th>
				<th>
					Action
				</th>
			</thead>
			<tbody>
				<tr class="line_items">
				    <td>
		            	<input style="width: 120px;" type="text" value="" name="code" id="code" class="form-control" placeholder="Code">
		        	</td>
					<td>
		            	<input style="width: 120px;" type="text" value="" name="design" id="design" class="form-control" placeholder="Design">
		        	</td>
					<td>
		            	<input type="file" name="image" id="image" style="width: 83px;">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="0" name="gw" id="gw" class="form-control" placeholder="GW">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="0" name="dw" id="dw" class="form-control" placeholder="DW">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="0" name="gl" id="gl" class="form-control" placeholder="Gold + Lab" jAutoCalc="{gw}*{{$settings->gold_lab}}">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="0" name="diap" id="diap" class="form-control" placeholder="Dia P/Ct">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="0" name="diav" id="diav" class="form-control" placeholder="Dia Value" jAutoCalc="{dw}*{diap}">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="0" name="sub_total" id="sub_total" class="form-control" placeholder="Sub Total" jAutoCalc="{gl}+{diav}">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="0" name="customs" id="customs" class="form-control" placeholder="Customs" jAutoCalc="({sub_total}*{{$settings->custom_charge}})/100">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="0" name="total_value_d" id="total_value_d" class="form-control" placeholder="Total Value($)" jAutoCalc="{sub_total}+{customs}">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="0" name="total_value_kwd" id="total_value_kwd" class="form-control" placeholder="Total Value(KWD)" jAutoCalc="{total_value_d}*{{$settings->sales_rate_kwd}}">
		        	</td>
					<td>
		            	<input style="width: 60px;" type="text" value="{{$settings->stamping_charge}}" name="stamping" id="stamping" class="form-control" placeholder="Stamping(KWD)" readonly="readonly">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="0" name="final_total" id="final_total" class="form-control" placeholder="Final Total" jAutoCalc="{total_value_kwd}+{stamping}">
		        	</td>
					<td>
		            	<input style="width: 120px;" type="text" value="" name="item" id="item" class="form-control" placeholder="Item">
		        	</td>
					<td>
		            	<input style="width: 120px;" type="text" value="0" name="cost" id="cost" class="form-control" placeholder="Cost">
		        	</td>
					<td><button class="btn btn-danger font-weight-bold row-remove">Remove</button></td>
                </tr>
		<tr style="border:0;">
			<td colspan="17"><button class="btn btn-success font-weight-bold row-add">Add Row</button></td>
		</tr>
		</tbody>
	</table>
        </div>
</div>
<div class="tab-pane fade" style="display:none" id="exceltab" role="tabpanel" aria-labelledby="excel-tab">
<div class="row" style="margin-top: 4%;margin-bottom: 4%;">
        <div class="col-sm-4 col-lg-6">
			<div class="form-group">
			    <label>Import Excel</label>
				<input type="file" name="excel">
				@error('supplier_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		</div>
</div>
	<a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
@endsection