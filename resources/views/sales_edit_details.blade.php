@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Edit Sales Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('sales_editPost',$details->id) }}">
	@csrf
  <div class="row" id="retailerrow" style="display:flex;">
        <div class="col-sm-4">
			<div class="form-group">
			<label>Retailer*</label>
				<select name="retailer_id" class="form-control" required>
		        <option value="">Select Retailer</option>
				@foreach($retailers as $retailer)
		        <option value="{{$retailer->id}}" <?= ($details->retailer_id == $retailer->id)?'selected':''?>>{{$retailer->company_name}} (+{{$retailer->mobile_code}} {{$retailer->mobile}})</option>
		        @endforeach
			    </select>
				@error('retailer_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
    </div>

<div class="table-responsive">
		<table class="table" id="myTable">
		<thead class="text-primary">
				<th style="font-size: 12px;">Code</th>
				<th style="font-size: 12px;">Design</th>
				<th style="font-size: 12px;">Image</th>
				<th style="font-size: 12px;">GW</th>
				<th style="font-size: 12px;">DW</th>
				<th style="font-size: 12px;">Gold&nbsp;+&nbsp;Lab</th>
				<th style="font-size: 12px;">Dia&nbsp;P/Ct</th>
				<th style="font-size: 12px;">Sub&nbsp;Total</th>
				<th style="font-size: 12px;">Custom</th>
				<th style="font-size: 12px;">Total&nbsp;Value<br>($)</th>
				<th style="font-size: 12px;">Total&nbsp;Value<br>KWD</th>
				<th style="font-size: 12px;">St.&nbsp;KWD</th>
				<th style="font-size: 12px;">Total&nbsp;Amount</th>
			</thead>
			<tbody>
				@foreach($sales as $sale)
				<tr>
				<td>{{ $sale->code }}</td>
				<td>{{ $sale->design}}</td>
				<td><img src ="..\{{$sale->image}}" width="120" height="70" style="max-width: none;" alt="No Image"></td>
				<td>{{ $sale->gw}}</td>
				<td>{{ $sale->dw}}</td>
				<td>{{ $sale->gold_lab}}</td>
				<td>{{ $sale->dia_pct}}</td>
				<td>{{ $sale->sub_total}}</td>
				<td>{{ $sale->customer_val}}</td>
				<td>{{ $sale->tot_val}}</td>
				<td>{{ $sale->tot_val_kwd}} KD</td>
				<td>{{ $sale->stm_kwd}} KD</td>
				<td>{{ $sale->tot_amt}}</td>
			    </tr>
				@endforeach
			<tr>
			<td colspan="3" style="text-align: right; font-weight:bold; border: 1px solid;">Total:</td>
			<td style="border: 1px solid;">{{ round($gw_total,2) }}</td>
		<td style="border: 1px solid;">{{ round($dw_total,2)}}</td>
		<td style="border: 1px solid;">{{ round($gl,2)}}</td>
		<td style="border: 1px solid;">{{ round($dp,2)}}</td>
		<td style="border: 1px solid;">{{ round($sub_total)}}</td>
		<td style="border: 1px solid;">{{ round($cs,2)}}</td>
		<td style="border: 1px solid;">{{ round($total_value_d_total)}}</td>
		<td style="border: 1px solid;">{{ round($total_value_kwd_total)}}</td>
		<td style="border: 1px solid;">{{ round($tot_stm_kwd)}} KD</td>
		<td style="border: 1px solid;">{{ round($total_amt)}}</td>
		<input type="hidden" value="{{ round($total_amt)}}" name="total_value_kwd_total" id="total_value_kwd_total">
		</tr>
		</tbody>
	</table>
        </div>
		<div>
			<table class="table">
				<tbody>
				<tr>
				<td style="border-top:1px;">Description</td>
				<td style="border-top:1px;">Discount</td>
				<td style="border-top:1px;">Freight</td>
				<td style="border-top:1px;">Sub Total</td>
				<td style="border-top:1px;">VAT - ({{$settings->vat}}%)</td>
				<td style="border-top:1px;">Total Value</td>
			</tr>
			<tr>
				<td width="40%">
				<textarea class="form-control" name="note" id="note" placeholder="Special Note">{{$details->note}}</textarea>
                </td>
                <td>
                <input type="text" value="{{ round($details->discount,2)}}" name="discount" id="discount" class="form-control" placeholder="Discount" onkeyup="total_cal()">
                </td>
			    <td>
				<input type="text" value="{{ round($details->freight,2)}}" name="freight" id="freight" class="form-control" placeholder="Freight" onkeyup="total_cal()">
                </td>
				<td>
				<input type="text" value="{{ round($total_amt,2) }}" name="sub_total" id="sub_total" class="form-control" placeholder="Sub Total" readonly>
                </td>
                <td>
				<input type="text" value="{{ round($details->vat,2) }}" name="vat" id="vat" class="form-control" placeholder="VAT" readonly>
				<input type="hidden" name="cal_vat" id="cal_vat" value="{{$settings->vat}}">
                </td>
                <td>
				<input type="text" value="{{ round($total_amt,2) }}" name="total" id="total" class="form-control" placeholder="Total" readonly>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
<script>
function total_cal() {
	var total_value_kwd_total = parseFloat($("#total_value_kwd_total").val()).toFixed(2);
	let vat_cal = parseFloat($("#cal_vat").val()).toFixed(2);
	var discount = parseFloat($("#discount").val()).toFixed(2);
	var freight = parseFloat($("#freight").val()).toFixed(2);
	var sub_total = parseFloat(parseFloat(total_value_kwd_total)-parseFloat(discount)+parseFloat(freight)).toFixed(2);
	var vat_total = parseFloat((sub_total*vat_cal)/100).toFixed(2);
	var total = Math.round(parseFloat(parseFloat(sub_total)+parseFloat(vat_total)).toFixed(2));
	$("#vat").val(vat_total);
	$("#sub_total").val(sub_total); 
	$("#total").val(total);
	$("#amount").val(total); 
	$("#due").val(total)
}

</script>
@endsection