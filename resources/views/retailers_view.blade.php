@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Retailer Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Company Name: </label><label class="col-md-10 col-form-label">{{$data->company_name}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Contact Person: </label><label class="col-md-10 col-form-label">{{$data->contact_person}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Address: </label><label class="col-md-10 col-form-label">{{$data->address}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Civil/CR. No.: </label><label class="col-md-10 col-form-label">{{$data->civil_no}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Mobile No.: </label><label class="col-md-10 col-form-label">+{{$data->mobile_code}} {{$data->mobile}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Email ID: </label><label class="col-md-10 col-form-label">{{$data->email}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Total Sale: </label><label class="col-md-10 col-form-label">{{$total}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Due Amount: </label><label class="col-md-10 col-form-label">{{$due}}</label></div>

  <!-- <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a> -->
</div>

<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Retailer History</h4>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table data-table">
        <thead class="text-primary">
            <th>Sr. No</th>
            <th>Date</th>
            <th>Invoice No.</th>
            <th>Total Amount</th>            
            <th>Paid</th>
            <th>Due</th>
            <th>Payment Status</th>
            <th>Action</th>
        </thead>
        <tbody>
            @php $sub_total=0;$total=0;$amount=0;$cash=0;$knet=0;$due=0; @endphp
            @foreach($secondData as $key=>$dt)
            <tr>
            <td>{{$key+1}}</td>
            <td>{{date("d/M/Y", strtotime($dt->created_at))}}</td>
            <td>{{$dt->invoiceno}}</td>
            <td>{{round($dt->total,3)}}</td>
            <td>{{round($dt->amount,3)}}</td>
            <td>{{round($dt->due,3)}}</td>
            <td style="text-align: center;"><?= ($dt->total == $dt->amount )?'<div class="btn btn-icon btn-success" style="line-height: 35px;">Paid</div>':'<div class="btn btn-icon btn-danger" style="line-height: 35px;">Due</div>' ?> </td>
            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#paymentModal-{{$dt->id}}">Credit</button>
           <!--  <a href="{{ route('invoice_sales_download',$dt->id) }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-download" aria-hidden="true"></i></a> -->
        </td>
            </tr>
            @php $sub_total+=$dt->sub_total;$total+=$dt->total;$amount+=$dt->amount;$due+=$dt->due; @endphp
            @endforeach
            <tr style="background: #f96332;color:white;">
                <td colspan="3" style="text-align: center;">{{'TOTAL'}}</td>
                <td>{{$total}}</td>
                <td>{{$amount}}</td>
                <td>{{$due}}</td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        </table>
    </div>
</div>
  <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
</div>
@foreach($secondData as $dtx)
<div class="modal fade" id="paymentModal-{{ $dtx->id }}" >
      <div class="modal-content" style="max-width: 53%;margin-left: 28%;">
         <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Add Sales Payment</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
            </div>
        <div class="modal-body" >
          <div class="modal-dialog">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto; " id="preloader_sec" class="preloader_sec"></div>
          </div>
          <form method="post" action="{{ route('post-sale-payment')}}">
            @csrf
          <table class="table" id="myTable_2">
            <tbody>
            <tr>
                <td style="border-top:1px;">Amount</td>
                <td style="border-top:1px;">Sale Account</td>
                <td style="border-top:1px;">Pay Amount</td>
                <td style="border-top:1px;">Sale Account</td>
                <td style="border-top:1px;">Pay Amount</td>
                <td style="border-top:1px;">Due</td>
            </tr>
            <tr class="line_items">
                <td>
                <input type="hidden" value="{{ $dtx->id}}" name="id" readonly>   
                <input type="text" value="{{ round($dt->due,2) }}" name="amount" id="amountAdd" class="form-control amountAdd" placeholder="Amount" readonly>
                </td>
                <td>
                <select name="account_first" class="form-control" required>
                    <option value="">Select Account..</option>
                    @foreach($accounts as $account)
                    <option value="{{$account->id}}">{{ $account->name}}</option>
                    @endforeach
                </select>
                </td>
                <td>
                <input type="text" value="0" name="cash_first" id="cash_first" class="form-control cash_first" placeholder="Pay Amount " onkeyup="due_cal()" required>
                <input type="hidden" value="0" name="due_first" id="due_first" readonly>
                </td>
                <td>
                    <select name="account_sec" class="form-control" >
                    <option value="">Select Account..</option>
                    @foreach($accounts as $account)
                    <option value="{{$account->id}}">{{ $account->name}}</option>
                    @endforeach
                </select>
                </td>
                <td>
                <input type="text" value="0" name="cash_sec" id="cash_sec" class="form-control cash_sec" placeholder="Pay Amount " onkeyup="due_cal_sec()">
                </td>
                <td>
                <input type="text" value="{{ round($dt->due,2) }}" name="due" id="due" class="form-control due" placeholder="Due" readonly>
                </td>
            </tr>
            <tr>
                <td colspan="6"><button type="submit" class="btn btn-primary">Add Payment</button></td>
            </tr>
        </tbody>
    </table>
</form>
        </div>
        </div>
      </div>
      
    </div>
  </div>

@endforeach

@endsection
@section('script')
<script type="text/javascript">
function due_cal() {
    var cash = parseFloat($(".cash_first").val()).toFixed(2);
    var amount = parseFloat($(".amountAdd").val()).toFixed(2);
    var due = parseFloat(parseFloat(amount)-(parseFloat(cash))).toFixed(2);
    $(".due").val(due);
    $("#due_first").val(due);
}

function due_cal_sec() {
    var cash = parseFloat($(".cash_first").val()).toFixed(2);
    var cash_sec = parseFloat($(".cash_sec").val()).toFixed(2);
    var amount = parseFloat($(".amountAdd").val()).toFixed(2);
    var due = parseFloat(parseFloat(amount)-(parseFloat(cash)+parseFloat(cash_sec))).toFixed(2);
    $(".due").val(due);
}
</script>
@endsection