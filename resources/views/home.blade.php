

<!DOCTYPE html>
<html lang="en">
    <head>


    <meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/assets/img/apple-icon.png')}}">
<link rel="icon" type="image/png" href="{{ asset('public/assets/img/favicon.png')}}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>



     Dashboard



</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />


<!--  Social tags      -->
<meta name="keywords" content="">
<meta name="description" content="">

<!--     Fonts and icons     -->

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<!-- CSS Files -->

<link href="{{ asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/font-style.css')}}">
<style>
  .nav-tabs .nav-item .active{
    background-color: #f96332;
    color: #fff;
    border: 1px solid #f96332 !important;
  }
  
  .nav-tabs .nav-item .active:hover {
    color: #000;
    border: 1px solid #f96332 !important;
  }
</style>



<!-- <link rel="stylesheet" type="text/css" href="https://demos.creative-tim.com/now-ui-dashboard-pro/assets/css/now-ui-dashboard.min.css?v=1.6.0"> -->




<!-- CSS Just for demo purpose, don't include it in your project -->
<!-- <link href="{{ asset('public/assets/demo/demo.css')}}" rel="stylesheet" /> -->
    
        <div class="wrapper ">
          
            <div class="sidebar" data-color="orange">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->

    <div class="logo">
        <a href="#" class="simple-text logo-mini">
          DC
        </a>
        <a href="#" class="simple-text logo-normal">
          Diamond CRM
        </a>
      </div>

    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">

          <li class="@if(url()->current()==route('index')) active @endif">
            <a href="{{ route('index') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize; font-size: 14px;font-weight: 600; font-family: cambria !important; padding-right: 17px;">Dashboard</p>
            </a>
          </li>

           <!-- <li class="@if(url()->current()==route('sales_list')) active @endif">
            <a href="{{ route('sales_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Sales</p>
            </a>
          </li> -->


          <li onclick="hidefunctionSales()"><a href="#"><i class="now-ui-icons shopping_bag-16"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Sales</p></a></li>
           <li id="add_sales" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('sales_details')) active @endif">
            <a href="{{ route('sales_details') }}">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Add Sales</p>
            </a>
          </li>
          <li id="sales_list" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('sales_list')) active @endif">
            <a href="{{ route('sales_list') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Sales List</p>
            </a>
          </li>
         

          <!-- ------------------------------------------- stock ------------------------------ -->

        <li onclick="hidefunctionStock()"><a href="#"><i class="now-ui-icons shopping_bag-16"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Stock</p></a></li>
          <li id="stock_list" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('stocks_list')) active @endif">
            <a href="{{ route('stocks_list') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Stock List</p>
            </a>
          </li>
          <li id="stock_history" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('stocks_history')) active @endif">
            <a href="{{ route('stocks_history') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Stock History</p>
            </a>
          </li>


          <!-- ------------------------------------------- Return ------------------------------ -->

        <li onclick="hidefunctionReturn()" ><a href="#"><i class="now-ui-icons shopping_delivery-fast"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Return</p></a></li>
          <li id="salereturn_list" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('sale-return')) active @endif">
            <a href="{{ route('sale-return') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Sale</p>
            </a>
          </li>



          <!-- ------------------------------- accounts -------------------------------- -->


<li onclick="hidefunctionAccount()"><a href="#"><i class="now-ui-icons business_bank"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Accounting</p></a></li>
          <li id="myaccount" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('account_add')) active @endif">
            <a href="{{ route('account_add') }}">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Add Account</p>
            </a>
          </li>
          <li id="myaccounts" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('accounts_list')) active @endif">
            <a href="{{ route('accounts_list') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Accounts List</p>
            </a>
          </li>
           <li id="myaccountss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('money_transfer')) active @endif">
            <a href="{{ route('money_transfer') }}">
              <i class="now-ui-icons business_money-coins"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Money Transfer</p>
            </a>
          </li>
          <li id="myaccountsss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('balance_sheet')) active @endif">
            <a href="{{ route('balance_sheet') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Balance Sheet</p>
            </a>
          </li>

          <!-- ---------------------------------- Account Statement --------------------------  -->


            <li id="myaccountssss" style="display:none;margin-left: 10%;">
            <a href="#" data-toggle="modal" data-target="#StatementModal">
              <i class="now-ui-icons business_badge"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Account Statement</p>
            </a>
          </li>


          <li onclick="hidefunctionPurchase()"><a href="#"><i class="now-ui-icons shopping_box"></i><p style="text-transform: capitalize; font-size: 14px;font-weight: 600; font-family: cambria !important;">Purchase</p></a></li>

          <li id="purchase_new" style="display:none;margin-left: 10%;"  class="@if(url()->current()==route('purchase_new')) active @endif">
            <a href="{{ route('purchase_new') }}">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Add Purchase </p>
            </a>
          </li>      
          
          <li id="purchase_new_sec" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('purchase_list')) active @endif">
            <a href="{{ route('purchase_list') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Purchase List </p>
            </a>
          </li>
          


<!-- ----------------------------------- reports ----------------------------------------- -->
          
          <li onclick="hidefunction()"><a href="#"><i class="now-ui-icons media-2_sound-wave"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Reports</p></a></li>
         <!--  <li id="myreportsss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('creditor_list')) active @endif">
            <a href="{{ route('creditor_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Creditors</p>
            </a>
          </li> -->
          <li id="myreportssss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('expense_list')) active @endif">
            <a href="{{ route('expense_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">General Expense</p>
            </a>
          </li>

<!-- ------------------------------------ Barcode---------------------------------->

          <li onclick="hidefunctionBarcode()"><a href="#"><i class="now-ui-icons design_bullet-list-67"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Barcode</p></a></li>
          <li id="barcode" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('barcode')) active @endif">
            <a href="{{ route('barcode') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Barcode Setup</p>
            </a>
          </li>
         <!--  <li id="barcodes" style="display:none;margin-left: 10%;" class="">
            <a href="">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Barcode-Printing</p>
            </a>
          </li> -->

          
<!-- -------------------------------------- settings ------------------------------ -->          

          <li onclick="hidefunctionSettings()"><a href="#"><i class="now-ui-icons loader_gear"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Settings</p></a></li>
           <li id="mysetting" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('company_details')) active @endif">
            <a href="{{ route('company_details') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Company Profile</p>
            </a>
          </li>
          <li id="mysettings" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('settings')) active @endif">
            <a href="{{ route('settings') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">General Settings</p>
            </a>
          </li>
         
          <li id="mysettingss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('retailers_list')) active @endif">
            <a href="{{ route('retailers_list') }}">
              <i class="now-ui-icons shopping_shop"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Retailers</p>
            </a>
          </li>

          <li id="mysettingsss" style="display:none;margin-left: 10%;"  class="@if(url()->current()==route('suppliers_list')) active @endif">
            <a href="{{ route('suppliers_list') }}">
              <i class="now-ui-icons shopping_delivery-fast"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Suppliers</p>
            </a>
          </li>
        </ul>
      </div>
</div>


            <div class="main-panel" id="main-panel">
              <!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      
      <a class="navbar-brand" href="#pablo"></a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-bar navbar-kebab"></span>
      <span class="navbar-toggler-bar navbar-kebab"></span>
      <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>

      <div class="collapse navbar-collapse justify-content-end" id="navigation">
      
        
        

<ul class="navbar-nav displaynav" style="display:block">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="now-ui-icons users_single-02"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="{{ route('profile') }}">Profile</a>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                </div>
              </li>
            </ul>
      </div>
  </div>
</nav>
<!-- End Navbar -->

<div class="panel-header panel-header-lg">
  <canvas id="bigDashboardChart"></canvas>
</div>
<div class="content">

<div class="row">
    <div class="col-md-12">
        <div class="card card-stats">
            <div class="card-body">

        <div class="row">
          <div class="col-md-12">
           
              <div class="filter-toggle btn-group">
              <button class="btn btn-secondary date-btn active" data-start_date="{{date('Y-m-d')}}" data-end_date="{{date('Y-m-d')}}">Today</button>
              <button class="btn btn-secondary date-btn" data-start_date="{{date('Y-m-d', strtotime(' -7 day'))}}" data-end_date="{{date('Y-m-d')}}">Last 7 Days</button>
              <button class="btn btn-secondary date-btn" data-start_date="{{date('Y').'-'.date('m').'-'.'01'}}" data-end_date="{{date('Y-m-d')}}">This Month </button>
              <button class="btn btn-secondary date-btn" data-start_date="{{date('Y').'-01'.'-01'}}" data-end_date="{{date('Y').'-12'.'-31'}}">This Year </button>
            </div>
          </div>
      </div>


                <div class="row">
                    <div class="col-md-3">
                        <div class="statistics">
                            <div class="info">
                                <div class="icon icon-primary">
                                    <i class="now-ui-icons business_money-coins"></i>
                                </div>
                                <h3 class="info-title tot_sale">{{$tot_sale}}</h3>
                                <h6 class="stats-title">Sale</h6>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-md-3">
                        <div class="statistics">
                            <div class="info">
                                <div class="icon icon-primary">
                                    <i class="now-ui-icons arrows-1_share-66"></i>
                                </div>
                                <h3 class="info-title tot_sale_return">{{round($return_cnt)}}</h3>
                                <h6 class="stats-title">Sale Return</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="statistics">
                            <div class="info">
                                <div class="icon icon-success">
                                    <i class="now-ui-icons business_money-coins"></i>
                                </div>
                                <h3 class="info-title tot_purchase">{{$tot_purchase}}</h3>
                                <h6 class="stats-title">Purchase</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="statistics">
                            <div class="info">
                                <div class="icon icon-danger">
                                    <i class="now-ui-icons objects_support-17"></i>
                                </div>
                                <h3 class="info-title tot_profit">{{ round($total_profit)}}</h3>
                                <h6 class="stats-title">Total Profit</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-7">
              <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                  <h4 style="font-size: 14px;font-weight: 600;">Recent Transaction</h4>
                  <div class="right-column">
                    <div class="badge badge-primary">Latest 5</div>
                  </div>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" href="#sale-latest" role="tab" data-toggle="tab" style="line-height: 5px;">Sale</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#purchase-latest" role="tab" data-toggle="tab" style="line-height: 5px;">Purchase</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade show active" id="sale-latest">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Date</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Reference</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Customer</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Status</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Grand Total</th>
                            </tr>
                          </thead>
                          <tbody>
                           @php $sales = \App\Models\SaleDetail::orderBy('id','desc')->paginate(5); @endphp
                            @foreach($sales as $sale)
                            <tr>
                              <td style="text-align: center;">{{ date("d/M/Y", strtotime($sale->created_at)) }}</td>
                              <td style="text-align: center;">{{ $sale->invoiceno}}</td>
                              <td style="text-align: center;">{{ $sale->retailer->company_name}}</td>
                              <td style="text-align: center;"><?= ($sale->total == $sale->amount )?'<div class="badge badge-success">Paid</div>':'<div class="badge badge-danger">Due</div>' ?> </td>                              
                             <td style="text-align: center;">{{ round($sale->amount)}}</td>
                            </tr>
                            @endforeach    
                          </tbody>
                        </table>
                      </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="purchase-latest">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Date</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Reference</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Supplier</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Status</th>
                              <th style="font-size: 14px;font-weight: 500;text-align: center;">Grand Total</th>
                            </tr>
                          </thead>
                          <tbody>
                             @php $purchases = \App\Models\Purchase::orderBy('id','desc')->paginate(5); @endphp
                            @foreach($purchases as $purchase)
                            <tr>
                              <td style="text-align: center;">{{ date("d/M/Y", strtotime($purchase->created_at)) }}</td>
                              <td style="text-align: center;">{{ $purchase->order_no}}</td>
                              <td style="text-align: center;">{{ $purchase->supplier->company_name}}</td>
                             <td style="text-align: center;"><?= ($purchase->grand_total == $sale->paid_amount )?'<div class="badge badge-success">Paid</div>':'<div class="badge badge-danger">Due</div>' ?> </td>                      
                            <td style="text-align: center;">{{ round($sale->grand_total)}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                      </div>
                  </div>
                 
                </div>
              </div>
            </div>
             <div class="col-md-5">
              <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                  <h4 style="font-size: 14px;font-weight: 600;">Best Seller {{date('F')}}</h4>
                  <div class="right-column">
                    <div class="badge badge-primary">top 5</div>
                  </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                      <thead>
                       
                         
                        <tr>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">SL No</th>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">Customer</th>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">Grand total</th>
                        </tr>

                      </thead>
                      <tbody>
                        <?php $i =1;  
                        foreach($best_selling_qty as $qty){
                          // $sales = \App\Models\SaleDetail::where('retailer_id',$qty->retailer_id)->get();
                          // $getId = '';
                          //  foreach($sales as $sale){
                          //   $getId .=  ",".$sale->id;
                          //  }
                          //  $count_ids = \App\Models\Sale::whereIn('sale_detail_id',explode(',',$getId))->get();
                          //  $getTot = $count_ids->sum('tot_amt');

                        ?>
                        <tr>
                          <td style="text-align: center;">{{ $i++ }}</td>
                          <td style="text-align: center;">{{ $qty->retailer->company_name }}</td>
                          <td style="text-align: center;">{{ round($qty->total_price) }}</td>
                        </tr>
                       <?php  } ?>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                  <h4>Best Seller {{date('Y')}} (Qty)</h4>
                  <div class="right-column">
                    <div class="badge badge-primary">top 5</div>
                  </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                      <thead>

                        <tr>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">SL No</th>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">Customer </th>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">Qty</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $a = 1; @endphp
                        @foreach($yearly_best_selling_qty as $s)
                        <tr>
                          <td style="text-align: center;">{{ $a++}}</td>
                          <td style="text-align: center;">{{ $s->retailer->company_name}}</td>
                          <td style="text-align: center;">{{ $s->total_qty }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                  <h4>Best Seller {{date('Y') }} (Price) </h4>
                  <div class="right-column">
                    <div class="badge badge-primary">top 5</div>
                  </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">SL No</th>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">Customer</th>
                          <th style="font-size: 14px;font-weight: 500;text-align: center;">Grand total</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $a = 1; @endphp
                       @foreach($yearly_best_selling_price as $price)

                        <tr>
                          <td style="text-align: center;">{{ $a++}}</td>
                          <td style="text-align: center;">{{$price->retailer->company_name}}</td>
                          <td style="text-align: center;">{{round($price->total_price)}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
</div>

<!-- <div class="row">
    <div class="col-lg-4 col-md-6">
      <div class="card card-chart">
        <div class="card-header">
          <h5 class="card-category">Active Users</h5>
          <h2 class="card-title">34,252</h2>
          <div class="dropdown">
            <button type="button" class="btn btn-round btn-icon dropdown-toggle btn-outline-default no-caret" data-toggle="dropdown">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
              <a class="dropdown-item text-danger" href="#">Remove Data</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="chart-area">
            <canvas id="activeUsers"></canvas>
          </div>
          <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td>
                            <div class="flag">
                                <img src="{{ asset('public/assets/img/US.png')}}">
                        </div></td>
                        <td>USA</td>
                        <td class="text-right">
                            2.920
                        </td>
                        <td class="text-right">
                            53.23%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="flag">
                                <img src="{{ asset('public/assets/img/DE.png')}}">
                        </div></td>
                        <td>Germany</td>
                        <td class="text-right">
                            1.300
                        </td>
                        <td class="text-right">
                            20.43%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="flag">
                                <img src="{{ asset('public/assets/img/AU.png')}}">
                        </div></td>
                        <td>Australia</td>
                        <td class="text-right">
                            760
                        </td>
                        <td class="text-right">
                            10.35%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="flag">
                                <img src="{{ asset('public/assets/img/GB.png')}}">
                        </div></td>
                        <td>United Kingdom</td>
                        <td class="text-right">
                            690
                        </td>
                        <td class="text-right">
                            7.87%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="flag">
                                <img src="{{ asset('public/assets/img/RO.png')}}">
                        </div></td>
                        <td>Romania</td>
                        <td class="text-right">
                            600
                        </td>
                        <td class="text-right">
                            5.94%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="flag">
                                <img src="{{ asset('public/assets/img/BR.png')}}">
                        </div></td>
                        <td>Brasil</td>
                        <td class="text-right">
                            550
                        </td>
                        <td class="text-right">
                            4.34%
                        </td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer">
          <div class="stats">
                <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
            </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6">
      <div class="card card-chart">
        <div class="card-header">
          <h5 class="card-category">Summer Email Campaign</h5>
          <h2 class="card-title">55,300</h2>
          <div class="dropdown">
            <button type="button" class="btn btn-round dropdown-toggle btn-outline-default btn-icon no-caret" data-toggle="dropdown">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
              <a class="dropdown-item text-danger" href="#">Remove Data</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="chart-area">
            <canvas id="emailsCampaignChart"></canvas>
          </div>

          <div class="card-progress">
            <div class="progress-container">
              <span class="progress-badge">Delivery Rate</span>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                  <span class="progress-value">90%</span>
                </div>
              </div>
            </div>

            <div class="progress-container progress-success">
              <span class="progress-badge">Open Rate</span>
              <div class="progress">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                  <span class="progress-value">60%</span>
                </div>
              </div>
            </div>

            <div class="progress-container progress-info">
              <span class="progress-badge">Click Rate</span>
              <div class="progress">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 12%;">
                  <span class="progress-value">12%</span>
                </div>
              </div>
            </div>

            <div class="progress-container progress-warning">
              <span class="progress-badge">Hard Bounce</span>
              <div class="progress">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                  <span class="progress-value">5%</span>
                </div>
              </div>
            </div>

            <div class="progress-container progress-danger">
              <span class="progress-badge">Spam Report</span>
              <div class="progress">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0.11%;">
                  <span class="progress-value">0.11%</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="stats">
                <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
            </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4 col-md-6">
      <div class="card card-chart">
        <div class="card-header">
          <h5 class="card-category">Active Countries</h5>
          <h2 class="card-title">105</h2>
        </div>
        <div class="card-body">
          <div class="chart-area">
            <canvas id="activeCountries"></canvas>
          </div>
          <div id="worldMap" class="map"></div>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="now-ui-icons ui-2_time-alarm"></i> Last 7 days
          </div>
        </div>
      </div>
    </div>
</div> -->

<!-- <div class="row">
  <div class="col-md-12">

<div class="card">
    <div class="card-header">
        <h4 class="card-title"> Best Selling Products</h4>
        
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-shopping">
                <thead class="">
                    
<th  class="text-center" >
    
        
    
</th>

<th >
    
        Product
    
</th>

<th >
    
        Color
    
</th>

<th >
    
        Size
    
</th>

<th  class="text-right" >
    
        Price
    
</th>

<th  class="text-right" >
    
        Qty
    
</th>

<th  class="text-right" >
    
        Amount
    
</th>


                </thead>
                <tbody>
                    
                        
    

    
        
            <tr>
    <td>
        <div class="img-container">
            <img src="{{ asset('public/assets/img/saint-laurent.jpg')}}" alt="...">
        </div>
    </td>
    <td class="td-name">
        <a href="#jacket">Suede Biker Jacket</a>
        <br /><small>by Saint Laurent</small>
    </td>
    <td>
        Black
    </td>
    <td>
        M
    </td>
    <td class="td-number">
        <small>€</small>3,390
    </td>
    <td class="td-number">
        1
    </td>
    <td class="td-number">
        <small>€</small>549
    </td>

</tr>


        
    

    
        
            <tr>
    <td>
        <div class="img-container">
            <img src="{{ asset('public/assets/img/balmain.jpg')}}" alt="...">
        </div>
    </td>
    <td class="td-name">
        <a href="#pants">Jersey T-Shirt</a>
        <br /><small>by Balmain</small>
    </td>
    <td>
        Black
    </td>
    <td>
        M
    </td>
    <td class="td-number">
        <small>€</small>499
    </td>
    <td class="td-number">
        2
    </td>
    <td class="td-number">
        <small>€</small>998
    </td>

</tr>


        
    

    
        
            <tr>
    <td>
        <div class="img-container">
            <img src="{{ asset('public/assets/img/prada.jpg')}}" alt="...">
        </div>
    </td>
    <td class="td-name">
        <a href="#nothing">Slim-Fit Swim Short</a>
        <br /><small>by Prada</small>
    </td>
    <td>
        Red
    </td>
    <td>
        M
    </td>
    <td class="td-number">
        <small>€</small>200
    </td>
    <td class="td-number">
        1
    </td>
    <td class="td-number">
        <small>€</small>799
    </td>

</tr>
  
            <tr>
                <td colspan="5">
                </td>
                <td class="td-total">
                    Total
                </td>
                <td class="td-price">
                    <small>€</small>2,346
                </td>
            </tr>
        
    


                    
                </tbody>
            </table>
        </div>
    </div>
</div>

  </div>
</div> -->

                  </div>

                  <footer class="footer" >
    
    <div class=" container-fluid ">
         <div class="copyright" id="copyright">
            &copy; <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>Designed by <a href="#">Versatile International</a>. Coded by <a href="#">Versatile International</a>.
          </div>
    </div>
    
</footer>

               
             </div>
          
        </div>
        


<!--   Core JS Files   -->
<script src="{{ asset('public/assets/js/core/jquery.min.js')}}" ></script>

<script src="{{ asset('public/assets/js/core/popper.min.js')}}" ></script>

<script src="{{ asset('public/assets/js/core/bootstrap.min.js')}}" ></script>

<script src="{{ asset('public/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}" ></script>

<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGat1sgDZ-3y6fFe6HD7QUziVC6jlJNog"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<script src="{{ asset('public/assets/js/plugins/jquery-jvectormap.js')}}"></script>

<!-- Chart JS -->
<script src="{{ asset('public/assets/js/plugins/chartjs.min.js')}}"></script>



<script src="{{asset('public/assets/js/now-ui-dashboard.min.js?v=1.6.0')}}" type="text/javascript"></script>

<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('public/assets/demo/demo.js')}}"></script>


<script>
    $(document).ready(function(){
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();
      
      demo.initVectorMap();
      
    });
</script>
  <script>

function hidefunction() {
  var z = document.getElementById("myreportssss");

  if (z.style.display === "none") {
    z.style.display = "block";
  } else {
    z.style.display = "none";
  }
}

function hidefunctionSettings() {
  var w = document.getElementById("mysetting");
  var x = document.getElementById("mysettings");
  var y = document.getElementById("mysettingss");
  var z = document.getElementById("mysettingsss");
  if (w.style.display === "none") {
    w.style.display = "block";
  } else {
    w.style.display = "none";
  }

  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

    if (y.style.display === "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }

  if (z.style.display === "none") {
    z.style.display = "block";
  } else {
    z.style.display = "none";
  }
}

function hidefunctionPurchase() {
  var w = document.getElementById("purchase_new");
  var x = document.getElementById("purchase_new_sec");
  if (w.style.display === "none") {
    w.style.display = "block";
  } else {
    w.style.display = "none";
  }

  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function hidefunctionAccount() {
  var w = document.getElementById("myaccount");
  var x = document.getElementById("myaccounts");
  var y = document.getElementById("myaccountss");
  var z = document.getElementById("myaccountsss");
  var z_1 = document.getElementById("myaccountssss");
  if (w.style.display == "none") {
    w.style.display = "block";
  } else {
    w.style.display = "none";
  }

  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display == "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }

  if (z.style.display == "none") {
    z.style.display = "block";
  } else {
    z.style.display = "none";
  }

  if (z_1.style.display == "none") {
    z_1.style.display = "block";
  } else {
    z_1.style.display = "none";
  }
}

function hidefunctionStock()
{
   var x = document.getElementById("stock_list");
  //var y = document.getElementById("stock_history");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  // if (y.style.display == "none") {
  //   y.style.display = "block";
  // } else {
  //   y.style.display = "none";
  // }

}


function hidefunctionSales()
{
   var x = document.getElementById("sales_list");
  var y = document.getElementById("add_sales");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display == "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }

}


function hidefunctionBarcode()
{

 var x = document.getElementById("barcode");
 var y = document.getElementById("barcodes");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display == "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  } 
}

function hidefunctionReturn()
{
 var x = document.getElementById("salereturn_list");
 //var y = document.getElementById("barcodes");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  // if (y.style.display == "none") {
  //   y.style.display = "block";
  // } else {
  //   y.style.display = "none";
  // }
}

</script>
<script>
    // Show and hide color-switcher
    $(".color-switcher .switcher-button").on('click', function() {
        $(".color-switcher").toggleClass("show-color-switcher", "hide-color-switcher", 300);
    });

    // Color Skins
    $('a.color').on('click', function() {
        $.get('setting/general_setting/change-theme/' + $(this).data('color'), function(data) {
        });
        var style_link= $('#custom-style').attr('href').replace(/([^-]*)$/, $(this).data('color') );
        $('#custom-style').attr('href', style_link);
    });

    $(".date-btn").on("click", function() {
        $(".date-btn").removeClass("active");
        $(this).addClass("active");
        var start_date = $(this).data('start_date');
        var end_date = $(this).data('end_date');
        $.get('dashboard-filter/' + start_date + '/' + end_date, function(data) {
            dashboardFilter(data);
        });
    });

    function dashboardFilter(data){
        $('.tot_sale').hide();
        $('.tot_sale').html(Math.round(data[0]));
        $('.tot_sale').show(500);

        $('.tot_sale_return').hide();
        $('.tot_sale_return').html(Math.round(data[1]));
        $('.tot_sale_return').show(500);

        $('.tot_purchase').hide();
        $('.tot_purchase').html(Math.round(data[2]));
        $('.tot_purchase').show(500);

        $('.tot_profit').hide();
        $('.tot_profit').html(Math.round(data[3]));
        $('.tot_profit').show(500);
    }
</script>

    </body>

</html>
