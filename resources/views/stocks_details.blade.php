@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Stock Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" id="cart" action="{{ route('stocks_upsert') }}" enctype="multipart/form-data">
	@csrf
   <!--  <div class="row">
        <div class="col-sm-4 col-lg-6">
			<div class="form-group">
			    <label>Supplier*</label>
				<select name="supplier_id" id="" class="form-control" required>
		        <option value="">Select Supplier</option>
				@foreach($suppliers as $supplier)
		        <option value="{{$supplier->id}}">{{$supplier->company_name}} (+{{$supplier->mobile_code}} {{$supplier->mobile}})</option>
		        @endforeach
			    </select>
				@error('supplier_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		</div> -->
		<ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item" onclick="hidetab(1)">
            <a class="nav-link active" id="table-tab" data-toggle="tab" href="#tabletab" role="tab" aria-controls="tabletab" aria-selected="true">Table</a>
          </li>
          <li class="nav-item" onclick="hidetab(2)">
            <a class="nav-link" id="excel-tab" data-toggle="tab" href="#exceltab" role="tab" aria-controls="exceltab" aria-selected="false">Import Excel</a>
          </li>
        </ul>

		<div class="tab-pane fade show active"  style="display:block" id="tabletab" role="tabpanel" aria-labelledby="table-tab">
		<div class="table-responsive">
		<table border=1 style="margin-top:5%;" id="stockdetails_sec" class="table-view" name="cart">
		<thead class="text-primary text-center">
				<th>Code</th>
                    <th>Design</th>
                    <th>Image</th>
                    <th>Gold Wt</th>
                    <th>Dia Wt</th>
                    <th>Gold+Lab</th>
                    <th>Dia P/Ct</th>
                    <th>Dia Value</th>
                    <th>Sub Total</th>
                    <th>Customs</th>
                    <th>Total Value<br>$</th>
                    <th>Total Value<br>KWD</th>
                    <th>Stamping<br>KWD</th>
                    <th>Final Total</th>
                    <th>Item</th>
                    <th>Clarity</th>
                    <th>Grade</th>
                    <th>Stone PCS</th>
                    <th>Stone Wt </th>
				    <th>Action</th>
			</thead>
			<tbody>
				<tr class="line_items">
					<td>
		            	<input style="width: 110px;" type="text" value="" name="code" id="code" class="form-control" placeholder="Code">
		        	</td>
					<td>
		            	<input style="width: 110px;" type="text" value="" name="design" id="design" class="form-control" placeholder="Design">
		        	</td>
					<td>
		            	<input style="width: 83px;" type="file" name="image" id="image">
		        	</td>
					
					<td>
		            	<input style="width: 80px;" type="text" value="" name="gw" id="gw" class="form-control" placeholder="Gold Wt">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="dw" id="dw" class="form-control" placeholder="Diamond Wt">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="gl" id="gl" class="form-control" placeholder="Gold Lab">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="dp" id="dp" class="form-control" placeholder="Diamond P/Ct">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="dv" id="dv" class="form-control" placeholder="Diamond Value">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="st" id="st" class="form-control" placeholder="Sub Total">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="cus" id="cus" class="form-control" placeholder="Customs">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="tot_vl" id="tot_vl" class="form-control" placeholder="Total Value">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="tot_val_kwd" id="tot_val_kwd" class="form-control" placeholder="Total KWD Value">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="stm" id="stm" class="form-control" placeholder="Stamping KWD">
		        	</td>
					<td>
		            	<input style="width: 80px;" type="text" value="" name="final" id="final" class="form-control" placeholder="Final Total" >
		        	</td>
					<td>
		            	<input style="width: 110px;" type="text" value="" name="item" id="item" class="form-control" placeholder="Item" >
		        	</td>
					<td>
		            	<input style="width: 70px;" type="text" value="" name="cl" id="cl" class="form-control" placeholder="Clarity">
		        	</td>
					<td>
		            	<input style="width: 70px;" type="text" value="" name="grd" id="grd" class="form-control" placeholder="Grade">
		        	</td>
					<td>
		            	<input style="width: 70px;" type="text" name="pcs" id="pcs" class="form-control" placeholder="Stone PCS" >
		        	</td>
					<td>
					<input style="width: 70px;" type="text" name="stone_wt" id="stone_wt" class="form-control" placeholder="Stone Wt" >
		        	</td>
					<td><button class="btn btn-danger font-weight-bold row-remove-stock">Remove</button></td>
                </tr>
		<tr style="border:0;">
			<td colspan="22"><button class="btn btn-success font-weight-bold row-add">Add Row</button></td>
		</tr>
		</tbody>
	</table>
        </div>
</div>
<div class="tab-pane fade" style="display:none" id="exceltab" role="tabpanel" aria-labelledby="excel-tab">
<div class="row" style="margin-top: 4%;margin-bottom: 4%;">
        <div class="col-sm-4 col-lg-6">
			<div class="form-group">
			    <label>Import Excel</label>
				<input type="file" name="excel">
				@error('supplier_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
		</div>
</div>
	<a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
@endsection