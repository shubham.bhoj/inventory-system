@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Stock List</h4>
                <a href="{{ route('stocks_details') }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add New
					</a>
</div></div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table data-table">
                    <thead class="text-primary">
                    <th>ID</th>
                    <th>Supplier</th>
                    <th>Code</th>
                    <th>Design</th>
                    <th>Image</th>
                    <th>GW</th>
                    <th>DW</th>
        <th>Gold + Lab ({{$settings_data->gold_lab}}%)</th>
        <th>Dia P/Ct</th>
        <th>Dia Value</th>
				<th>Sub Total</th>
				<th>Customs({{$settings_data->custom_charge}}%)</th>
				<th>Total Value($)</th>
				<th>Total Value(KWD) ({{$settings_data->sales_rate_kwd}})</th>
				<th>Stamping (KWD) ({{$settings_data->stamping_charge}})</th>
				<th>Final Total</th>
				<th>Item</th>
				<th>Cost</th>
                    </thead>
                    <tbody>
                      @foreach($data as $dt)
                      <tr>
                      <td>{{$dt->id}}</td>
                        <td>{{$dt->company_name}} (+{{$dt->mobile_code}} {{$dt->mobile}})</td>
                        <td>{{$dt->code}}</td>
                        <td>{{$dt->design}}</td>
                        <td><img src="..\{{$dt->image}}" id="myImg{{$dt->id}}" onclick="getImageId('myImg{{$dt->id}}')" width="120" height="70" style="max-width: none;" alt="No Image"></td>
                        <td>{{$dt->gw}}</td>
                        <td>{{$dt->dw}}</td>
                        <td>${{$dt->gl}}</td>
                        <td>${{$dt->diap}}</td>
                        <td>${{$dt->diav}}</td>
                        <td>${{$dt->sub_total}}</td>
                        <td>${{$dt->customs}}</td>
                        <td>${{$dt->total_value_d}}</td>
                        <td>{{$dt->total_value_kwd}}</td>
                        <td>{{$dt->stamping}}</td>
                        <td>{{$dt->final_total}}</td>
                        <td>{{$dt->item}}</td>
                        <td>{{$dt->cost}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="myModal" class="modal">
<span class="close">&times;</span>
<img class="modal-content" id="img01">
</div>
<script>
function getImageId(imageId){
var modal = document.getElementById("myModal");
var img = document.getElementById(imageId);
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  $(".displaynav").css("display",'none');
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  modal.style.display = "none";
  $(".displaynav").css("display",'block');
}}
</script>
@endsection