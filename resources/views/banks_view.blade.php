@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Bank Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Name: </label><label class="col-md-10 col-form-label">{{$data->name}}</label></div>
  @if($data->type==1)
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Account</label><label class="col-md-10 col-form-label">{{$data->account}}</label></div>
  @endif
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Balance</label><label class="col-md-10 col-form-label">{{$data->balance}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Description</label><label class="col-md-10 col-form-label">{{$data->note}}</label></div>
  <!-- <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a> -->
</div>
  <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
</div>
@endsection