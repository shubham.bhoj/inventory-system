@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Sale Details</h4>
                <a href="{{ route('salesDetails-download', $id) }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5" target="_blank">
            Print
          </a>
</div></div>
              </div>
              <div class="card-body">

                <table class="table" >
        <thead class="text-primary">
            <tr>
            <th style="text-align: center;font-size: 12px;">Gold MC</th>
            <th style="text-align: center;font-size: 12px;">Dia P/Ct</th>
            <th style="text-align: center;font-size: 12px;">Custom</th>
            <th style="text-align: center;font-size: 12px;">KWD Convertion</th>
            </tr>
        </thead> 
          <tbody>
          <tr>
           <td align="center"><b>{{$retailer->master_gold}}</b></td>
           <td align="center"><b>{{$retailer->master_dia}}</b></td>
           <td align="center"><b>{{$retailer->master_cus}}</b></td>
           <td align="center"><b>{{$retailer->master_kwd}}</b></td>
         </tr>      
           
          </tbody>
        </table>

              <div class="table-responsive">
              <table border=1 class="table data-table">
                    <thead class="text-primary">
                    <th style="font-size: 12px;">No</th>
                    <th style="font-size: 12px;">Code</th>
                    <th style="font-size: 12px;">Design</th>
                    <th style="font-size: 12px;">Image</th>
                    <th style="font-size: 12px;">GW</th>
                    <th style="font-size: 12px;">DW</th>
                    <th style="font-size: 12px;">Gold+Lab</th>
                    <th style="font-size: 12px;">Dia Value</th>
                    <th style="font-size: 12px;">Sub Total</th>
                    <th style="font-size: 12px;">Custom</th>
                    <th style="font-size: 12px;">Total&nbsp;($)</th>
                    <th style="font-size: 12px;">Amount</th>
                    <th style="font-size: 12px;">St.&nbsp;KWD</th>
                    <th style="font-size: 12px;">T.&nbsp;Amount</th>
                    <th style="font-size: 12px;">Item</th>
                    <th style="font-size: 12px;">Clarity</th>
                    <th style="font-size: 12px;">Grade</th>
                    <th style="font-size: 12px;">St&nbsp;Pcs</th>
                    <th style="font-size: 12px;">St&nbsp;Wt</th>
                    </thead>
                    <tbody>
                      @php $key=1; @endphp
                      @foreach($data as $dt)
                      <tr>
                        <td>{{ $key++}}</td>
                        <td>{{ $dt->code}}</td>
                        <td>{{ $dt->design}}</td>
                        <td><img src="..\{{$dt->image}}" id="myImg{{$dt->id}}" onclick="getImageId('myImg{{$dt->id}}')" width="120" height="70" style="max-width: none;" alt="No Image"></td>
                        <td>{{$dt->gw}}</td>
                        <td>{{$dt->dw}}</td>
                        <td>${{$dt->gold_lab}}</td>
                        <td>${{$dt->dia_pct}}</td>
                        <td>${{$dt->sub_total}}</td>
                        <td>${{$dt->customer_val}}</td>
                        <td>${{$dt->tot_val}}</td>
                        <td>{{$dt->tot_val_kwd}} KD</td>
                        <td>{{$dt->stm_kwd}} KD</td>
                        <td>{{$dt->tot_amt}}</td>
                        <td>{{$dt->description}}</td>
                        <td>{{$dt->dq}}</td>
                        <td>{{$dt->cg}}</td>
                        <td>{{$dt->csp}}</td>
                        <td>{{$dt->csw}}</td>
                      </tr>
                      @endforeach
                      <tr>
                      <td colspan="4" style="font-weight:bold;text-align: center;">Total:</td>
                      <td>{{$gw_total}}</td>
                      <td>{{$dw_total}}</td>
                      <td>${{$gl}}</td>
                      <td>${{$dp}}</td>
                      <td>${{$totsub_total}}</td>
                      <td>${{$cs}}</td>
                      <td>${{$total_value}}</td>
                      <td>{{$tot_val_kwd}} KD</td>
                      <td>{{$tot_stm_kwd}} KD</td>
                      <td>{{$total_tot}}</td>
                      <td colspan="5" class="text-right"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="myModal" class="modal">
<span class="close">&times;</span>
<img class="modal-content" id="img01">
</div>
<script>
function getImageId(imageId){
var modal = document.getElementById("myModal");
var img = document.getElementById(imageId);
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  $(".displaynav").css("display",'none');
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  modal.style.display = "none";
  $(".displaynav").css("display",'block');
}}
</script>
@endsection