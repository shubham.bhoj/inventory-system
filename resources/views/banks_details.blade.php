@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left">Bank Details</h4>
            <h6 class="card-title float-right">Current Balance : {{$data->balance}}</h6>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('banks_upsert') }}">
	@csrf
	<input type="hidden" value="{{isset($data->id) ? $data->id : ''}}" name="id">
    <div class="row">
        <div class="col-sm-4">
			<div class="form-group">
				<label>Name*</label>
				<input type="text" value="{{isset($data->name) ? $data->name : old('name')}}" name="name" class="form-control" placeholder="Name" required>
				@error('name')
				<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
        @if($data->type==1)
        <div class="col-sm-4">
					<div class="form-group">
						<label>Account No.*</label>
						<input type="text" value="{{isset($data->account) ? $data->account : old('account')}}" name="account" class="form-control" placeholder="" required>
						@error('account')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
        @endif
        <div class="col-sm-4">
					<div class="form-group">
						<label>Add Balance*</label>
						<input type="number" value="{{isset($data->balance) ? $data->balance : old('balance')}}" name="balance" class="form-control" placeholder="Balance" required>
						@error('balance')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
		<div class="col-sm-6">
					<div class="form-group">
						<label>Description*</label>
						<textarea name="note" class="form-control" placeholder="Description">{{isset($data->note) ? $data->note : old('note')}}</textarea>
						@error('note')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
    </div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
@endsection