@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Retailer List</h4>
                <a href="{{ route('retailers_details') }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add New
					</a>
</div></div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table data-table">
                    <thead class="text-primary">
                      <!-- <th>ID</th> -->
                      <th>Company Name</th>
                      <th>Contact Person</th>
                      <!-- <th>Address</th> -->
                      <th>Civil/CR. No.</th>
                      <th>Mobile No.</th>
                      <!-- <th>Email ID</th> -->
                      <th>Action</th>
                    </thead>
                    <tbody>
                      @foreach($data as $dt)
                      <tr>
                        <!-- <td>{{$dt->id}}</td> -->
                        <td>{{$dt->company_name}}</td>
                        <td>{{$dt->contact_person}}</td>
                        <!-- <td>{{$dt->address}}</td> -->
                        <td>{{$dt->civil_no}}</td>
                        <td>+{{$dt->mobile_code}} {{$dt->mobile}}</td>
                        <!-- <td>{{$dt->email}}</td> -->
                        <td>
                        <a href="{{ route('retailers_details',$dt->id) }}" class="btn btn-icon btn-success btn-sm mr-2"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <a href="{{ route('retailers_view',$dt->id) }}" class="btn btn-icon btn-info btn-sm mr-2"><i class="fa fa-eye" aria-hidden="true"></i></a>
							          <!-- <a onclick="return confirm('Are you sure?')" href="{{ route('retailers_delete',$dt->id) }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>

@endsection
