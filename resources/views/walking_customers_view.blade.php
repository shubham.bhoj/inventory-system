@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Retailer Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Company Name: </label><label class="col-md-10 col-form-label">{{$data->company_name}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Contact Person: </label><label class="col-md-10 col-form-label">{{$data->contact_person}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Address: </label><label class="col-md-10 col-form-label">{{$data->address}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Civil/CR. No.: </label><label class="col-md-10 col-form-label">{{$data->civil_no}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Mobile No.: </label><label class="col-md-10 col-form-label">+{{$data->mobile_code}} {{$data->mobile}}</label></div>
  <div class="form-group row"><label class="col-md-2 col-form-label font-weight-bold">Email ID: </label><label class="col-md-10 col-form-label">{{$data->email}}</label></div>
  <!-- <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a> -->
</div>

<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Retailer History</h4>
        </div>
    </div>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table data-table">
        <thead class="text-primary">
            <th style="width: 37.3281px;">Sr No.</th>
            <th>Total</th>
            <th>Cash</th>
            <th>Knet</th>
            <th>Credit</th>
            <th>Date</th>
            <th>Action</th>
        </thead>
        <tbody>
            @php $sub_total=0;$total=0;$amount=0;$cash=0;$knet=0;$due=0; @endphp
            @foreach($secondData as $key=>$dt)
            <tr>
            <td>{{$key+1}}</td>
            <td>{{round($dt->total,3)}}</td>
            <td>{{round($dt->cash,3)}}</td>
            <td>{{round($dt->knet,3)}}</td>
            <td>{{round($dt->due,3)}}</td>
            <td>{{$dt->created_at}}</td>
            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$dt->id}}">Credit</button>
            <a href="{{ route('invoice_sales_download',$dt->id) }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-download" aria-hidden="true"></i></a></td>
            </tr>
            @php $sub_total+=$dt->sub_total;$total+=$dt->total;$amount+=$dt->amount;$cash+=$dt->cash;$knet+=$dt->knet;$due+=$dt->due; @endphp
            @endforeach
            <tr style="background: #f96332;color:white;">
                <td>{{'TOTAL'}}</td>
                <td>{{$total}}</td>
                <td>{{$cash}}</td>
                <td>{{$knet}}</td>
                <td>{{$due}}</td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        </table>
    </div>
</div>
  <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
</div>
@foreach($secondData as $dtx)
<div class="modal fade" style="z-index:1500;" id="exampleModal{{$dtx->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pay Dues</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('dues_update') }}">
        @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Cash</label>
            <input type="number" class="form-control" name="cash">
            <input type="number" hidden class="form-control" name="id" value="{{$dtx->id}}">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Knet</label>
            <input type="number" class="form-control" name="knet">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>

@endforeach

@endsection