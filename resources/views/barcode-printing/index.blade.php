@extends('layouts.app')
@section('content')
<style>
    /*.barcodelist {
        max-width: 378px;
        text-align: center;
    }
    .barcodelist img {
        max-width: 150px;
    }*/

    @media print {
        * {
            font-size:12px;
            line-height: 20px;
        }
        td,th {padding: 5px 0;}
        .hidden-print {
            display: none !important;
        }
        @page { size: landscape; margin: 0 !important; }
        .barcodelist {
            max-width: 378px;
        }
        .barcodelist img {
            max-width: 150px;
        } 
    }
    @page {
        size: A4;
        margin: 11mm 17mm 17mm 17mm;
        }

        @media print {
        footer {
            position: fixed;
            bottom: 0;
        }
        

    }
  /*   @media all {
    .page-break { display: block; }
    } */

    @media print {
        tr.page-break { display: block; page-break-before: always; }
        .modal-body{
            padding:none;
        }
        a[href]:after {
        content: none !important;
        }
        @page {
           margin-top: 0;
           margin-bottom: 0;
         }
         @page :footer {
        display: none
    }
  
    @page :header {
        display: none
    }
    #Header, #Footer { display: none !important; }
    }
ul#product{
    list-style:none;
    width:200px;
    max-height:200px;
    overflow-y:auto;
    background:#fff;
}
ul#product li{
    padding:7px;
}
</style>
<div class="card-header">
    <h4 class="card-title float-left">Barcode Printing</h4>
</div>
<br><br>

    <div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <label>Add Product*</label>
            <div class="search-box input-group">
                <input type="text" name="product_code_name" id="lims_productcodeSearch" placeholder="Please type product code and select..." class="form-control" />
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="table-responsive mt-3">
                <table id="myTable" class="table table-hover order-list">
                    <thead>
                        <tr>
                            <th>Design No.</th>
                            <th>Gold Wt.</th>
                            <th>Dia Wt.</th>
                            <th>Clarity</th>
                            <th>Color Grade</th>
                            <th><i class="dripicons-trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="form-group mt-2">
        <strong>Print: </strong>&nbsp;
        <strong><input type="checkbox" name="name" checked /> Product Name</strong>&nbsp;
        <strong><input type="checkbox" name="price" checked/> Price </strong>&nbsp;

        <strong>&nbsp;&nbsp;Show on :</strong>&nbsp;
        <strong><input type="radio" name="showOn" value="Left" checked /> Left</strong>&nbsp;
        <strong><input type="radio" name="showOn" value="Right"/> Right</strong>&nbsp;

        <!-- <strong><input type="checkbox" name="promo_price"/> {{trans('Promotional Price')}}</strong> -->
    </div>
    <div class="row">
        <div class="col-md-4">
            <label><strong>Paper Size *</strong></label>
            <select class="form-control" name="paper_size" required id="paper-size">
                <option value="0">Select paper size...</option>
                @foreach($sizes_list as $size)
                <option value="{{$size->width}}_{{$size->height}}_{{$size->item_gap}}_{{$size->pages_gap}}_{{$size->no_of_items_in_page}}_{{$size->margin_top}}_{{$size->margin_bottom}}_{{$size->margin_left}}_{{$size->margin_right}}">{{$size->width}} mm ({{round(($size->width * 0.0393701),2)}} inch)</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group mt-3">
        <input type="submit" value="submit" class="btn btn-primary" id="submit-button">
    </div>
    </div>

    </div>
    </div>
        
    </div>
</div>

<div id="print-barcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h5 id="modal_header" class="modal-title">{{trans('Barcode')}}</h5>&nbsp;&nbsp;
                  <button id="print-btn" type="button" class="btn btn-default btn-sm"><i class="dripicons-print"></i> {{trans('Print')}}</button>
                  <button type="button" id="close-btn" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i class="dripicons-cross"></i></span></button>
                </div>
                <div class="modal-body">
                    <div id="label-content">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

