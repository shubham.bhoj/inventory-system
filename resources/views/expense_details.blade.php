@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left">Bank Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('expense_upsert') }}">
	@csrf
	<input type="hidden" value="{{isset($data->id) ? $data->id : ''}}" name="id">
    <div class="row">
        <div class="col-sm-4">
			<div class="form-group">
				<label>Amount*</label>
				<input type="number" value="{{isset($data->amount) ? $data->amount : old('amount')}}" name="amount" class="form-control" placeholder="Amount" required>
				@error('amount')
				<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
        <div class="col-sm-6">
					<div class="form-group">
						<label>Description*</label>
						<textarea name="note" class="form-control" placeholder="Description" reqiured>{{isset($data->note) ? $data->note : old('note')}}</textarea>
						@error('note')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
        <div class="col-sm-2">
			<div class="form-group">
			<label>Select Account*</label>
				<select name="account" class="form-control">
		        <option value="1">Bank</option>
		        <option value="2">Cash</option>				
			    </select>
				<span class="form-text text-muted"></span>
			</div>
        </div>
    </div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
@endsection