@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Sales Return List</h4>
                <a href="{{ route('new-sales-return') }}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add Return
					</a>
</div>
</div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
             
                  <table class="table data-table">
                    <thead class="text-primary">
            
                    <th>Date</th>
                    <th>Invoice No.</th>
                    <th>Customer Name</th>
                    <th>Amount</th> 
                    <th>Action</th>           
                    </thead>
                    <tbody>
                    @php $i =1; @endphp
                    @foreach($returns as $return)
                    <tr>
                    <td>{{date('d/M/Y', strtotime($return->created_at))}}</td>
                    <td>{{ $return->invoice }}</td>
                    <td>{{ $return->retailer->company_name }}</td>
                    <td>{{ $return->return_amt }}</td>
                    <td>
                        <div class="dropdown">
                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action</button>
                          <ul class="dropdown-menu">
                             <li><a class="btn btn-link invoiceModel" data-toggle="modal" data-target="#invoiceModel-{{ $return->id}}" data-order_no="{{ $return->invoice }}"><i class="dripicons-trash"></i>Invoice</a></li>
                            <li><a class="btn btn-link viewDetails" href="{{ route('return-view',$return->invoice) }}"><i class="dripicons-document-edit" ></i>View</a></li>
                            <li><a class="btn btn-link" href="{{ route('return-edit',$return->invoice) }}"><i class="dripicons-document-edit"></i>Edit</a></li>
                            <li><a class="btn btn-link" data-toggle="modal" data-target="#deleteReturnModal-{{ $return->id}}"><i class="dripicons-trash"></i>Delete</a></li>
                          </ul>
                        </div>  

                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
             
              </div>

<!-- ====================== Invoice Model ============================================ -->
@foreach($returns as $return)
              <div style="padding-right: 222px !important;" class="modal fade" id="invoiceModel-{{ $return->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel" >
    <div class="modal-dialog" style="margin-top: -51px; margin-right: 560px">
        
      <div class="modal-content" style="width: 900px; max-width: 900px;" id="modal_content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
          <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none;margin-left: 300px;
height: 97%;" id="preloader_fif" class="preloader_fif"></div>
         <div class="container mt-3 pb-2 border-bottom">
            <!-- <input type="button" value="Print" onClick="printDiv('modal_content')" id="print-btn"> -->
            <a class="btn btn-primary" href="{{route('print-return-invoice',$return->invoice)}}" target="_blank">Print</a>
            <div class="row">
                <div class="col-md-12 text-center head-sl">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>

                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Sale Return Invoice</i>
                </div>
            </div>
            </div>

            <div id="purchase-content" class="modal-body purchase-content"></div>
        
        <div class="modal-body modal_invoice" id="modal_invoice">
          <table class="table table-bordered product-purchase-list">
                <thead>
                    <tr>
                    <th style="border:0.5px solid #fff;">Sr No.</th>
                    <th style="border:0.5px solid #fff;">GW</th>
                    <th style="border:0.5px solid #fff;">DW</th>
                    <th style="border:0.5px solid #fff;">Gold&nbsp;+&nbsp;Lab</th>
                    <th style="border:0.5px solid #fff;">Dia&nbsp;Value</th>
                    <th style="border:0.5px solid #fff;">Sub&nbsp;Total</th>
                    <th style="border:0.5px solid #fff;">Custom</th>
                    <th style="border:0.5px solid #fff;">$&nbsp;Total</th>
                    <th style="border:0.5px solid #fff;">Amount</th>
                    <th style="border:0.5px solid #fff;">Stamping</th>
                    <th style="border:0.5px solid #fff;">Total Amount</th>
                </tr>
              </thead>
                
            <tbody class="showDetail">  
            
           </tbody>
          </table>

        </div>
      </div>
      
    </div>
  </div>
@endforeach


<!--------------------------- Delete Model ----------------------------------->

@foreach($returns as $return)
<div class="modal fade" id="deleteReturnModal-{{ $return->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
        <div class="modal-header">
          <h4 class="modal-title">Are you sure to delete {{ $return->invoiceno }}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="viewModal-{{ $return->id }}" method="post" action="{{ route('return-delete',$return->id)}}">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
@section('script')
<script>

$('.invoiceModel').click(function(e){
 
  let order_no = $(this).attr('data-order_no');
  let total_amount = 0;
  $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          url:"return-invoice",
          type:"POST",
          data:{invioceNo:order_no},
          cache:'false',
          beforeSend: function() {
              $("#preloader_fif").show();
           },
          success:function(response)
          {
            $(".purchase-content").html(response.head)
            $(".showDetail").html(response.body)
            $("#preloader_fif").hide();
          },
          error:function(e)
          {  
            $("#preloader_fif").hide();
            console.log(e);
          }
        });
});
</script>
@endsection
@endsection

