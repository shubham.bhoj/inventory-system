@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Sales Return Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
   <!--  method="POST" action="{{ route('sales-return-add') }}" -->
    <form id="return-form">
	@csrf
   <div class="row">
	 <div class="col-sm-4">
			<div class="form-group">
			<label>Retailer*</label>
				<select name="retailer_id" id="retailer_id" class="form-control retalier_id" required="">
		        <option value="">Select Retailer</option>
				@foreach($retailers as $retailer)
		        <option value="{{$retailer->id}}">{{$retailer->company_name}} (+{{$retailer->mobile_code}} {{$retailer->mobile}})</option>
		        @endforeach
			    </select>
				@error('retailer_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
<div class="row">
<div class="col-sm-4">
			<div class="form-group">
			<label>Invoice No*</label>
				<select name="invoice_no" id="invoice_no" class="form-control" >
		        <option value=" ">Select Invoice</option>
			    </select>
				@error('stock_detail_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
</div>
<div class="tab-pane fade show active"  style="display:block" id="tabletab" role="tabpanel" aria-labelledby="table-tab">

		<div class="table-responsive">
		<table class="table data-table no-footer" id="saledetails">
		<thead class="text-primary">
			<tr>
				<th></th>
				<th style="font-size: 12px;">Code</th>
				<th style="font-size: 12px;">Design</th>
				<th style="font-size: 12px;">Image</th>
				<th style="font-size: 12px;">GW</th>
				<th style="font-size: 12px;">DW</th>
				<th style="font-size: 12px;">Gold+Lab</th>
				<th style="font-size: 12px;">Dia&nbsp;P/Ct</th>
				<th style="font-size: 12px;">Sub&nbsp;Total</th>
				<th style="font-size: 12px;">Cust&nbsp;Value</th>
				<th style="font-size: 12px;">Total&nbsp;Value<br>($)</th>
				<th style="font-size: 12px;">Total&nbsp;Value<br>KWD</th>
				<th style="font-size: 12px;">Stamp&nbsp;KWD</th>
				<th style="font-size: 12px;">Total&nbsp;Amount</th>
				<th style="font-size: 12px;">Item</th>
				<th style="font-size: 12px;">Clarity</th>
				<th style="font-size: 12px;">Grade</th>
				<th style="font-size: 12px;">St&nbsp;Pcs</th>
				<th style="font-size: 12px;">St&nbsp;Wt</th>
			</tr>
			</thead>
			<tbody>
				<tr>
			<td colspan="3" style="text-align: right; font-weight:bold; border: 1px solid;">Total:</td>
			<td style="border: 1px solid;">0</td>
			<td style="border: 1px solid;">0</td>
			<td style="border: 1px solid;" colspan="5" >0</td>
		<td style="border: 1px solid;" colspan="3" >0</td>
		<td style="border: 1px solid;" colspan="4" >0</td>
		<td style="border: 1px solid;"></td>
		</tr>
		</tbody>
	</table>
    </div>
    <br>
	
    </div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold" id="submit_button">Next</button>
    </form>
</div>
 <!----------Return  Model-------------->
<div class="modal fade" id="returnModel" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader" class="preloader_for"></div>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Sales Return</i>
                </div>
            </div>
        </div>
        <div class="modal-body return_view" id="return_view">          
        <form method="POST" action="{{ route('sales-return-add') }}">
        	@csrf
           <table class="table data-table">
                    <thead class="text-primary">
                    <th style="font-weight: 600;font-size: 13px;">No.</th>
                    <th style="font-weight: 600;font-size: 13px;">Code</th>
                    <th style="font-weight: 600;font-size: 13px;">Design</th>
                    <th style="font-weight: 600;font-size: 13px;">Image</th>
                    <th style="font-weight: 600;font-size: 13px;">Total&nbsp;Amount</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
			    <tr>
			      <td colspan="3">
			      	<select name="account" class="form-control" required>
					<option value="">Select Account..</option>
					@php $accounts = \App\Models\Account::where('is_active','1')->get(); @endphp
					@foreach($accounts as $account)
					<option value="{{ $account->id}}">{{ $account->name}}</option>
					@endforeach
				  </select>
			      </td>
			      <td colspan="2"><input type="hidden" name="vat" id="vat" class="form-control" placeholder="Vat" readonly style="color: #000;"><input type="text" name="return_amount" id="return_amount" class="form-control" placeholder="Return Amount" readonly style="color: #000;"><input type="hidden" name="pay_amt" id="pay_amt" class="form-control" placeholder="Paid Amount" readonly style="color: #000;"></td>
			    </tr>
			     <tr>
                <td colspan="5"><button type="submit" class="btn btn-primary" >Return</button></td>
                </tr>
			  </tfoot>
        	</table>
        </form>
        </div>
      </div>
      
    </div>
  </div>


<script>
	


function getAmount()
{
	let tot_amount = parseFloat($('#total_amount_val').text()).toFixed(2);
	$('#return_amount').val( Math.round(tot_amount));
}
</script>
@endsection

@section('script')
<script>
	
$("#return-form").on('submit', function(e) {

			e.preventDefault();
            var allVals = []; 
            let retailer = $('#retailer_id').val(); 
            let invoice = $('#invoice_no').val(); 

            $(".sub_chk:checked").each(function() {  

                allVals.push($(this).attr('data-id'));

            });  


            if(allVals.length <=0)  

            {  
                alert("Please select atleast one Product.");  
                return false;

            }  else {    

                var join_selected_values = allVals.join(","); 

                    $.ajax({

                        url: 'return-detalis',

                        type: 'POST',

                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

                        data: {ids: join_selected_values,invoice: invoice,retailer: retailer},

                        beforeSend: function() {
                         $('#submit_button').text('Please wait..');
                       },

                        success: function (resopnse) {
                            $("#loder").hide();
                            $(".return_view table tbody").html(resopnse.data)
                            $("#return_amount").val(resopnse.total)
                            $("#vat").val(resopnse.vat)
                            $("#pay_amt").val(resopnse.amount)
                            $('#returnModel').modal('show');
                            $('#submit_button').text('Next');
                        },

                        error: function (error) {
                            $("#loder").hide();
                             $('#submit_button').text('Next');
                            console.log(error)

                        }

                    });                 

            }  

        });

$("#return_view").on('submit', function(e) {

let paid_amt = $("#pay_amt").val();
let return_amt = $("#return_amount").val();
if(return_amt > paid_amt)
{
	alert('Opps, Your Amount is Grater then Paid Amount.');
	return false;
}
});			
</script>
<script>

	$("#invoice_no").select2();


	$(".retalier_id").change( function(e){
	 let id = $(this).val();
	 let opt = '';
	 $.ajax({
		headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      type: "POST",
      url: "get-invioce",
      data: {id: id},
      beforeSend: function(){
      	$("#invoice_no").prop('disabled', true);
      },
      success: function (response) {
      	$("#invoice_no").prop('disabled', false);
      	if(response != null){
      		opt += '<option value=" ">Select Invoice</option>';
      	response.forEach( i => {
      		opt += '<option value="'+i.invoiceno+'">'+i.invoiceno+'</option>';
      	})
      }else{
      	opt += '<option value="">No Item Found</option>';
      }
      	$('#invoice_no').html(opt);
     },
      error: function(e) {
      	$("#invoice_no").prop('disabled', false);
      	console.log(e)
      }

})
})


	$("#invoice_no").change( function(e){
	 var retailer = $('#retailer_id').val();
	 let invoice = $(this).val();
	 let tab = '';
	 $.ajax({
		headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      type: "POST",
      url: "get-product",
      data: {invoice: invoice, retailer: retailer},
      beforeSend: function(){
      	$("#stocks_list").prop('disabled', true);
      },
      success: function (response) {
      	$("#stocks_list").prop('disabled', false);
      	if(response != null){
      		$('#saledetails tr:last').remove();
      	    response.forEach( i => {
      		tab += '<tr>'
      	+'<td><input type="checkbox" class="sub_chk" value="'+i.saleId+'" name="sale_id[]" data-id="'+i.saleId+'"></td>'
		+'<td>'+i.code+'</td>'
		+'<td>'+i.stockDesign+'</td>'
		+'<td><img src="'+'..\\'+i.image+'"'+'width="120" height="70" style="max-width: none;" alt="No Image"></td>'
		+'<td><input id="tot_gold_'+i+'" type="text" name="tot_gold" class="form-control tot_gold" value="'+i.gw+'" style="color:#000;width: 60px;" readonly ></td>'
		+'<td><input id="tot_dia" type="text" data_id="'+i+'" name="tot_gold" class="form-control tot_dia" value="'+i.dw+'" readonly style="color:#000;width: 60px;" ></td>'
		+'<td><input type="text" name="gold_lab[]" id="gold_lab_'+i+'" class="form-control gold_lab" value="'+i.gold_lab+'" readonly style="color:#000;" ></td>'
		+'<td><input type="text" name="dia_pct[]" id="dia_pct_'+i+'" class="form-control dia_pct" value="'+i.dia_pct+'" readonly style="color:#000;width: 60px;"></td>'
		+'<td><input type="text" name="sub_total[]" id="sub_total_'+i+'" class="form-control sub_total" value="'+i.sub_total+'" readonly style="color:#000;width: 70px;"></td>'
		+'<td><input type="text" name="cus_value[]" id="cus_value_'+i+'" class="form-control cus_value" value="'+i.customer_val+'"  readonly style="color:#000;width: 60px;" ></td>'
		+'<td><input type="text" name="tot_val[]" id="tot_val_'+i+'" class="form-control tot_val" value="'+i.tot_val+'" readonly style="color:#000;width: 70px;"></td>'
		+'<td><input type="text" name="total_kwd_val[]" id="total_kwd_val_'+i+'" class="form-control total_kwd_val" value="'+i.tot_val_kwd+'" readonly style="color:#000;width: 70px;"></td>'
		+'<td><input type="text" name="stamping[]" id="stamping" data_id="'+i+'" class="form-control stamping" value="'+i.stm_kwd+'" readonly style="color:#000;"></td>'
		+'<td><input type="text" name="total_amount[]" id="total_amount_'+i+'" class="form-control total_amount" value="'+i.tot_amt+'" readonly style="color:#000;"></td>'
		+'<td>'+i.description+'</td>'
		+'<td>'+i.dq+'</td>'
		+'<td>'+i.cg+'</td>'
		+'<td>'+i.csp+'</td>'
		+'<td>'+i.csw+'</td>'
		+'</tr>';
      	})
      }
      	
      	$('#saledetails tbody').html(tab);
      },
 
      error: function(error) {
      	$("#stocks_list").prop('disabled', false);
      	console.log(error)
      }

})
})
	

</script>
@endsection