@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Sale Return Details</h4>
                <a href="{{ route('sale-return-print',$invoice)}}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5" target="_blank">
            Print
          </a>
</div></div>
              </div>
              <div class="card-body">
              <div class="table-responsive">
              <table border=1 class="table data-table">
                    <thead class="text-primary">
                    <th style="font-size: 12px;">No</th>
                    <th style="font-size: 12px;">Code</th>
                    <th style="font-size: 12px;">Design</th>
                    <th style="font-size: 12px;">Image</th>
                    <th style="font-size: 12px;">GW</th>
                    <th style="font-size: 12px;">DW</th>
                    <th style="font-size: 12px;">Gold+Lab</th>
                    <th style="font-size: 12px;">Dia&nbsp;P/Ct</th>
                    <th style="font-size: 12px;">Sub&nbsp;Total</th>
                    <th style="font-size: 12px;">Custom</th>
                    <th style="font-size: 12px;">Total&nbsp;Value<br>($)</th>
                    <th style="font-size: 12px;">Total&nbsp;Value<br>KWD</th>
                    <th style="font-size: 12px;">Stamp&nbsp;KWD</th>
                    <th style="font-size: 12px;">Total&nbsp;Amount</th>
                    <th style="font-size: 12px;">Item</th>
                    <th style="font-size: 12px;">Clarity</th>
                    <th style="font-size: 12px;">Grade</th>
                    <th style="font-size: 12px;">St&nbsp;Pcs</th>
                    <th style="font-size: 12px;">St&nbsp;Wt</th>
                    </thead>
                    <tbody>
                      @php $key=1; @endphp
                      @foreach($returns as $dt)
                      <tr>
                        <td>{{ $key++}}</td>
                        <td>{{ $dt->code}}</td>
                        <td>{{ $dt->design}}</td>
                        <td><img src="..\{{$dt->image}}" id="myImg{{$dt->id}}" onclick="getImageId('myImg{{$dt->id}}')" width="120" height="70" style="max-width: none;" alt="No Image"></td>
                        <td>{{$dt->gw}}</td>
                        <td>{{$dt->dw}}</td>
                        <td>${{$dt->gold_lab}}</td>
                        <td>${{$dt->dia_pct}}</td>
                        <td>${{$dt->sub_total}}</td>
                        <td>${{$dt->customer_val}}</td>
                        <td>${{$dt->tot_val}}</td>
                        <td>{{$dt->tot_val_kwd}} KD</td>
                        <td>{{$dt->stm_kwd}} KD</td>
                        <td>{{$dt->tot_amt}}</td>
                        <td>{{$dt->description}}</td>
                        <td>{{$dt->dq}}</td>
                        <td>{{$dt->cg}}</td>
                        <td>{{$dt->csp}}</td>
                        <td>{{$dt->csw}}</td>
                      </tr>
                      @endforeach
                      <tr>
                      <td colspan="4" style="text-align: center; font-weight:bold; border: 1px solid;">Total:</td>
                      <td style="border: 1px solid;">{{ $gold_wt}}</td>
                      <td style="border: 1px solid;">{{ $dia_wt}}</td>
                      <td style="border: 1px solid;">{{ $gold_lab}}</td>
                      <td style="border: 1px solid;">{{ $dv}}</td>
                      <td style="border: 1px solid;">{{ $sub_total}}</td>
                      <td style="border: 1px solid;">{{ $custom}}</td>
                      <td style="border: 1px solid;">{{ $total}}</td>
                      <td style="border: 1px solid;">{{ $total_kwd}} KD</td>
                      <td style="border: 1px solid;">{{ $total_stm}} KD</td>
                      <td style="border: 1px solid;">{{ $tot_amt}}</td>
                      <td style="border: 1px solid;" colspan="5"></td>
                      
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="myModal" class="modal">
<span class="close">&times;</span>
<img class="modal-content" id="img01">
</div>
<script>
function getImageId(imageId){
var modal = document.getElementById("myModal");
var img = document.getElementById(imageId);
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  $(".displaynav").css("display",'none');
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  modal.style.display = "none";
  $(".displaynav").css("display",'block');
}}
</script>
@endsection