@extends('layouts.app')
@section('content')
<div class="card-header">
                <h4 class="card-title float-left"> Money Transfer</h4>
                <a href="#" data-toggle="modal" data-target="#moneyTransModal" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">Add Money Transfer </a></div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table">
                    <thead class="text-primary">
                      <th>
                      Sr No.
                      </th>
                      <th>
                        Date
                      </th>
                      <th>
                       Reference No
                      </th>
                      <th>
                       From Account 
                      </th>
                      <th>
                       To Account 
                      </th>
                      <th>
                       Amount 
                      </th>
                      <th>
                        Action
                      </th>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                     @foreach($transfers as $transfer)
                      <tr>
                        <td>
                         {{ $i++ }}
                        </td>
                        <td>
                         {{ date("d/M/Y", strtotime($transfer->created_at)) }}
                        </td>
                        <td>
                          {{ $transfer->reference_no }}
                        </td>
                        <td>
                          {{ $transfer->from_Account->name }}
                        </td>
                        <td>
                          {{ $transfer->to_Account->name }}
                        </td>
                        <td>
                          {{ $transfer->amount }}
                        </td>
                        <td>
                        <a href="#" id="modal" data-toggle="modal" data-target="#moneyEditModal" class="btn btn-icon btn-success btn-sm mr-2"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a href="#" data-toggle="modal" data-target="#deleteMoneyModal-{{ $transfer->id }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>

                  </table>
                </div>
              </div>


<!---------- Updatet Account Model-------------->
<div class="modal fade" id="moneyTransModal">
      <div class="modal-content">
         <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Add Money transfer</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
            </div>
        <div class="modal-body">
          <div class="modal-dialog">
        
          </div>
               <form method="post" action="{{ route('money_add')}}">
                      @csrf
                      <div class="row">
                        <div class="col-md-6">
                            <label>From Account  *</label>
                            <select name="form_account" class="form-control numkey" required>
                                <option value="">Select from account....</option>
                                @foreach($accounts as $account)
                                <option value="{{$account->id}}">{{$account->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>To Account *</label>
                            <select name="to_account" class="form-control numkey" required>
                                <option value="">Select to account....</option>
                                @foreach($accounts as $account)
                                <option value="{{$account->id}}">{{$account->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-8">
                         <div class="form-group">
                        <label>Amount*</label>
                        <input type="number" name="balance" step="any" class="form-control numkey" required onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45">
                    </div>
                </div>
            </div>
                  <button type="submit" class="btn btn-primary">Transfer</button>
          </form>
        </div>
        </div>
      </div>
      
    </div>
  </div>


      <!---------- Delete account Model-------------->
 @foreach($transfers as $transfer)
<div class="modal fade" id="deleteMoneyModal-{{ $transfer->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
        <div class="modal-header">
          <h4 class="modal-title">Are you sure to delete .</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form method="post" action="{{ route('delete_transfer',$transfer->id)}}">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

@endsection