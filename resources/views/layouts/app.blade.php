<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Diamond CRM
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="{{ asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{ asset('public/assets/css/now-ui-dashboard.css?v=1.5.0')}}" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://intact-pos.saffutions.com/public/vendor/bootstrap/css/bootstrap-datepicker.min.css" type="text/css">
    <link rel="stylesheet" href="https://intact-pos.saffutions.com/public/vendor/jquery-timepicker/jquery.timepicker.min.css" type="text/css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.css">
<link rel="stylesheet" href="https://intact-pos.saffutions.com/public/vendor/daterange/css/daterangepicker.min.css" type="text/css">
<link rel="stylesheet" href="https://intact-pos.saffutions.com/public/vendor/bootstrap/css/bootstrap-select.min.css" type="text/css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<style>
  td {
    border-right: ridge 1px;
    /* border-left: ridge 1px; */
    /* border-left: solid 1px; */
  }
  thead tr{
    padding-left:7px;
    font-size: 10px;
    background-color: #2a5686;
    /* white-space:nowrap; */
    color:white;
  }
  thead tr th{
    padding-left:7px;    
  }
  tbody tr{
    font-size: 12px;
  }
  table{
    border:1px solid
  }

  .numkey{
    height: 40px;
  }
  
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
  animation-name: zoom;
  animation-duration: 0.6s;
}


@keyframes zoom {
  from {transform:scale(0)}
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 35px;
  right: 40px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.modal-backdrop.show {
    display: none;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

#preloader {
    background-color: white;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999;
}
#preloader_sec{
   background-color: white;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999;
}
#preloader_thr{
   background-color: white;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999;
}

#preloader_fif{
  background-color: white;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

img.image_set:hover{
  cursor: pointer;
}

.daterangepicker{
  z-index: 999999;
}

.controls .periods{
  display: none;
}

.controls .ranges{
  display: none;
}

</style>
<body style="font-family:cambria !important" class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          DC
        </a>
        <a href="#" class="simple-text logo-normal">
          Diamond CRM
        </a>
      </div>

      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">

          <li class="@if(url()->current()==route('index')) active @endif">
            <a href="{{ route('index') }}">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <!-- <li class="@if(url()->current()==route('sales_list')) active @endif">
            <a href="{{ route('sales_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Sales</p>
            </a>
          </li> -->


          <li onclick="hidefunctionSales()"><a href="#"><i class="now-ui-icons shopping_bag-16"></i><p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Sales</p></a></li>
           <li id="add_sales" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('sales_details')) active @endif">
            <a href="{{ route('sales_details') }}">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Add Sales</p>
            </a>
          </li>
          <li id="sales_list" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('sales_list')) active @endif">
            <a href="{{ route('sales_list') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;font-weight: 600;font-family: cambria !important;">Sales List</p>
            </a>
          </li>


          <!-- ------------------------------------------- stock ------------------------------ -->

        <li onclick="hidefunctionStock()"><a href="#"><i class="now-ui-icons shopping_bag-16"></i><p style="text-transform: capitalize;font-size: 14px;">Stock</p></a></li>
          <li id="stock_list" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('stocks_list')) active @endif">
            <a href="{{ route('stocks_list') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Stock List</p>
            </a>
          </li>
          <li id="stock_history" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('stocks_history')) active @endif">
            <a href="{{ route('stocks_history') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Stock History</p>
            </a>
          </li>



        <!-- ------------------------------------------- Return ------------------------------ -->

        <li onclick="hidefunctionReturn()"><a href="#"><i class="now-ui-icons shopping_delivery-fast"></i><p style="text-transform: capitalize;font-size: 14px;">Return</p></a></li>
          <li id="salereturn_list" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('sale-return')) active @endif">
            <a href="{{ route('sale-return') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Sale</p>
            </a>
          </li>
         <!--  <li id="stock_history" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('stocks_history')) active @endif">
            <a href="{{ route('stocks_history') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Stock History</p>
            </a>
          </li> -->

          <!-- ------------------------------- accounts -------------------------------- -->


<li onclick="hidefunctionAccount()"><a href="#"><i class="now-ui-icons business_bank"></i><p style="text-transform: capitalize;font-size: 14px;">Accounting</p></a></li>
          <li id="myaccount" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('account_add')) active @endif">
            <a href="{{ route('account_add') }}">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Add Account</p>
            </a>
          </li>
          <li id="myaccounts" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('accounts_list')) active @endif">
            <a href="{{ route('accounts_list') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Accounts List</p>
            </a>
          </li>
           <li id="myaccountss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('money_transfer')) active @endif">
            <a href="{{ route('money_transfer') }}">
              <i class="now-ui-icons business_money-coins"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Money Transfer</p>
            </a>
          </li>
          <li id="myaccountsss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('balance_sheet')) active @endif">
            <a href="{{ route('balance_sheet') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Balance Sheet</p>
            </a>
          </li>

          <!-- ---------------------------------- Account Statement --------------------------  -->


            <li id="myaccountssss" style="display:none;margin-left: 10%;">
            <a href="#" data-toggle="modal" data-target="#StatementModal">
              <i class="now-ui-icons business_badge"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Account Statement</p>
            </a>
          </li>

          <!----------------------------------------- Purchase ---------------------------->

          <li onclick="hidefunctionPurchase()"><a href="#"><i class="now-ui-icons shopping_box"></i><p style="text-transform: capitalize; font-size: 14px;">Purchase</p></a></li>

          <li id="purchase_new" style="display:none;margin-left: 10%;"  class="@if(url()->current()==route('purchase_new')) active @endif">
            <a href="{{ route('purchase_new') }}">
              <i class="now-ui-icons ui-1_simple-add"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Add Purchase </p>
            </a>
          </li>      
          
          <li id="purchase_new_sec" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('purchase_list')) active @endif">
            <a href="{{ route('purchase_list') }}">
              <i class="now-ui-icons files_single-copy-04"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Purchase List </p>
            </a>
          </li>

         
  
          <!-- <li onclick="hidefunctionSettings_purchase()"><a href="#"><i class="now-ui-icons design_app"></i><p>New Purchase</p></a></li>
          <li id="purchase_new" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('purchase_list')) active @endif">
            <a href="{{ route('purchase_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;">Purchase List</p>
            </a>
          </li>
          <li id="purchase_new" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('purchase_new')) active @endif">
            <a href="{{ route('purchase_new') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;">Add Purchase</p>
            </a>
          </li> -->

          <!-- <li class="@if(url()->current()==route('purchase_new')) active @endif">
            <a href="{{ route('purchase_new') }}">
              <i class="now-ui-icons design_app"></i>
              <p></p>
            </a>
          </li> -->

          <!-- <li class="@if(url()->current()==route('walking_customers')) active @endif">
            <a href="{{ route('walking_customers') }}">
              <i class="now-ui-icons sport_user-run"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Walking Customers</p>
            </a>
          </li> -->
          


           
          <!-- <li class="@if(url()->current()==route('sales_list')) active @endif">
            <a href="{{ route('sales_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p>Counter Sales</p>
            </a>
          </li>
          <li class="@if(url()->current()==route('sales_list')) active @endif">
            <a href="{{ route('sales_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p>Sales Return</p>
            </a>
          </li> -->


<!-- ----------------------------------- reports ----------------------------------------- -->
          
          <li onclick="hidefunction()"><a href="#"><i class="now-ui-icons media-2_sound-wave"></i><p style="text-transform: capitalize;font-size: 14px;">Reports</p></a></li>
         <!--  <li id="myreports" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('creditor_list')) active @endif">
            <a href="{{ route('history') }}">
              <i class="now-ui-icons education_paper"></i>
              <p style="text-transform: capitalize;font-size: 14px;">History</p>
            </a>
          </li> -->
          <!-- <li id="myreportss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('banks_list')) active @endif">
            <a href="{{ route('banks_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Balance</p>
            </a>
          </li> -->
         <!--  <li id="myreportsss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('creditor_list')) active @endif">
            <a href="{{ route('creditor_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Creditors</p>
            </a>
          </li> -->
          <li id="myreportssss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('expense_list')) active @endif">
            <a href="{{ route('expense_list') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">General Expense</p>
            </a>
          </li>

<!-- ------------------------------------ Barcode---------------------------------->

          <li onclick="hidefunctionBarcode()"><a href="#"><i class="now-ui-icons design_bullet-list-67"></i><p style="text-transform: capitalize;font-size: 14px;">Barcode</p></a></li>
          <li id="barcode" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('barcode')) active @endif">
            <a href="{{ route('barcode') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Barcode Setup</p>
            </a>
          </li>
          <!-- <li id="barcodes" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('barcode-printing')) active @endif">
            <a href="{{ route('barcode-printing') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Barcode-Printing</p>
            </a>
          </li> -->

          

          
<!-- -------------------------------------- settings ------------------------------ -->          

          <li onclick="hidefunctionSettings()"><a href="#"><i class="now-ui-icons loader_gear"></i><p style="text-transform: capitalize;font-size: 14px;">Settings</p></a></li>
          <li id="mysetting" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('company_details')) active @endif">
            <a href="{{ route('company_details') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Company Profile</p>
            </a>
          </li>
          <li id="mysettings" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('settings')) active @endif">
            <a href="{{ route('settings') }}">
              <i class="now-ui-icons design_app"></i>
              <p style="text-transform: capitalize;font-size: 14px;">General Settings</p>
            </a>
          </li>

          <li id="mysettingss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('retailers_list')) active @endif">
            <a href="{{ route('retailers_list') }}">
              <i class="now-ui-icons shopping_shop"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Retailers</p>
            </a>
          </li>

          
           <li id="mysettingsss" style="display:none;margin-left: 10%;" class="@if(url()->current()==route('suppliers_list')) active @endif">
            <a href="{{ route('suppliers_list') }}">
              <i class="now-ui-icons shopping_delivery-fast"></i>
              <p style="text-transform: capitalize;font-size: 14px;">Suppliers</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo"></a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav displaynav" style="display:block">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="now-ui-icons users_single-02"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="{{ route('profile') }}">Profile</a>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="panel-header panel-header-sm">
      </div>
      <div class="content">
      <div class="row">
          <div class="col-md-12">
            <div class="card">
            @yield('content')
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class=" container-fluid ">
          <div class="copyright" id="copyright">
            &copy; <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>Designed by <a href="#">Versatile International</a>. Coded by <a href="#">Versatile International</a>.
          </div>
        </div>
      </footer>
    </div>
  </div>

<?php $accounts = \App\Models\Account::where('is_active','1')->get();  ?>

<!---------- Account Statement Model-------------->
<div class="modal fade" id="StatementModal">
      <div class="modal-content" style="width:800px;">
         <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Account Statement</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
            </div>
        <div class="modal-body">
          <div class="modal-dialog">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader_sec" class="preloader_sec"></div>
          </div>
               <form method="post" action="{{ route('account_statement')}}">
                      @csrf
                      <div class="row">
                        <input type="hidden" name="payment_id" id="deduct_amount" class="deduct_amount">
                        <div class="col-md-6">
                            <label>Account *</label>
                            <select name="statement" id="statement" class="form-control numkey">
                              @foreach($accounts as $account)
                              <option value="{{ $account->id }}">{{ $account->name}} [{{ $account->account_no}}]</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Type *</label>
                            <select name="type" id="type" class="form-control numkey">
                             <option value="all">All</option>
                             <option value="debit">Debit</option>
                             <option value="credit">Credit</option>
                            </select>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <label>Choose Your Date *</label>
                      <input type="text" class="daterangepicker-field form-control numkey" required name="date">
                      <input type="hidden" class="daterangepicker-field form-control numkey" name="start_date">
                      <input type="hidden" class="daterangepicker-field form-control numkey" name="end_date">
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
        </div>
      </div>
      
    </div>
  </div>



  <script src="{{ asset('public/assets/js/core/jquery.min.js')}}"></script>
  <script src="{{ asset('public/assets/js/core/popper.min.js')}}"></script>
  <script src="{{ asset('public/assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{ asset('public/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/bootstrap/js/bootstrap-select.min.js"></script>
     <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/jquery/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/jquery/jquery.timepicker.min.js"></script>
      <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/daterange/js/moment.min.js"></script>
    <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/daterange/js/knockout-3.4.2.js"></script>
  <script type="text/javascript" src="https://intact-pos.saffutions.com/public/vendor/daterange/js/daterangepicker.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.js"></script>
  <script src="{{ asset('public/assets/js/plugins/chartjs.min.js')}}"></script>
  <script src="{{ asset('public/assets/js/plugins/bootstrap-notify.js')}}"></script>
  <script src="{{ asset('public/assets/js/now-ui-dashboard.min.js?v=1.5.0')}}" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> 
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
  
  @yield('script')
  <script>
    function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}
  $(document).ready( function () {
      $('.data-table').DataTable();
  } );
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jautocalc@1.3.1/dist/jautocalc.js"></script>
	<script type="text/javascript">
		$(function() {
			function autoCalcSetup() {
				$('form#cart').jAutoCalc('destroy');
				$('form#cart tr.line_items').jAutoCalc({keyEventsFire: true, decimalPlaces: 2, emptyAsZero: true});
				$('form#cart').jAutoCalc({decimalPlaces: 2});
			}
			autoCalcSetup();
			$('button.row-remove').on("click", function(e) {
				e.preventDefault();
				var form = $(this).parents('form')
        if($('#stockdetails tr').length  == 3 ){
          $(this).parents('tr').remove();
        }else{
				  $(this).parents('tr').remove();
				  autoCalcSetup();
        }
        let qty = document.querySelectorAll("[id='qty']");
        let total = 0;
        qty.forEach(i => {
          total = total + Number(i.value)
        })
        $('#tot_qty').text(total);

        let grs_wt = document.querySelectorAll("[id='grs_wt']");
        let total_1 = 0;
        grs_wt.forEach(i => {
          total_1 = total_1 + Number(i.value)
        })
        $('#tot_grs_wt').text(total_1);

        let gldnet_wt = document.querySelectorAll("[id='gldnet_wt']");
        let total_2 = 0;
        gldnet_wt.forEach(i => {
          total_2 = total_2 + Number(i.value)
        })
        $('#tot_grs_wt').text(total_2);

        let diamond_wt = document.querySelectorAll("[id='diamond_wt']");
        let total_3 = 0;
        diamond_wt.forEach(i => {
          total_3 = total_3 + Number(i.value)
        })
        $('#tot_dmnd_wt').text(total_3);

        let stone_wt = document.querySelectorAll("[id='stone_wt']");
        let total_4 = 0;
        stone_wt.forEach(i => {
          total_4 = total_4 + Number(i.value)
        })
        $('#tot_stn_wt').text(total_4);

        let amount = document.querySelectorAll("[id='amount']");
        let total_5 = 0;
        amount.forEach(i => {
          total_5 = total_5 + Number(i.value)
        })
        $('#tot_amount').text(total_5);

        let kwd_amt = document.querySelectorAll("[id='kwd_amt']");
        let total_6 = 0;
        kwd_amt.forEach(i => {
          total_6 = total_6 + Number(i.value)
        })
        $('#tot_kwd_amt').text(total_6);
        total_cal();
        
			});

			$('button.row-add').on("click", function(e) {
				e.preventDefault();
				var $table = $(this).parents('table');
				var $top = $table.find('tr.line_items').first();
				var $new = $top.clone(true);
				$new.jAutoCalc('destroy');
				$new.insertBefore($top);
				$new.find('input[type=text]').val('');
				autoCalcSetup();
			});

      // $('button.row-add-row').on("click", function(e) {
      //   e.preventDefault();
      //   var $table = $(this).parents('table');
      //   var $top = $table.find('tr.line_items').first();
      //   var $new = $top.clone(true);
      //   $new.jAutoCalc('destroy');
      //   $new.insertBefore($top);
      //   $new.find('input[type=text]').val('');
      //   autoCalcSetup();
      //    let last_due = Math.round($('#myTable_2 tr:nth-child(3)').find("td:nth-child(5)").find("input[name='due[]']").val());
      //   $('#myTable_2 tr:nth-child(2)').find("td:first").find("input[name='amount']").val(last_due);
      //   $('#myTable_2 tr:nth-child(2)').find("td:nth-child(3)").find("input[name='cash[]']").val(0);
      //   $('#myTable_2 tr:nth-child(2)').find("td:nth-child(4)").find("input[name='knet[]']").val(0);
      //   $('#myTable_2 tr:nth-child(2)').find("td:nth-child(5)").find("input[name='due[]']").val(last_due);
      // });

		});


    $('.row-remove-stock').on("click", function(e) {
        e.preventDefault();
        var form = $(this).parents('form')
        if($('#stockdetails_sec tr').length  == 3 ){
          $(this).parents('tr').remove();
        }else{
          $(this).parents('tr').remove();
        }
      });

</script>
<script>
  function hidetab(key) {
    if(key==1){
  $("#tabletab").css("display",'block');
  $("#exceltab").css("display",'none');
} else if(key==2) {
  $("#tabletab").css("display",'none');
  $("#exceltab").css("display",'block');
}
  }
</script>
<script>
	function hideorshow(show)
	{
		if(show==2){
			$("#customerrow").css("display",'flex');
      $("#retailerrow").css("display",'none');
      $("#retailer_id").prop('required',false);
      // $('#retailer_id').removeAttr('required');
		} else {
			$("#retailerrow").css("display",'flex');
            $("#customerrow").css("display",'none');
             $("#retailer_id").prop('required',true);
		}
	}
</script>
<script>
$("#stocks_list").select2();

function hidefunction() {
  var z = document.getElementById("myreportssss");

  if (z.style.display === "none") {
    z.style.display = "block";
  } else {
    z.style.display = "none";
  }
}

function hidefunctionSettings() {
  var w = document.getElementById("mysetting");
  var x = document.getElementById("mysettings");
  var y = document.getElementById("mysettingss");
  var z = document.getElementById("mysettingsss");
  if (w.style.display === "none") {
    w.style.display = "block";
  } else {
    w.style.display = "none";
  }

  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display === "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }

  if (z.style.display === "none") {
    z.style.display = "block";
  } else {
    z.style.display = "none";
  }
}

function hidefunctionPurchase() {
  var w = document.getElementById("purchase_new");
  var x = document.getElementById("purchase_new_sec");
  if (w.style.display === "none") {
    w.style.display = "block";
  } else {
    w.style.display = "none";
  }

  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function hidefunctionAccount() {
  var w = document.getElementById("myaccount");
  var x = document.getElementById("myaccounts");
  var y = document.getElementById("myaccountss");
  var z = document.getElementById("myaccountsss");
  var z_1 = document.getElementById("myaccountssss");
  if (w.style.display == "none") {
    w.style.display = "block";
  } else {
    w.style.display = "none";
  }

  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display == "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }

  if (z.style.display == "none") {
    z.style.display = "block";
  } else {
    z.style.display = "none";
  }

  if (z_1.style.display == "none") {
    z_1.style.display = "block";
  } else {
    z_1.style.display = "none";
  }
}

function hidefunctionStock()
{
   var x = document.getElementById("stock_list");
  //var y = document.getElementById("stock_history");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  // if (y.style.display == "none") {
  //   y.style.display = "block";
  // } else {
  //   y.style.display = "none";
  // }

}

function hidefunctionSales()
{
   var x = document.getElementById("sales_list");
  var y = document.getElementById("add_sales");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display == "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }

}

function hidefunctionBarcode()
{

 var x = document.getElementById("barcode");
 var y = document.getElementById("barcodes");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  if (y.style.display == "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  } 
}

function hidefunctionReturn()
{
 var x = document.getElementById("salereturn_list");
 //var y = document.getElementById("barcodes");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  // if (y.style.display == "none") {
  //   y.style.display = "block";
  // } else {
  //   y.style.display = "none";
  // }
}
</script>
<script type="text/javascript">
      $(".daterangepicker-field").daterangepicker({
          callback: function(startDate, endDate, period){
            var start_date = startDate.format('YYYY-MM-DD');
            var end_date = endDate.format('YYYY-MM-DD');
            var title = start_date + ' To ' + end_date;
            $(this).val(title);
            $('#StatementModal input[name="start_date"]').val(start_date);
            $('#StatementModal input[name="end_date"]').val(end_date);
          }
      });

      $('.selectpicker').selectpicker({
          style: 'btn-link',
      });
</script>
</body>
</html>