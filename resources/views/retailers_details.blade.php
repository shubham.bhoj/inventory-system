@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Retailer Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('retailers_upsert') }}">
	@csrf
	<input type="hidden" value="{{isset($data->id) ? $data->id : ''}}" name="id">
    <div class="row">
        <div class="col-sm-4">
			<div class="form-group">
				<label>Company Name*</label>
				<input type="text" value="{{isset($data->company_name) ? $data->company_name : old('company_name')}}" name="company_name" class="form-control" placeholder="Company Name" required>
				@error('company_name')
				<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
        <div class="col-sm-4">
					<div class="form-group">
						<label>Contact Person*</label>
						<input type="text" value="{{isset($data->contact_person) ? $data->contact_person : old('contact_person')}}" name="contact_person" class="form-control" placeholder="Contact Person" required>
						@error('contact_person')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
        <div class="col-sm-4">
					<div class="form-group">
						<label>Address*</label>
						<input type="text" value="{{isset($data->address) ? $data->address : old('address')}}" name="address" class="form-control" placeholder="Address" required>
						@error('address')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
		<div class="col-sm-4">
					<div class="form-group">
						<label>Civil/CR. No.*</label>
						<input type="number" onkeypress="return isNumberKey(event)" value="{{isset($data->civil_no) ? $data->civil_no : old('civil_no')}}" name="civil_no" class="form-control" placeholder="Civil/CR. No." required>
						@error('civil_no')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
        <div class="col-sm-4">
			<div class="form-group">
			    <label class="mobile">Mobile No.*</label>
				<select name="mobile_code" id="" class="form-control mobile-code">
		        <option value="965" Selected>KW (+965)</option>
				@foreach($countries as $ct)
		        <option value="{{$ct->phonecode}}" {{isset($data->mobile_code) && $data->mobile_code==$ct->phonecode ? 'Selected' : ''}} >{{$ct->iso}} (+{{$ct->phonecode}})</option>
		        @endforeach
			    </select>
				<input type="number" onkeypress="return isNumberKey(event)" value="{{isset($data->mobile) ? $data->mobile : old('mobile')}}" name="mobile" class="form-control mobile-form-control" placeholder="Mobile No." required>
				@error('mobile')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
        <div class="col-sm-4">
					<div class="form-group">
						<label>Email ID</label>
						<input type="email" value="{{isset($data->email) ? $data->email : old('email')}}" name="email" class="form-control" placeholder="Email ID">
						@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
        </div>
    </div>
    <a type="button" href="{{url()->previous()}}" class="btn btn-secondary font-weight-bold text-white">Back</a>
    <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
    </form>
</div>
@endsection