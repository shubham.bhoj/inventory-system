@extends('layouts.app')
@section('content')
<div class="card-header">
  <div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left"> Purchase List</h4>
                <a href="{{ route('purchase_new')}}" class="btn btn-primary float-right font-weight-bolder btn-md text-right mr-5">
						Add Purchase
					</a>
        </div>
      </div>
    </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              
                  <table class="table data-table">
                    <thead class="text-primary">
                      <th>
                       Sr No.
                      </th>
                      <th>
                        Date
                      </th>
                      <th>
                        Order No.
                      </th>
                      <th>
                       Supplier
                      </th>
                      <th>
                       Grand Total
                      </th>
                      <th>
                      Paid
                      </th>
                      <th>
                      Due
                      </th>
                      <th>
                      Payment status
                      </th>
                       <th>
                      Action
                      </th>
                    </thead>
                    <tbody>
                      <?php $i = 1;?>
                      @foreach($purchases as $purchase)
                      <tr>
                        <td>{{ $i++}}</td>
                        <td>{{ date("d/M/Y", strtotime($purchase->created_at)) }}</td>
                        <td>{{ $purchase->order_no}}</td>
                        <td>{{ $purchase->supplier->company_name}}</td>
                        <td>{{ number_format((float)$purchase->grand_total, 2, '.', '')}}</td>
                        <td>{{ number_format((float)$purchase->paid_amount, 2, '.', '')}}</td>
                        <td>{{ number_format((float)$purchase->grand_total - $purchase->paid_amount, 2, '.', '')}}</td>
                        <td style="text-align: center;"><?= ($purchase->status == '1')?'<div class="btn btn-icon btn-success" style="line-height: 35px;">Paid</div>':'<div class="btn btn-icon btn-danger" style="line-height: 35px;">Due</div>'?></td>
                        <td>
                         <div class="dropdown">
                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action</button>
                          <ul class="dropdown-menu">
                             <li><a class="btn btn-link invoiceModel" data-toggle="modal" data-target="#invoiceModel-{{ $purchase->id }}" data-order_no="{{ $purchase->order_no}}"><i class="dripicons-document-edit" ></i>Generate Invoice</a></li>
                            <li><a class="btn btn-link viewDetails" data-toggle="modal" data-target="#viewModel-{{ $purchase->id }}" data-order_no="{{ $purchase->order_no}}"><i class="dripicons-document-edit" ></i>View</a></li>
                            <li><a class="btn btn-link" href="{{ route('purchase_edit',$purchase->id)}}"><i class="dripicons-document-edit"></i>Edit</a></li>
                            <li><a class="btn btn-link payment" data-order_no="{{ $purchase->order_no}}" data-toggle="modal" data-target="#paymentModal-{{ $purchase->id }}"><i class="dripicons-document-edit"></i>Add Payment</a></li>
                            <li><a class="btn btn-link view_Payment" data-toggle="modal" data-target="#viewPaymentModel-{{ $purchase->id }}" data-order_no="{{ $purchase->order_no}}"><i class="dripicons-document-edit"></i>View Payment</a></li>
                            <li><a class="btn btn-link" data-toggle="modal" data-target="#deletePurchaseModal-{{ $purchase->id }}"><i class="dripicons-trash"></i>Delete</a></li>
                          </ul>
                        </div> 
                    </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
              </div>


<!---------- View Invoice Model-------------->
@foreach($purchases as $purchase)
<div class="modal fade" id="invoiceModel-{{ $purchase->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog" style="margin-top: -51px; margin-right: 560px">
      <div class="modal-content" style="width: 900px; max-width: 900px;" id="modal_content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none;margin-left: 300px;
height: 97%;" id="preloader_fif" class="preloader_fif"></div>
         <div class="container mt-3 pb-2 border-bottom">
             <!-- <input type="button" value="Print" onClick="printDiv('modal_content')" id="print-btn"> -->
             <a class="btn btn-primary" href="{{route('downloadPurchaseInvoice',$purchase->id)}}" target="_blank">Print</a>
            <div class="row">
                <div class="col-md-12 text-center head-hd" >
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Purchase Invoice</i>
                </div>
            </div>
            </div>

            <div id="purchase-content" class="modal-body purchase-content"></div>
        
        <div class="modal-body modal_invoice" id="modal_invoice">
          <table class="table table-bordered product-purchase-list">
                <thead>
                    <tr>
                    <th>Sr.No</th>
                    <th>Particular</th>
                    <th>Qty</th>
                    <th>Gold Wt</th>
                    <th>G. Net Wt</th>
                    <th>DIA Wt</th>
                    <th>Stone Wt</th>
                </tr>
              </thead>
                
            <tbody class="showDetail">
             

              
            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

<!---------- Delete purchase Model-------------->
@foreach($purchases as $purchase)
<div class="modal fade" id="deletePurchaseModal-{{ $purchase->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
        <div class="modal-header">
          <h4 class="modal-title">Are you sure to delete {{ $purchase->order_no}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="viewModal-{{ $purchase->id }}" method="post" action="{{ route('purchase_delete',$purchase->id)}}">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

<!---------- View details Model-------------->
@foreach($purchases as $purchase)
<div class="modal fade" id="viewModel-{{ $purchase->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader" class="preloader"></div>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Purchase Details</i>
                </div>
            </div>
        </div>
        <div class="modal-body modal_view" id="modal_body">
        </div>
      </div>
      
    </div>
  </div>
@endforeach



<!---------- Add Payment Model-------------->
@foreach($purchases as $purchase)
<div class="modal fade" id="paymentModal-{{ $purchase->id }}">
      <div class="modal-content" style="width:800px;">
         <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Add Payment</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
            </div>
        <div class="modal-body">
          <div class="modal-dialog">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader_sec" class="preloader_sec"></div>
          </div>
               <form method="post" action="{{ route('payment_add')}}">
                      @csrf
                      <div class="row">
                        <input type="hidden" name="payment_id" id="deduct_amount" class="deduct_amount">
                        <div class="col-md-6">
                            <label>Received Amount *</label>
                            <input type="text" name="paying_amount" id="paying_amount" class="form-control numkey paying_amount" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45">
                        </div>
                        <div class="col-md-6">
                            <label>Paying Amount *</label>
                            <input type="text" id="amount" name="amount" class="form-control numkey amount" step="any" required="" onkeypress="return event.charCode >=48 && event.charCode <=57 || event.charCode==43 || event.charCode==40 || event.charCode==41 || event.charCode==45" onkeyup="change_val()">
                        </div>
                      </div>

                       <div class="row">
                        <div class="col-md-6 mt-1">
                            <label>Change : </label>
                            <p class="change ml-2">0.00</p>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-6" style="margin-top:15px">
                          <label> Paid By</label>
                        <select name="paid_by" id="paid_by" class="form-control numkey">
                          <option value="cash">Cash</option>
                          <option value="bank">Bank</option>
                        </select>
                      </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <label>Account *</label>
                      <select name="account_type" id="account_type" class="form-control numkey">
                        @foreach($accounts as $account)
                        <option value="{{ $account->id }}">{{ $account->name}} [{{ $account->account_no}}]</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Add Payment</button>
          </form>
        </div>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

<!---------- View Payment Model-------------->
@foreach($purchases as $purchase)
<div class="modal fade" id="viewPaymentModel-{{ $purchase->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader_thr" class="preloader_thr"></div>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Payment Details</i>
                </div>
            </div>
        </div>
        <div class="modal-body modal_payment" id="modal_payment">

        </div>
      </div>
    </div>
  </div>
@endforeach

<!---------- Update Payment Model-------------->
<div class="modal fade" id="updatePaymentModel" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content" style="width:800px;">
        <div class="status"><img src="{{asset('public/gif/sample.gif')}}" style="display: none; margin: auto;" id="preloader" class="preloader_for"></div>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close" style="color: #f96332;font-size: 23px;top: 10px;right: 10px;">X</button>
         <div class="container mt-3 pb-2 border-bottom">
            <div class="row">
                <div class="col-md-12 text-center">
                    <i style="font-size: 20px;">Hdiamond</i>
                </div>
                <div class="col-md-12 text-center">
                    <i style="font-size: 15px;">Update Payment</i>
                </div>
            </div>
        </div>
        <div class="modal-body modal_paymentUpdate" id="modal_paymentUpdate">
          
        
        </div>
      </div>
      
    </div>
  </div>


@endsection
@section('script')

<script>

$('.invoiceModel').click(function(e){

  let order_no = $(this).attr('data-order_no');
  let total_amount = 0;
  $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          url:"purchase-invoice",
          type:"POST",
          data:{id:order_no,},
          cache:'false',
          beforeSend: function() {
              $(".preloader_fif").show();
           },
          success:function(e)
          {
            $(".purchase-content").html(e.head)
            $(".showDetail").html(e.body)
            $(".preloader_fif").hide()
          },
          error:function(e)
          {  
            $(".preloader_fif").hide();
            console.log(e);
          }
        });
});


 function printDiv(divName) {
        $('#print-btn').hide()
        $('.btn-primary').hide()
        $('.close').hide()
        $('.head-hd').hide()
        var printContents = document.getElementById(divName).innerHTML;
        w=window.open();
        w.document.write('<!DOCTYPE html><html lang="en"><head><link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" />'+
            '<style type="text/css">table {border: 1px solid #000 !important;} table th {border: 1px solid #000 !important;} table td{border: 1px solid #000 !important;}@media  print {.modal-dialog { max-width: 1000px;} } table{ border:1px solid }</style>'+
            '<title>Invoice</title></head><body onload="window.print()" ><div class="card-body">'
            +printContents + 
            '</div></body></html>');
        w.print();
        w.close();
        $('#print-btn').show()
        $('.close').show()
        $('.head-hd').show()
        $('.btn-primary').show()
    
    }

$('.viewDetails').click(function(e){
  let order_no = $(this).attr('data-order_no');
  let token = '<?php echo csrf_token() ?>';
  $.ajax({
          url:"view_Details",
          type:"POST",
          data:{order_no:order_no,_token:token},
          cache:'false',
           beforeSend: function() {
              $(".preloader").show();
           },
          success:function(e)
          {
             $(".preloader").hide();
            $('.modal_view').html(e);
          },
          error:function(e)
          {
            $(".preloader").hide();
            console.log(e);
          }
        });
});
</script>
<script>
$('.payment').click(function(e){
  let order_no = $(this).attr('data-order_no');
  let token = '<?php echo csrf_token() ?>';
  let total_amount = 0;
  $.ajax({
          url:"payment_detail",
          type:"POST",
          data:{order_no:order_no,_token:token},
          cache:'false',
          beforeSend: function() {
              $(".preloader_sec").show();
           },
          success:function(e)
          {
            total_amount = e.data.grand_total - e.data.paid_amount;
            $('.paying_amount').val(total_amount);
            $('.amount').val(total_amount);
            $('.deduct_amount').val(e.data.id);
            $(".preloader_sec").hide();
          },
          error:function(e)
          {
             $(".preloader_sec").hide();
            console.log(e);
          }
        });
});
</script>

<script>
$('.view_Payment').click(function(e){
  let order_no = $(this).attr('data-order_no');
  let token = '<?php echo csrf_token() ?>';
  $.ajax({
          url:"payment_view",
          type:"POST",
          data:{order_no:order_no,_token:token},
          cache:'false',
           beforeSend: function() {
              $(".preloader_thr").show();
           },
          success:function(e)
          {
            $(".preloader_thr").hide();
            $('.modal_payment').html(e);
          },
          error:function(e)
          {
            $(".preloader_thr").hide();
            console.log(e);
          }
        });
});
</script>

<script>
  function updatePayment(aru)
  {
  let token = '<?php echo csrf_token() ?>';
  $.ajax({
          url:"payment_update",
          type:"POST",
          data:{id:aru,_token:token},
          cache:'false',
           beforeSend: function() {
              $(".preloader_for").show();
           },
          success:function(e)
          {
            $(".preloader_for").hide();
            $('.modal_paymentUpdate').html(e);
          },
          error:function(e)
          {
            $(".preloader_for").hide();
            console.log(e);
          }
        });
}
</script>

<script>
  function change_val()
  {
    let grand_tot = $('.paying_amount').val();
    let pay_amt = $('.amount').val();
    let change = grand_tot - pay_amt;
    $('.change').text(change);
  }
</script>

<script>
  function change_valUpdate()
  {
    let grand_tot = $('.paying_amountUpdate').val();
    let pay_amt = $('.amountUpdate').val();
    let change = grand_tot - pay_amt;
    $('.changeUpdate').text(change);
  }
</script>



@endsection