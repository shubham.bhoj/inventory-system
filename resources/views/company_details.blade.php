@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left">Company Profile</h4>
</div>
</div>
            </div>
              <div class="card-body">
            @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
            <form method="POST" action="{{ route('company_details_update') }}" enctype="multipart/form-data">
            @method('PATCH')
				@csrf
        <div class="row">
						<input type="hidden" value="{{$data->id}}" name="id">
<div class="col-sm-4">
					<div class="form-group">
						<label>Company Name*</label>
						<input type="text" value="{{$data->company_name}}" name="company_name" class="form-control" placeholder="" required>
						@error('company_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Civil/CR. NO.*</label>
						<input type="number" onkeypress="return isNumberKey(event)" value="{{$data->cr_no}}" name="cr_no" class="form-control" placeholder="" required>
						@error('cr_no')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Address*</label>
						<input type="text" value="{{$data->address}}" name="address" class="form-control" placeholder="" required>
						@error('address')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Mobile*</label>
						<input type="text" value="{{$data->contact_phone}}" onkeypress="return isNumberKey(event)" name="contact_phone" class="form-control" placeholder="" required>
						@error('contact_phone')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Email*</label>
						<input type="text" value="{{$data->contact_email}}" name="contact_email" class="form-control" placeholder="" required>
						@error('contact_email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Website*</label>
						<input type="text" value="{{$data->website}}" name="website" class="form-control" placeholder="" required>
						@error('website')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>
<div class="col-sm-4">
					<div class="form-group">
						<label>Logo*</label>
						<input type="file" name="logo" accept="image/*" class="form-control" placeholder="">
						@error('logo')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>

<div class="col-sm-4">
					<div class="form-group" style="margin-top: 18px;">
						<img src={{url('logo/'.$data->logo)}} id="myImg"  width="60" height="50" alt="No Image">
					</div>
</div>

<div class="col-sm-4">
					<div class="form-group">
						<label>Barcode*</label>
						<input type="text" name="barcode" class="form-control" maxlength="10" placeholder="Enter only 10 character" value="{{$data->barcode_com}}">
						@error('barcode')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<span class="form-text text-muted"></span>
					</div>
</div>

      </div>
<button type="submit" class="btn btn-primary font-weight-bold">Save</button>

      </form>
      </div>
	  <div id="myModal" class="modal">
<span class="close">&times;</span>
<img class="modal-content" id="img01">
</div>
<script>
var modal = document.getElementById("myModal");
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  $(".displaynav").css("display",'none');
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
  modal.style.display = "none";
  $(".displaynav").css("display",'block');
} 
</script>
@endsection