@extends('layouts.app')
@section('content')
<div class="card-header">
<div class="row">
        <div class="col-sm-12">
                <h4 class="card-title float-left">Balance</h4>
</div></div>
              </div>
              <div class="card-body">
              @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Success</span> 
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>  
            @endif
            @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">Error</span> 
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @endif
              <div class="table-responsive">
                  <table class="table data-table">
                    <thead class="text-primary">
                    <th>Sr. No</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Account No.</th>
                    <th>Balance</th>
                    <th>Description</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($data as $key=>$st)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$st->name}}</td>
                        @if($st->type==1)
                        <td>Bank</td>
                        @else
                        <td>Cash</td>
                        @endif
                        <td>{{$st->account}}</td>
                        <td>{{$st->balance}}</td>
                        <td>{{$st->note}}</td>
                        <td>
                        <a href="{{ route('banks_details',$st->id) }}" class="btn btn-icon btn-success btn-sm mr-2"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <a href="{{ route('banks_view',$st->id) }}" class="btn btn-icon btn-info btn-sm mr-2"><i class="fa fa-eye" aria-hidden="true"></i></a>
							          <!-- <a onclick="return confirm('Are you sure?')" href="{{ route('retailers_delete',$st->id) }}" class="btn btn-icon btn-danger btn-sm mr-2"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>

@endsection
