@extends('layouts.app')
@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="card-title float-left"> Purchase Details</h4>
        </div>
    </div>
</div>
<div class="card-body">
	@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> 
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>  
    @endif
    @if(session()->has('error'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Error</span> 
                {{ session()->get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('purchase_editPost',$purchase->id)}}" enctype="multipart/form-data">
	@csrf
  <div class="row" id="retailerrow" style="display:flex;">
        <div class="col-sm-6">
			<div class="form-group">
			<label>Supllier*</label>
				<select name="supllier_id" id="" class="form-control" required>
				<option value="">Select Supllier</option>
		        @foreach($suppliers as $supllier)
		        <option value="{{ $supllier->id }}" <?= ($purchase->supplier_id == $supllier->id)?'selected':'' ?>>{{ $supllier->company_name }}</option>
		        @endforeach
			    </select>
			    @error('supllier_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>

        <div class="col-sm-6">
			<div class="form-group">
			<label>Attached Document*</label>
				<input type="file" name="document" class="form-control" >
				@error('document')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
				<span class="form-text text-muted"></span>
			</div>
        </div>
    </div>

    	<div class="tab-pane fade show active"  style="display:block" id="tabletab" role="tabpanel" aria-labelledby="table-tab">
		<div class="table-responsive">
		<table border=1 style="margin-top:5%;" id="stockdetails" class="table-view" name="cart">
		<thead class="text-primary text-center">
				
				<th>
					Particular
				</th>
				<th>
					Qty
				</th>
				<th>
					Gross Wt. (Gms)
				</th>
				<th>
					Gold Net Wt. (Gms)
				</th>
				
				<th>
					Diamond Wt. (cts)
				</th>
				<th>
					Stone Wt. (cts)
				</th>
				<th>
					$ Amount
				</th>
				<th>
					KWD Amount
				</th>
				
				<th>
					Action
				</th>
			</thead>
			<tbody>
				@foreach($details as $detail)
				<tr class="line_items">
					
					<td>
		            	<input style="width:250px" type="text" name="particular[]" id="particular" placeholder="particular" class="form-control" required value="{{ $detail->particular}}">
		        	</td>
					<td>
		            	<input  style="width:100px" type="text" name="qty[]" id="qty" class="form-control" placeholder="Quantity" onkeyup="total_amount('qty','tot_qty','input_tot_qty')" required value="{{ $detail->qty}}">
		        	</td>
					<td>
		            	<input  type="text" name="grs_wt[]" id="grs_wt" class="form-control" placeholder="Gross Weight" onkeyup="total_amount('grs_wt','tot_grs_wt','input_tot_grs_wt')" required value="{{ $detail->gross_wt}}">
		        	</td>
		        	<td>
		            	<input  type="text" name="gldnet_wt[]" id="gldnet_wt" class="form-control" placeholder="Gold Net Weight" onkeyup="total_amount('gldnet_wt','tot_gldnet_wt','input_tot_gldnet_wt')" required value="{{ $detail->gld_net_wt}}">
		        	</td>
					<td>
		            	<input type="text" name="diamond_wt[]" id="diamond_wt" class="form-control" placeholder="Diamond Weight" onkeyup="total_amount('diamond_wt','tot_dmnd_wt','input_tot_dmnd_wt')" required value="{{ $detail->diamond_wt}}"> 
		        	</td>
					<td>
		            	<input type="text" name="stone_wt[]" id="stone_wt" class="form-control" placeholder="Stone Weight" onkeyup="total_amount('stone_wt','tot_stn_wt','input_tot_stn_wt')" required value="{{ $detail->stone_wt}}">
		        	</td>
					<td>
		            	<input type="text" name="amount[]" id="amount" class="form-control" placeholder="Amount" onkeyup="total_amount_v1('amount','tot_amount')" required value="{{ $detail->amount}}">
		            	<input type="hidden" id="usdtokwd" value="{{ $settings->purchase_rate_kwd }}" >
		        	</td>
					<td>
		            	<input type="text" name="kwd_amt[]" id="kwd_amt" class="form-control" placeholder="KWD Amount" onkeyup="total_amount_v2('kwd_amt','tot_kwd_amt')" required value="{{ $detail->kwd_amount}}">
		            	<input type="hidden" id="kwdtousd" value="{{ $settings->sales_rate_kwd }}">
		            	<input type="hidden" name="order_no" value="{{ $purchase->order_no}}">
		        	</td>
					<td><button class="btn btn-danger font-weight-bold row-remove">Remove</button></td>
                </tr>
                @endforeach
		<tr style="border:0;">
			<td colspan="22"><button class="btn btn-success font-weight-bold row-add">Add Row</button></td>
		</tr>
		<tr>
		<td style="text-align: right; font-weight:bold; border: 1px solid;">Total:</td>
		<td style="border: 1px solid;" id="tot_qty">{{$purchase->tot_qty}}</td>
		<input type="hidden" name="tot_qty" id="input_tot_qty">
		<td style="border: 1px solid;" id="tot_grs_wt">{{$purchase->tot_gross_wt}}</td>
		<input type="hidden" name="tot_grs_wt" id="input_tot_grs_wt">
		<td style="border: 1px solid;" id="tot_gldnet_wt">{{$purchase->tot_gld_net_wt}}</td>
		<input type="hidden" name="tot_gldnet_wt" id="input_tot_gldnet_wt">
		<td style="border: 1px solid;" id="tot_dmnd_wt">{{$purchase->tot_diamond_wt}}</td>
		<input type="hidden" name="tot_dmnd_wt" id="input_tot_dmnd_wt">
		<td style="border: 1px solid;" id="tot_stn_wt">{{$purchase->tot_stone_wt}}</td>
		<input type="hidden" name="tot_stn_wt" id="input_tot_stn_wt">
		<td style="border: 1px solid;" id="tot_amount">{{$purchase->tot_amount}}</td>
		<input type="hidden" name="tot_amount" id="input_tot_amount">
		<td style="border: 1px solid;" colspan="3" id="tot_kwd_amt">{{$purchase->total_cost}}</td>
		<input type="hidden" name="tot_kwd_amt" id="input_kwd_amt">

		</tr>
		</tbody>
	</table>
        </div>
</div>
		<div style="width: 230px;display: block;margin-left: auto;">
		<table class="table" id="myTable">
			<tbody>
			<tr>
				<td style="border-top: 1px;">Discount</td>
				<td style="border-top: 1px;">
				<input type="text" value="{{$purchase->discount}}" name="discount" id="discount" class="form-control" placeholder="Discount" onkeyup="total_cal()">
                </td>
			</tr>
			<tr>
				<td>Freight</td>
				<td>
				<input type="text" value="{{$purchase->freight}}" name="freight" id="freight" class="form-control" placeholder="Freight" onkeyup="total_cal()">
                </td>
			</tr>
			<tr>
				<td>Sub Total</td>
				<td>
				<input type="text" value="{{ ($purchase->total_cost - $purchase->discount + $purchase->freight)}}" name="sub_total" id="sub_total" class="form-control" placeholder="Sub Total" readonly>
                </td>
			</tr>
			<tr>
				<td>VAT - ({{ $settings->vat }}%)</td>
				<td>
				<input type="hidden" id="vat_charge" value="{{ $settings->vat }}">
				<input type="text" value="{{$purchase->vat}}" name="vat" id="vat" class="form-control" placeholder="VAT" readonly>
                </td>
			</tr>
			<tr>
				<td>Net Amount</td>
				<td>
				<input type="text" value="{{ $purchase->grand_total}}" name="total" id="total" class="form-control" placeholder="Total" readonly>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
    <button type="submit" class="btn btn-primary font-weight-bold">Update</button>
    </form>
</div>
<script>
	function total_amount(arr,arr_sec,arr_thr)
	{
		let allElems = document.querySelectorAll("[id='"+arr+"']");
		let total = 0;
		allElems.forEach(i => {
			total = total + Number(i.value)
		})
		$('#'+arr_sec).text(total);
		$('#'+arr_thr).val(total);
	}

	function total_amount_v1(arr,arr_sec)
	{

		let allElems = document.querySelectorAll("[id='"+arr+"']");
		let usdTokwd = $("#usdtokwd").val();
		let total = 0;
		allElems.forEach(i => {
			total = total + Number(i.value)
		})
		$('#'+arr_sec).text(total);
		$('#input_tot_amount').val(total);

		let amt = document.getElementById('amount').value;
		document.getElementById('kwd_amt').value = amt * usdTokwd;
		let allKwd_amt = document.querySelectorAll("[id='kwd_amt']");
		let total_kwd = 0;
		allKwd_amt.forEach(i => {
			total_kwd = total_kwd + Number(i.value)
		})
		$('#tot_kwd_amt').text(total_kwd);
		$('#input_kwd_amt').val(total_kwd);
		$("#sub_total").val(total_kwd);
		total_cal();
	}

	function total_amount_v2(arr,arr_sec)
	{

		let allElems = document.querySelectorAll("[id='"+arr+"']");
		let kwdTousd = $("#kwdtousd").val();
		let total = 0;
		allElems.forEach(i => {
			total = total + Number(i.value)
		})
		$('#'+arr_sec).text(total);
		let amtKWD = document.getElementById('kwd_amt').value;
		document.getElementById('amount').value = amtKWD * kwdTousd;
		total_cal();
	}

	

	function total_cal() {
	var total_value_kwd_total = parseFloat($("#tot_kwd_amt").text()).toFixed(2);
	var discount = parseFloat($("#discount").val()).toFixed(2);
	var freight = parseFloat($("#freight").val()).toFixed(2);
	var vat_cal = parseFloat($("#vat_charge").val()).toFixed(2);
	var sub_total = parseFloat(parseFloat(total_value_kwd_total)-parseFloat(discount)+parseFloat(freight)).toFixed(2);
	var vat_total = parseFloat((sub_total*vat_cal)/100).toFixed(2);
	var total_amount = Math.round(parseFloat(parseFloat(sub_total)+parseFloat(vat_total)).toFixed(2));
	$("#vat").val(vat_total);
	$("#sub_total").val(sub_total); 
	$("#total").val(total_amount);
	$("#paying_amount").val(total_amount);
    }

    
</script>
@endsection